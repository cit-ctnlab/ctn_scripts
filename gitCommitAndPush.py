import subprocess
import os
import argparse


def gitCommitAndPush(fileList, commitMessage, branchName = 'master'):
    print('Creating commit and push bash script...')
    with open('temp.sh', 'w') as f:
        f.write('#!/bin/sh\n')
        # switch to branch you want to use
        f.write('git checkout ' + branchName + '\n')
        # Git pull first
        f.write('git pull\n')
        # add all added/modified files
        for filename in fileList:
            f.write('git add '+filename + '\n')
        # commit changes
        f.write('git commit -m \"' + commitMessage + '\"\n')
        # push to git remote repository
        f.write('git push\n')

    print('Running the bash script...')
    os.system('chmod +x temp.sh')
    os.system('./temp.sh')

    print('Deleting the temporary bash script...')
    os.remove('temp.sh')
    print('Done!')


def grabInputArgs():
    parser = argparse.ArgumentParser(description='This script runs git commit '
                                                 'and push in current git '
                                                 'repo')
    parser.add_argument('-f','--fileList', nargs='+', required=True,
                        help='List of files to add')
    parser.add_argument('-m', '--commitMessage', type=str,
                        help='COmmit Message', required=True)
    parser.add_argument('-b', '--branchName', type=str,
                        help='Branch. Default is master.', default='master')
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    gitCommitAndPush(args.fileList, args.commitMessage, args.branchName)
