import os
import time
import sys
import argparse

parser = argparse.ArgumentParser(description='Usage: python measBNnoise.py '+
                                 '--command <input command string for '+
                                 'BNnoise measurement> '+
                                 '--number <input some int> '+
                                 '--duration <input duration in hrs> '+
                                 '--filename <Filename prefix str> '+
                                 '--ipaddress <ip address of equipment>')
parser.add_argument('--command', type=str, nargs=1,
                    help='If defined, becomes the command line run <number> times',
                    default=['python /home/controls/Git/labutils/netgpibdata/SRmeasure'])
parser.add_argument('--number', type=int, nargs=1,
                    help='The number of measurements taken',
                    default=[sys.maxint])
parser.add_argument('--duration', type=int, nargs=1,
                    help='The number hrs of measurements taken',
                    default=[sys.maxint])
parser.add_argument('--delay', type=int, nargs=1,
                    help='If defined, becomes the delay (s) between each measurement',
                    default=[0])
parser.add_argument('-f',
                    '--filename',
                    help='Stem of output filename.',
                    default='BNmeasurement_')
parser.add_argument('-i',
                    '--ipaddress',
                    help='IP address or hostname.',
                    default=None)
args = parser.parse_args()

actualCommand1 = args.command[0]+' -i '+args.ipaddress+' --trigger'
actualCommand2 = args.command[0]+' -i '+args.ipaddress+' --getdata'+' -f '+args.filename
number = args.number[0]
duration = (args.duration[0])*3600.0
delay = args.delay[0]

print('Sending command: ')
print(actualCommand1)
print(actualCommand2)

print(str(number)+' times or for '+str(duration/3600)+' hrs')
print('Whichever is reached first.')
print

measStartTime = time.time()
ii=0
while(ii<number and (time.time()-measStartTime)<duration):
  print
  print 'Starting Measurement ', ii
  print
  startTime = time.time()
  os.system(actualCommand1)
  os.system(actualCommand2+'_'+str(ii))
  print
  print 'Measurement ', ii,' finished in ', time.time() - startTime, ' seconds'
  print
  print '---------------------------------------------------------------------'
  ii = ii+1
  time.sleep(delay)
