#!/bin/bash

export EPICS_HOST_ARCH=linux-x86_64
export epicstop=/ligo/apps/ubuntu12/epics-3.14.12.2_long
export EPICS_BASE=${epicstop}/base
export EPICS_BASEDIR=${epicstop}/base
export EPICS_MODULES=${epicstop}/modules
export EPICS_SEQ=${EPICS_MODULES}/seq
export EPICS_EXTENSIONS=${epicstop}/extensions
export EPICSBIN=${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${EPICS_EXTENSIONS}/bin/${EPICS_HOST_ARCH}
export EPICSLIB=${EPICS_BASE}/lib/${EPICS_HOST_ARCH}:${EPICS_EXTENSIONS}/lib/${EPICS_HOST_ARCH}
export PYEPICS_LOCATION=${epicstop}/pyext/pyepics
export PCASPY_LOCATION=${epicstop}/pyext/pcaspy
export EPICS_DB_INCLUDE_PATH=${epicstop}/base/dbd
export RPN_DEFNS=${epicstop}/etc/defns.rpn

export PATH=${EPICS_EXTENSIONS}/bin/${EPICS_HOST_ARCH}:${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${EPICS_SEQ}/bin/${EPICS_HOST_ARCH}:${PATH}
D_LIBRARY_PATH=${EPICS_EXTENSIONS}/lib/${EPICS_HOST_ARCH}:${EPICS_BASE}/lib/${EPICS_HOST_ARCH}:${EPICS_SEQ}/lib/${EPICS_HOST_ARCH}:${LD_LIBRARY_PATH}

#exec /home/controls/modbus/bin/linux-x86_64/modbusApp /home/controls/modbus/iocBoot/iocTest/acromag.cmd >> /home/controls/logs/Acromagtestboot.log 2>/home/controls/logs/Acromagtestboot2.log
/home/controls/modbus/bin/linux-x86_64/modbusApp /home/controls/modbus/iocBoot/iocTest/acromag.cmd
echo "Script ran but didn't hold on modbus" >> /home/controls/logs/Acromagtestboot.log

