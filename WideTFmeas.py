'''
author: Anchal Gupta
email: anchal@caltech.edu
date: 05/30/2019

This script performs a measurement of TF of FSS loop.
Split RF out of AD4395A into 2 cables.
Connect EXC port on the left side in Common group
of TTFSS box to RF out of AG4395A.
Connect port A to splitted RF out.
Connect port B to OUT1 port on the left side in
Common group of TTFSS box through a high impedance
probe HP 41800A.
Run the script!
'''
import ConfigParser
import argparse
import os
import glob
import time
import numpy as np
import yaml
# Custom libaries
import AG4395A as inst

def readParams(paramFile):
    # Function to read a measurement parameter file in the YAML format
    with open(paramFile, 'r') as f:
        reader = yaml.load_all(f)
        params = reader.next()
        reader.close()
    return(params)

def noOfFilesWithRoot(filename):
    Dir = os.getcwd()
    return len(
        [name for name in os.listdir(Dir) if name.find(filename) is not -1])

def stitch(filelist):
    leastF = 5e4
    prevul = -1
    stichedData = np.zeros((0,3))
    header = []
    while(len(filelist)>0):
        for fn in filelist:
            ll = np.loadtxt(fn)[0,0]
            if ll<leastF:
                leastF = ll
                LFfile = fn
        temp = np.loadtxt(LFfile)
        with open(LFfile) as f:
            for line in f.readlines():
                if line[0] is '#':
                    header += [line]
        tempff = temp[:,0]
        for ii in range(len(tempff)):
            if tempff[ii]>prevul:
                break
        stichedData = np.concatenate((stichedData,temp[ii:,:]))
        prevul = tempff[-1]
        leastF = 5e4
        filelist.remove(fn)
    return stichedData, header

def main(args,param):
    if args.paramfile is not None:
        paramFilename = args.paramfile
    else:
        paramFilename = param.paramfile
    print('Reading parameters from ' + paramFilename)
    params = readParams(paramFilename)
    params['fileName'] = paramFilename
    if args.ipaddress is not None:
        params['ipAddress'] = args.ipaddress
    if args.gpibaddress is not None:
        params['gpibAddress'] = args.gpibaddress
    if param.nameRoot is not None:
        params['nameRoot'] = param.nameRoot
    fileExt = '.txt'
    if args.filename is not None:
        params['nameRoot'] = args.filename
        if params['saveDir'] == 'NaN':
            params['saveDir'] = os.path.dirname(filename) + '/'
            params['nameRoot'] = os.path.basename(filename).split('.')[0]
            print(params['saveDir'])
            print(params['nameRoot'])
        if '.' in args.filename:
            fileExt = ''.join(args.filename.split('.')[1:])
    gpibObj = inst.connectGPIB(params['ipAddress'], params['gpibAddress'])
    # Set up output file names
    fileRoot = (params['nameRoot'] + '_' +
                time.strftime('%d-%m-%Y', time.localtime()) +
                time.strftime('_%H%M%S', time.localtime()))
    dataFileName = fileRoot+fileExt
    outDir = os.path.expanduser(params['saveDir'])
    # Check if outDir exists
    if not os.path.exists(outDir):
        os.makedirs(outDir)
    #Starting the measurement
    print('Executing measurement specified in ' + paramFilename)
    measData = np.zeros((0,3))
    for ii in range(1,len(param.rangeSet)+1):
        set = 'Set'+str(ii)
        arr = param.rangeSet[set]
        print('Performing measurement '+set)
        params['measType'] = 'TF'
        params['startFreq'] = arr[0]
        params['stopFreq'] = arr[1]
        params['ifBandwidth'] = arr[2]
        params['numOfPoints'] = arr[3]
        params['sweepType'] = arr[4]
        params['averages'] = arr[5]
        params['timeStamp'] = time.strftime('%b %d %Y - %H:%M:%S',
                                            time.localtime())
        inst.setParameters(gpibObj, params)
        inst.measure(gpibObj, params)
        nDisp = int(gpibObj.query('DUAC?')) + 1
        if nDisp == 1:
            chans = [int(gpibObj.query('CHAN2?')) + 1]
        else:
            chans = [1, 2]

        meas = []
        fmt = []
        for chan in chans:
            gpibObj.command('CHAN'+str(chan))
            # What measurement?
            meas.append(gpibObj.query('MEAS?')[:-1])
            fmt.append(gpibObj.query('FMT?')[:-1])
        if fmt == ['LOGM', 'PHAS']:
            params['dataMode'] = 'dbdeg'
        elif fmt == ['LINM', 'PHAS']:
            params['dataMode'] = 'magdeg'
        elif fmt == ['REAL', 'IMAG']:
            params['dataMode'] = 'reim'
        else:
            params['dataMode'] = str(fmt)
        print('Detected Units: '+params['dataMode'])

        # Let the instrument catch up, then download the data
        time.sleep(2)
        (freq, data) = inst.download(gpibObj)
        with open(outDir + dataFileName, 'a') as dataFile:
            inst.writeHeader(dataFile, params['timeStamp'])
            dataFile.write('# Parameter File: ' + params['fileName'] + '\n')
            inst.writeParams(gpibObj, dataFile)
            inst.writeData(dataFile, freq, data)
    print("Done!")
    gpibObj.close()


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description="PID control script that reads out and controls EPICS"
                    " channels")
    parser.add_argument('configfile',
                        type=str,
                        help="Enter a config .ini file + path")
    parser.add_argument('-p',
                        '--paramfile',
                        help='Enter a param file name to override'
                            ' the one from config file.',
                        default=None)
    parser.add_argument('-i',
                        '--ipaddress',
                        help='IP address or hostname. Overrides param file.',
                        default=None)
    parser.add_argument('-a',
                        '--gpibaddress',
                        help='GPIB address, typically 10.'
                            'Overrides parameter file.',
                        default=10)
    parser.add_argument('-f',
                        '--filename',
                        help='Stem of output filename.'
                            ' Overrides parameter file.',
                        default=None)
    parser.add_argument('-m',
                        '--inputmode',
                        help='Input Mode of Network Analyzer'
                            '([AR]->A/R, [BR]->B/R)',
                        default=None)
    return parser.parse_args()

class ImportConfig():
    def __init__(self, configfile):
        config = ConfigParser.ConfigParser()
        config.read(configfile)
        self.paramfile = config.get("TFMeasSetting",
                                  "paramfile")
        self.AGmeasurepath = config.get("TFMeasSetting",
                                        "AGmeasurepath")
        try:
            self.nameRoot = config.get("TFMeasSetting",
                                       "nameRoot")
        except:
            self.nameRoot = None
        ii=0
        self.rangeSet = {}
        while(True):
            ii=ii+1
            set = 'Set'+str(ii)
            startFreq = 'startFreq'+set
            stopFreq = 'stopFreq'+set
            ifBW = 'ifBW'+set
            numOfPoints = 'numOfPoints'+set
            sweepType = 'sweepType'+set
            averages = 'averages'+set
            try:
                self.rangeSet[set]=[config.get('MeasParams',startFreq),
                                    config.get('MeasParams',stopFreq),
                                    config.get('MeasParams',ifBW),
                                    config.get('MeasParams',numOfPoints),
                                    config.get('MeasParams',sweepType),
                                    config.get('MeasParams',averages)]
            except:
                break


if __name__ == "__main__":
    args = grabInputArgs()  # import cmdline arguments
    param = ImportConfig(args.configfile)  # import config file
    main(args, param)
