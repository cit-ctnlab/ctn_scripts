'''
expConfig
This script reads channels in the given channelListFile and writes their
data in a .txt file from a framebuilder machine. It also takes mean of
them and create a log file containing experiment configuration details in
yaml format.
'''

import argparse
import time
import yaml
import os
import nds2
import numpy as np

channelListFile = 'ChannelList.txt'


def main(args):
    with open(args.channelListFile, 'r') as clf:
        CHList = clf.readlines()
    for ii,ch in enumerate(CHList):
        CHList[ii] = CHList[ii].replace('\n','')
        CHList[ii] = CHList[ii].replace('\r','')
    print('Reading channels:')
    print(CHList)
    print('at '+args.ipaddress+' on port '+str(args.port))
    c = nds2.connection(args.ipaddress, args.port)
    data = c.fetch(args.start,args.stop,CHList)
    filename = (args.filename + '_'
                + time.strftime('%d-%m-%Y', time.localtime())
                + time.strftime('_%H%M%S', time.localtime())+'.txt')
    dataToWrite = np.zeros((len(data[0].data),len(CHList)+1))
    dictToWrite = {}
    timeSeries = np.linspace(args.start,args.stop,len(data[0].data)+1)[:-1]
    dataToWrite[:,0] = timeSeries
    header = 'TimeSeries(s)  '
    for ii,ch in enumerate(CHList):
        dataToWrite[:,ii+1] = data[ii].data
        dictToWrite[ch] = str(np.mean(data[ii].data))
        header = header+ch+'  '
    print('Writing data to file '+filename+' ...')
    np.savetxt(filename,dataToWrite,header=header)

    logFileName = filename.replace('.txt', '_mean.yml')

    # Add Detector
    dictToWrite['detector'] = args.detector
    with open(logFileName, 'w') as logFile:
        yaml.dump(dictToWrite, logFile)


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script runs mokuReadFreqNoise on frequency data of '
                    'beatnote measured by moku. In addition, it also reads '
                    'channels written in ChannelList file and logs them in '
                    'a log file. Detector information is also written on '
                    'the log file.')
    parser.add_argument('channelListFile', type=str,
                        help='Channel List File')
    parser.add_argument('--detector', type=str,
                        help='Detector used. NF1811 or SN101',
                        default='NF1811')
    parser.add_argument('-f',
                        '--filename',
                        help='Stem of output filename.',
                        default='fromFBread')
    parser.add_argument('--start', type=int,help='Start gpstime.')
    parser.add_argument('--stop', type=int,help='End gpstime.')
    parser.add_argument('-i',
                        '--ipaddress',
                        type=str,
                        default='10.0.1.156',
                        help='IP Address of framebuilder.')
    parser.add_argument('-p',
                        '--port',
                        type=int,
                        default=8088,
                        help='Port number for connection to framebuilder.')
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    main(args)
