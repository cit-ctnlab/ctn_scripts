'''
mokuCleanRAM
Clears the RAM of moku wihtout rebooting. All data in the RAM will be lost.
Input Arguments:
ipAddress:  IP Address of Moku. Default is 10.0.1.81.
mokuName:    If used, moku will be searched for by name.
'''

from pymoku import Moku
import argparse


def mokuCleanRAM(ipAddress='10.0.1.81', mokuName=None):
    try:
        if mokuName is None:
            m = Moku(ipAddress)
        else:
            m = Moku.get_by_name(mokuName)
        fileList = m._fs_list('i')
        for ii in range(len(fileList)):
            fn = fileList[ii][0]
            print('Deleting ', fn)
            m._delete_file('i', fn)
        print('All cleaned.')
    except BaseException:
        print('Error occured: ')
    finally:
        try:
            m.close()
        except BaseException:
            print('Failed to connect to moku properly.')


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script clears the RAM of moku wihtout rebooting.'
                    ' All data in the RAM will be lost.')
    parser.add_argument('-i', '--ipAddress',
                        help='IP address of Moku. Default is 10.0.1.81',
                        default='10.0.1.81')
    parser.add_argument('--mokuName', type=str,
                        help='Connect to Moku by Name.',
                        default=None)
    return parser.parse_args()


if __name__ == '__main__':
    args = grabInputArgs()
    mokuCleanRAM(args.ipAddress, args.mokuName)
