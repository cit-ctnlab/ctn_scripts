import os
import argparse
from datetime import datetime
import time
import numpy as np
from epics import caget


def measBNASDfromSR(paramFile, fileRoot):
    print('Using SRmeasure to measure beatnote spectrum...')
    with open('temp.sh', 'w') as f:
        f.write('#!/bin/sh\n')
        # switch to branch you want to use
        # f.write('eval "$(conda shell.bash hook)"\n')
        # f.write('conda activate python2 \n')
        # Git pull first
        f.write('python ~/Git/labutils/netgpibdata/SRmeasure.py '
                + paramFile + ' -f 1111tempFileName \n')

    startTime = time.time()
    os.system('chmod +x temp.sh')
    os.system('./temp.sh')
    os.remove('temp.sh')
    endTime = time.time()

    fl = os.listdir()
    for fn in fl:
        if fn.find('1111tempFileName_') == 0:
            tempFileName = fn
            break
    date_time = datetime.strptime(tempFileName,
                                  '1111tempFileName_%d-%m-%Y_%H%M%S.txt')
    dataFileName = date_time.strftime('BeatnoteSpectrum_%Y%m%d_%H%M%S.txt')
    data = np.loadtxt(tempFileName)
    os.remove(tempFileName)
    data[:, 1] = data[:, 1]*caget('C3:PSL-PLL_AUTOLOCKER_FREQ_MOD')*1e3
    np.savetxt(dataFileName, data)

    if checkMarconi(startTime, endTime):
        return dataFileName
    else:
        os.remove(dataFileName)
        return 'Error'


def checkMarconi(startTime, endTime):
    print('Checking for PLL Carrier Freq jump')
    endTime = str(int(endTime - 315964782))
    startTime = str(int(startTime - 315964782 - 5))
    print(startTime, endTime)
    with open('temp.sh', 'w') as f:
        f.write('#!/bin/sh\n')
        # switch to branch you want to use
        f.write('eval "$(conda shell.bash hook)"\n')
        f.write('conda deactivate   \n')
        f.write('conda deactivate   \n')
        # Git pull first
        f.write('python ~/Git/cit_ctnlab/ctn_scripts/fromFBread.py '
                '-c C3:PSL-PLL_AUTOLOCKER_CARRIER_FREQ'
                ' --start ' + startTime + ' --stop ' + endTime + ' -f '
                + '1111tempFileName\n')
    startTime = time.time()
    os.system('chmod +x temp.sh')
    os.system('./temp.sh')
    os.remove('temp.sh')
    fl = os.listdir()
    for fn in fl:
        if fn.find('1111tempFileName') == 0:
            tempFileName = fn
            break
    data = np.loadtxt(tempFileName)
    os.remove(tempFileName)
    if np.nanstd(data[:,1])==0.0:
        return True
    else:
        return False


def grabInputArgs():
    parser = argparse.ArgumentParser(description='This script runs SRmeasure '
                                                 'to read BN freq noise and '
                                                 'return latest created '
                                                 'filename if PLL did not jump'
                                    )
    parser.add_argument('paramFile', nargs='?',
                        help='The parameter file for the measurement.',
                        default=None)
    parser.add_argument('-f',
                        '--fileRoot',
                        help='Stem of output filename.'
                             ' Overrides parameter file.',
                        default='BeatnoteSpectrum')
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    filename = measBNASDfromSR(args.paramFile, args.fileRoot)
    print(filename)
