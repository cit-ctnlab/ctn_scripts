import numpy as np
import re
import os

DirListFile = 'dbfilesdir.txt'
dbCompile = re.compile('\w+.db')  # compile the regular expression
dbFiles = np.array([])
cwd = os.getcwd()

with open(DirListFile) as f:
    DirList = [line.rstrip('\n') for line in f]
    DirList = [line.replace('/dbdir', '/home/controls/Git/cit_ctnlab/modbus') for line in DirList]
for dir in DirList:
    try:
        os.chdir(dir)
        FileList = os.listdir(dir)
        for filename in FileList:
            if dbCompile.search(filename):
                dbFiles = np.append(dbFiles, dir+filename)
    except Exception:
        print(dir+' was not found.')

os.chdir(cwd)
print('db files: ', dbFiles)

# compile a regular expression for lines with channel names
recordCompile = re.compile(r'\Arecord\(\w+,\s*"C3:PSL-\w+"\)')
channelCompile = re.compile(r'C3:PSL-\w+')
channelTypeCompile = re.compile(r'\(\w+,')
channelDict = {}

for filename in dbFiles:
    lines = [line.rstrip('\n') for line in open(filename)]  # get lines from file, and strip it
    # Now search each line for a channel name
    for ii, line in enumerate(lines):
        lineSearch = recordCompile.search(line)
        if lineSearch:
            channelSearch = channelCompile.search(line)
            channelName = channelSearch.group()

            channelTypeSearch = channelTypeCompile.search(line)
            channelType = channelTypeSearch.group()
            channelType = channelType[1:-1]  # Shave off the first and last characters

            channelDict[channelName] = channelType

# Will keep this in ctn_scripts for git version control
channelFilename = 'C3CTN.ini'

with open(channelFilename, 'w') as f:
    # First, write the default framebuilder info
    f.write('[default]\n')
    f.write('dcuid=4\n')
    f.write('datarate=16\n')
    f.write('gain=1.0\n')
    f.write('acquire=1\n')
    f.write('ifoid=0\n')
    f.write('datatype=4\n')
    f.write('units=units\n')
    f.write('slope=1.0\n')
    f.write('offset=0\n')

    f.write('\n')
    f.write('### All CTN Channels ###\n')
    # Now write all the channels we've found
    for channel in channelDict.keys():
        f.write('[' + channel + ']\n')  # Write every channel

        # Then write each channel's units based on the channel name or type.  I know this is bad but it's fast.
        channelType = channelDict[channel]
        if 'b' in channelType:  # If the letter b is in the channel type, we know it's boolean
            units = 'bool'
        elif ('_KP' in channel) or ('_KI' in channel) or ('_KD' in channel):
            units = 'gain'
        elif ('C3:PSL-TEMP' in channel) or ('_CAN_SETPOINT' in channel):
            units = 'celcius'
        elif ('_PD_GAIN' in channel) or ('_DB' in channel):
            units = 'dB'
        elif ('_POW' in channel):
            units = 'mW'
        elif ('_POUT' in channel):
            units = 'watts'
        elif ('_MODE_MATCHING' in channel):
            units = '%'
        elif ('_SLOPE2' in channel):
            units = 'Hz/s/s'
        elif ('_SLOPE' in channel):
            units = 'kHz/s'
        elif ('_DRIFT' in channel):
            units = 'kHz'
        elif ('_CARRIER_FREQ' in channel) or ('_BEATNOTE_FREQ' in channel) or ('PID_SETPOINT' in channel):
            units = 'MHz'
        elif ('_FREQ_MOD' in channel):
            units = 'kHz'
        else:
            units = 'volts'
        f.write('units='+units+'\n')

print('Framebuilder Config File written to ', channelFilename)
print('To restart the framebuilder with these channels, copy this file onto ',
      'fb4 into /opt/rtcds/caltech/c4/chans/daq/C3CTN.ini')
print('Then, restart the framebuilder by following the instructions in',
      'https://nodus.ligo.caltech.edu:8081/CTN/2014')
