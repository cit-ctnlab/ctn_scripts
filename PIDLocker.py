#!/usr/bin/env python

import re
import time
import os
from ezca import Ezca
import ConfigParser
import argparse


'''
Author: Yeyuan (Yinzi) Xin - yxin@caltech.edu
Author: Andrew Wade (awade) - awade@ligo.caltech.edu
Created: ~ Nov 3, 2016

Change log:
20161104 - awade: Moved the configuation of channels to a seperate .ini configuration file.  This make the loop to implimented without hardcoding into the script
20161104 - awade: Added parsing for command line options so that config and debug options can be called directly from commandline instance.  This also means that script will no long need to be hard coded with channen names or any reference to a particular application.  It will be a generalized PID script in python.
20161104 - awade: Indents on some of the if debug==1 lines were wrong, this was causing errors, has been fixed.
20170902 - awade: Fixed config input parser to take muli channel requirments like autolocker.py and to cope with default special cases where setpoint or blinker are not requried by user.  Also fixed arg parse to something a little less archaic 
Issues:
- Even when not in debug mode the write command prints values written out, no big problem but defeats purpose of having debug control if it does suppress this.
- There is no option to not set a soft blink channel.  
'''

RCPID = Ezca(ifo=None)  # initialize ezca python access to channels object


def main():
  args = grabInputArgs()  # import cmdline/bash argments
  param = ImportConfig(args.configfile)  # import config file into variables
  if args.debug == 1:  # debug mode: print message
    print("PIDLocker started with config file {}.\n".format(args.configfile))


  if args.debug == 1:  # debug mode: print message
    print("-----------------------------------------------")
    print("Channel configuration:")
    print(param.EPICS_channel_config_all)
    print("")
    print("Hard stops and slew rate settings for  actuator:")
    print(param.hard_actuator_limits_all)
    print("-----------------------------------------------")


  # Initiate some variables
  blinkystatus = 0  # initiate blink status, used to show script is alive (if soft channel is in config file)
  
  u_init = RCPID.read(param.actuator, log=False)
  print("Starting  value of actuator = " + str(u_init) + "\n")
  # Variables
  u = [u_init]*4 # outputs to the actuator
  e = [0]*4 # error signal
  
  
  #start controller
  
  while(1):
    timestep = RCPID.read(param.timestep, log=False)
    time.sleep(timestep)  # wait for next iteration of the loop for timestep
    
    blinkystatus = 0 if blinkystatus else 1  # blink the blinky light
    if (not param.EPICSBlinkChan == ""):  # check if a blink channel is given
       RCPID.write(param.EPICSBlinkChan, blinkystatus, monitor=False)  # write out to blink inidicator channel, if it is specified 
    
    if ANDChannels(param.requiredstates):  # if all required binary channels are true activate loop

      #read actuator value
      u[0] = RCPID.read(param.actuator)
    
      #shift right
      u.insert(0,0)
      e.insert(0,0)
    
      # Read the PID parameters in case they have changed, this happens on every loop for robustness to catch all cases
      Kp = RCPID.read(param.KpParam)
      Ki = RCPID.read(param.KiParam)
      Kd = RCPID.read(param.KdParam)
    
      # Scale by the loop update rate
      Ki = Ki*timestep
      Kd = Kd/timestep
    
      # Read the current value of the Process Variable and the Setpoint
      p = RCPID.read(param.process)
#      if not param.setpoint == "":  # case setpoint is specified in .ini file
#        s = RCPID.read(param.setpoint)  # get setpoint from channel
#      else:  # case setpoint is not specified in .ini file
#        s = 0  # set setpoint to zero
      s = RCPID.read(param.setpoint)  # get setpoint from channel
    
      # The basic finite-difference PID approximation
      e[0] = p-s
    
      u[0] = u[1]
      u[0] = u[0] + Ki * (e[0])
      u[0] = u[0] + Kp * (e[0] - e[1])
      u[0] = u[0] + Kd * (e[0] - 2*e[1] + e[2])
    
      # Enforce hard stops and maximum increment
      u[0] = u[1] + rail(u[0] - u[1], -param.increment_limit, param.increment_limit) # imposes maximium increment limit
      u[0] = rail(u[0], param.hard_stops[0], param.hard_stops[1])  # impose hard stop limit
    
      #perform the actuation
      if np.abs(u[0]-u[1])>0.5 and np.abs(u[1]-u[2])<0.5:
          RCPID.write(param.actuator, u[1], monitor=False)
      else:
          RCPID.write(param.actuator, u[0], monitor=False)
    
      # Discard samples sufficiently far in the past
      u.pop()
      e.pop()




def grabInputArgs():  # get input argments passed to the script from the command line
  parser = argparse.ArgumentParser(description="PID control script that reads out and controls EPICS channels")
  parser.add_argument('configfile',type=str,help="Here you must enter a config .ini file name and location that contains the channels names for process, actuator and set point as well as the soft channels for setting PID settings and hard limits. Also included should be an list of channels that must be true for loop to engage")
  parser.add_argument('--debug', action='store_true')
  return parser.parse_args()  # Grabs arguments and puts into a local variable


class ImportConfig():
  def __init__(self,configfile):  # initiate class and generate structure for object to call channel names from
    config = ConfigParser.ConfigParser()
    config.read(configfile)
    self.process  =  config.get("EPICSChannelConfig","process")  # channel name of process to be controls
    self.actuator = config.get("EPICSChannelConfig","actuator")  # channel name of actuator
    self.KpParam = config.get("EPICSChannelConfig","KpParam")  # channel name of PID proportional constant
    self.KiParam = config.get("EPICSChannelConfig","KiParam")  # channel name of PID integral constant
    self.KdParam = config.get("EPICSChannelConfig","KdParam")  # channel name of PID derivative constant
    try:
      self.setpoint =  config.get("EPICSChannelConfig","setpoint")  # channel name of set point value for PID, this is the value the loop will converge to
    except:  # case that user has not specified a setpoint
      self.setpoint = "" # defults setpoint channel value to none, set point will be zero
    try:
      self.timestep = config.get("EPICSChannelConfig","timestep")  # channel name of time step in seconds between loop iterations
    except:  # case user has not specified a soft channel for loop update rate
      self.timestep = 1 # defults time parameter to 1 second (slow control)
    try:
      self.EPICSBlinkChan = config.get("EPICSChannelConfig","EPICSBlinkChan")  # Define soft channel used to blink on front pannel, this is useful to have a soft channel blink a medm indicator.
    except:  # case user has not specified a blink channel defult to none
      self.EPICSBlinkChan = ""
    try:  # attempt to get required states for autolock
      self.requiredstates = config.get("EPICSChannelConfig","requiredstates").splitlines()  # Define channel or list of channels used to define loop activation state.  1 = on , 0 = off. All must be true for loop to be activated. If no channels are listed PID is always on.
    except:
      self.requiredstates = []
    self.increment_limit = config.getfloat("HardActuatorLimits","incrementlimit")  # this is like a slew rate limit on the actuator
    self.hard_stops = [config.getfloat("HardActuatorLimits","hardstops_lower"),config.getfloat("HardActuatorLimits","hardstops_upper")] # load hard stops on channel, this stops actuator going too far

    self.EPICS_channel_config_all = config.items("EPICSChannelConfig")
    self.hard_actuator_limits_all = config.items("HardActuatorLimits")

def rail(n, minn, maxn):  # test if argument is outside and range and rails it to the limit of that range if it goes over
    return max(min(maxn, n), minn)


def ANDChannels(chanList):  # find boolean AND of a list of binary EPICs channels and returns true if all are 1 and false if any are 0
  return all([RCPID.read(ii, log=False) for ii in chanList])


#checks if string looks like a number using regex
def looks_like_number(in_string):
  pattern = re.compile("/^-?\d+\.?\d*$/")
  if pattern.match(in_string) == None:
    return(False)
  else:
    return(True)


if __name__ == "__main__": #triger main on call
  main()
