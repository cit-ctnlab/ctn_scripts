'''
mokuPhaseMeterTimeSeries
This function runs a time series measurement of phase phasemeter in Moku.
Input Arguments:
chan:       {'ch1'(default),'ch2' or 'both'}. Case insensitive.
ipAddress:  IP Address of Moku. Default is 10.0.1.81.
duration:   Duration of measurement in seconds.
sampleRate: {'veryslow', 'slow', 'medium', 'fast'(default), 'veryfast',
            'ultrafast'} corresponding to {30.5176 smp/s, 122.0703 smp/s,
            488smp/s, 1.9531 ksmp/s(default), 15.625 ksmp/s, 125 ksps/s}.
            Case insensitive.
bandWidth:  {10, 40, 150, 600, 2.5e3, 10e3(default)} in Hz. (Will be rounded
            up to to the nearest multiple 10kHz / 2^N with N = [0,10]). Can be
            a list for different values for different channels.
fiftyr:     50 Ohm inputs. {True, False(default)}. Can be a list for different
            values for different channels.
atten:      Turn on 10x attenuation. {True, False(default)}. Changes the
            dynamic range between 1Vpp(False) and 10Vpp(True). Can be a list
            for different calues for different channels.
ac:         AC coupling. {True, False(Default)}. Can be a list for different
            values for different channels.
altFileRoot: Moku's default fileroot is MokuPhasemeterData which can be
             replaced by providing an alternate file root. Default is None.
useExternal: Attempt to lock to an external reference clock.
             {True(default), False}.
useSD:       Whether to log to the SD card, else the internal Moku filesystem.
             {True, False(default)}. Using SD card limits sampling rate.
fileType:    Log file type. {'csv'(default),'bin','mat','npy'} corresponding to
             {CSV(default), Binary, MATLAB, NPY (Numpy Data)} respectively.
outChan:     {'ch1','ch2', 'none'(default) or 'both'}. Case insensitive.
wf:          Waveform type. {'sine'(default), 'ramp' or 'square'}. Case
             insensitive. Can be a list for different values for different
             channels. Currently moku with phasemeter only supports 'sine'
amp:         Amplitude in volts. {float, default=0.1}. Can be a list for
             different values for different channels.
freq:        Frequency in Hz. {float, default=10e3}. Can be a list for
             different values for different channels.
phase:       Phase offset of the wave. {float, [0-360] degrees, default=0.0}.
             Can be a list for different values for different channels.
phaseLocked: Locks the phase of the generated sinewave to the measured phase of
             the input signal. Default is False. Can be a list for different
             values for different channels.
verbose:     If True, print statements will get activated.
mokuName:    If used, moku will be searched for by name.
Output:
dataFileName: File name of the measurement data.
'''

import os
import time
import pymoku
from pymoku import Moku, StreamException
from pymoku.instruments import Phasemeter
from mokuGetLastFile import mokuGetLastFile
import argparse
import numpy as np

pymoku._FS_CHUNK_SIZE = 1024 * 512


def mokuPhaseMeterTimeSeries(chan='ch1', ipAddress='10.0.1.81', duration=60,
                             sampleRate='fast', bandWidth=10e3,
                             fiftyr=True, atten=False, ac=False,
                             altFileRoot=None, useExternal=True, useSD=False,
                             fileType='csv', outChan='none',  wf='sine',
                             amp=0.1, freq=10e3, phase=0.0,
                             phaseLocked=False, verbose=True,
                             mokuName=None):
    chan = chan.lower()
    if chan == 'ch1':
        ch1 = True
        ch2 = False
    elif chan == 'ch2':
        ch1 = False
        ch2 = True
    elif chan == 'both':
        ch1 = True
        ch2 = True
    chList = [ch1, ch2]

    if outChan == 'ch1':
        outCh1 = True
        outCh2 = False
    elif outChan == 'ch2':
        outCh1 = False
        outCh2 = True
    elif outChan == 'both':
        outCh1 = True
        outCh2 = True
    elif outChan == 'none':
        outCh1 = False
        outCh2 = False
    outChList = [outCh1, outCh2]

    if not isinstance(wf, list):
        wf = [wf.lower(), wf.lower()]
    else:
        wf = [s.lower() for s in wf]
    if not isinstance(amp, list):
        amp = [amp, amp]
    if not isinstance(freq, list):
        freq = [freq, freq]
    if not isinstance(phase, list):
        phase = [phase, phase]
    if not isinstance(phaseLocked, list):
        phaseLocked = [phaseLocked, phaseLocked]

    sampleRate = sampleRate.lower()
    if not isinstance(bandWidth, list):
        bandWidth = [bandWidth, bandWidth]
    if not isinstance(fiftyr, list):
        fiftyr = [fiftyr, fiftyr]
    if not isinstance(atten, list):
        atten = [atten, atten]
    if not isinstance(ac, list):
        ac = [ac, ac]
    duration = int(np.ceil(duration))  # Duration has to be integer in sec

    dataFileName = 'Error'
    try:
        if mokuName is None:
            m = Moku(ipAddress)
        else:
            m = Moku.get_by_name(mokuName)
        i = m.deploy_instrument(Phasemeter, use_external=useExternal)
        i.set_defaults()
        for ii, outCh in enumerate(outChList):
            chNo = ii+1
            if outCh:
                if wf[ii] == 'sine':
                    i.gen_sinewave(chNo, amplitude=amp[ii], frequency=freq[ii],
                                   phase=phase[ii],
                                   phase_locked=phaseLocked[ii])
                    if verbose:
                        print('Ch', chNo, ': Sine wave of Amplitude=',
                              amp[ii], 'V at ', ftoss(freq[ii]) + 'Hz')
                '''
                Following can be uncommented once moku phasemeter supports
                more waveform types.
                elif wf[ii]=='ramp':
                    i.gen_rampwave(chNo, amplitude=amp[ii], frequency=freq[ii],
                                   offset=offset[ii], phase=phase[ii])
                    print('Ch',chNo,': Ramp wave of Amplitude=',
                          amp[ii],'V at ', ftoss(freq[ii])+'Hz')
                elif wf[ii]=='square':
                    i.gen_squarewave(chNo, amplitude=amp[ii],
                                     frequency=freq[ii], offset=offset[ii],
                                     phase=phase[ii])
                    print('Ch',chNo,': Square wave of Amplitude=',
                          amp[ii],'V at ', ftoss(freq[ii])+'Hz')
                '''
            else:
                i.gen_off(chNo)
                if verbose:
                    print('Switching off Output Channel', chNo)

        for ii, ch in enumerate(chList):
            if ch:
                chNo = ii+1
                # Set front end settings
                i.set_frontend(chNo, fiftyr=fiftyr[ii], atten=atten[ii],
                               ac=ac[ii])
                # Set bandwidth
                i.set_bandwidth(chNo, bandWidth[ii])
                # Auto-acquire current signal
                i.auto_acquire(chNo)

        # Set sampling rate
        i.set_samplerate(sampleRate)

        # Start data logging
        i.start_data_log(duration=duration, use_sd=useSD, ch1=ch1, ch2=ch2,
                         filetype=fileType)
        # Track progress percentage of the data logging session
        progress = 0
        while progress < 100:
            # Wait for the logging session to progress
            time.sleep(duration/10)
            # Get current progress percentage and print it out
            progress = i.progress_data_log()
            if verbose:
                print('Progress {}%'.format(progress))
        try:
            # Upload the log file to the local directory
            i.upload_data_log()
            if verbose:
                print('Uploaded log file to local directory.')
            # Denote that we are done with the data logging session so
            # resources may be cleand up
            i.stop_data_log()
            # Rename logged datafile to BeatnoteSpec_
            if fileType == 'bin':
                fileExt = 'li'
            else:
                fileExt = fileType
            defFileName = i.data_log_filename() + '.' + fileExt
        except BaseException as e:
            print('Error occured:', e)
            print('Automatic file upload failed.',
                  'Trying to access measured file.')
            try:
                time.sleep(10)
                defFileName = mokuGetLastFile(ipAddress, mokuName)
            except BaseException as e:
                print('Error occured:', e)
                defFileName = 'Error'
        if defFileName != 'Error':
            if altFileRoot is not None:
                dataFileName = defFileName.replace('MokuPhasemeterData',
                                                   altFileRoot)
                os.rename(defFileName, dataFileName)
            else:
                dataFileName = defFileName
            m._delete_file('i', defFileName)
    except StreamException as e:
        dataFileName = 'Error'
        print('Error occured: %s' % e)
    except BaseException as e:
        dataFileName = 'Error'
        print('Error occured: %s' % e)
    finally:
        try:
            m.close()
        except BaseException:
            print('Failed to connect to moku properly.')
        finally:
            return dataFileName


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script runs a time series measurement of phase'
                    'using phasemeter in Moku.')
    parser.add_argument('-c', '--chan', type=str,
                        help='ch1(default), ch2 or both. Case insensitive.',
                        default='ch1')
    parser.add_argument('-i', '--ipAddress',
                        help='IP address of Moku. Default is 10.0.1.81',
                        default='10.0.1.81')
    parser.add_argument('-b', '--bandWidth', type=float,
                        help='Phasemeter tracking bandwidth.'
                             'Default is 10kHz.',
                        default=10e3)
    parser.add_argument('--bandWidth2', type=float,
                        help='Phasemeter tracking bandwidth for channel 2',
                        default=None)
    parser.add_argument('-d', '--duration', type=int,
                        help='Measurement time in seconds. Default 60.',
                        default=60)
    parser.add_argument('-s', '--sampleRate', type=str,
                        help='Sampling Rate. {veryslow, slow , medium ,'
                             'fast, veryfast, ultrafast}. Refer moku manual.'
                             'Default is fast.',
                        default='fast')
    parser.add_argument('--MOhmr',
                        help='1 MOhm input impedance. Default: False',
                        action='store_true')
    parser.add_argument('--MOhmr2',
                        help='1 MOhm input impedance for Ch 2. Default: False',
                        action='store_true')
    parser.add_argument('-a', '--atten',
                        help='10x Attenuation. True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('--atten2',
                        help='10x Attenuation for Ch2.'
                             'True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('--ac',
                        help='AC Coupling. Default: False',
                        action='store_true')
    parser.add_argument('--ac2',
                        help='AC Coupling for channel 2. Default: False',
                        action='store_true')
    parser.add_argument('--altFileName', type=str,
                        help='Alternate File Root Name.',
                        default=None)
    parser.add_argument('--useInternal',
                        help='Use internal 10 MHz clock only. Default: False',
                        action='store_true')
    parser.add_argument('--useSD',
                        help='Use SD Card for storing data. Default: False',
                        action='store_true')
    parser.add_argument('--fileType', type=str,
                        help='File Type: csv(default), bin, mat, npy',
                        default='csv')
    parser.add_argument('--outChan', type=str,
                        help='ch1, ch2, none(default) or both.'
                             'Case insensitive.',
                        default='none')
    parser.add_argument('--wf1', type=str,
                        help='Channel 1 waveform. Ramp, Sine, Square.'
                             'Case insensitive. Default is Sine. Currently '
                             'moku phasemeter only supports sinewave',
                        default='sine')
    parser.add_argument('--wf2', type=str,
                        help='Channel 2 waveform. Ramp, Sine, Square.'
                             'Case insensitive. Default is Sine. Currently '
                             'moku phasemeter only supports sinewave',
                        default='sine')
    parser.add_argument('--amp1', type=float,
                        help='Channel 1 amplitude in volts. Default 0.1',
                        default=0.1)
    parser.add_argument('--amp2', type=float,
                        help='Channel 2 amplitude in volts. Default 0.1',
                        default=0.1)
    parser.add_argument('--f1', type=float,
                        help='Channel 1 frequency in Hz. Default 10e3',
                        default=10e3)
    parser.add_argument('--f2', type=float,
                        help='Channel 2 frequency in Hz. Default 10e3',
                        default=10e3)
    parser.add_argument('--phase1', type=float,
                        help='Channel 1 phase in degrees. Default 0',
                        default=0)
    parser.add_argument('--phase2', type=float,
                        help='Channel 2 phase in degrees. Default 0',
                        default=0)
    parser.add_argument('--phaseLocked1',
                        help='Locks the phase of the generated sinewave on Ch1'
                             ' to the measured phase of the input signal. '
                             'Default is False.',
                        action='store_true')
    parser.add_argument('--phaseLocked2',
                        help='Locks the phase of the generated sinewave on Ch2'
                             ' to the measured phase of the input signal. '
                             'Default is False.',
                        action='store_true')
    parser.add_argument('--mokuName', type=str,
                        help='Connect to Moku by Name.',
                        default=None)
    return parser.parse_args()


def ftoss(value):
    sgn = np.sign(value)
    if sgn == 0:
        return '0'
    else:
        absv = np.abs(value)
        ex = np.minimum(12, np.maximum(-15, np.floor(np.log10(absv))))
        sv = absv/(10**ex)
        sc = str(int(np.floor(ex/3)))
        unit = {
            '-5': 'f',
            '-4': 'p',
            '-3': 'n',
            '-2': 'u',
            '-1': 'm',
            '0': '',
            '1': 'k',
            '2': 'M',
            '3': 'G',
            '4': 'T'
        }[sc]
        val = str(sgn*sv*10**(ex % 3))
        return val+unit


if __name__ == '__main__':
    args = grabInputArgs()
    fiftyr = [not args.MOhmr, not args.MOhmr2]
    atten = [args.atten, args.atten2]
    ac = [args.ac, args.ac2]
    wf = [args.wf1, args.wf2]
    amp = [args.amp1, args.amp2]
    freq = [args.f1, args.f2]
    phase = [args.phase1, args.phase2]
    phaseLocked = [args.phaseLocked1, args.phaseLocked2]
    if args.bandWidth2 is None:
        bandWidth = args.bandWidth
    else:
        bandWidth = [args.bandWidth, args.bandWidth2]
    output = mokuPhaseMeterTimeSeries(chan=args.chan,
                                      ipAddress=args.ipAddress,
                                      duration=args.duration,
                                      sampleRate=args.sampleRate,
                                      bandWidth=bandWidth,
                                      fiftyr=fiftyr, atten=atten, ac=ac,
                                      altFileRoot=args.altFileName,
                                      useExternal=not args.useInternal,
                                      useSD=args.useSD,
                                      fileType=args.fileType,
                                      outChan=args.outChan,
                                      wf=wf, amp=amp, freq=freq, phase=phase,
                                      phaseLocked=phaseLocked,
                                      mokuName=args.mokuName)
    print('Data File Name:', output)
