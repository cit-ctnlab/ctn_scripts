#!/usr/bin/env/ python

# Author: Andrew Wade
# Date: 2017 1 September
# Comment: This is the same as autolocker.py but incrementally increases the scan range on each attempt. The idea is to start by looking around the last known lock point and then sweep wider.  This should improve duty cycle in cases where lock loss is a temporary glitch.

#Issues: Needs a log file output to track events states so lock loss conditions can be diagnosed
#        - Much of the preamble can be pushed out into functions at the bottom, would be even better if could initialize object with obj.variable calling to reduce number of global varibles floating around

import re
import time
import os
import sys
from ezca import Ezca
import ConfigParser
import argparse

CheckPeriod = 5  # hard code value for how long to wait between rechecking threshold status is met


# Configure argument parser for cmdline calls of script
parser = argparse.ArgumentParser(description='Auto locker script for PDH locker, steps actuator until threshold is reached and then engages lock if auxilary conditions are met. If no binary auxillary conditions are defined then assumed condition is met')
parser.add_argument('configfile',type=str,help='Here you must enter a config file that contains the channels for error signal, actuator, engauge and hard limits. You may also optionally specify binary channels, the autolocker will only engauge when these are in the 1 state')
#parser.add_argument('--chanCondition',action='append)
parser.add_argument('--debug', action='store_true')
args = parser.parse_args()  # Grabs arguments and puts into a local variable

# Grab config file settings and put in local variables
config = ConfigParser.ConfigParser()
config.read(args.configfile)

monitor = config.get("EPICSChannelConfig","monitor") # Channel to monitor for threshold of lock
actuator = config.get("EPICSChannelConfig","actuator") # Actuator to drive towards lock
loopStateEnable = config.get("EPICSChannelConfig","loopStateEnable") # Binary channel used to engage channel
EPICSBlinkChan = config.get("EPICSChannelConfig","EPICSBlinkChan") # Define soft channel used to blink medm indicator for script living.  Value of zero skips this
requiredstates = config.get("EPICSChannelConfig","requiredstates") # Optional list of binary channels that veto lock engauge if in state zero.  If nothing defults to state 1 (always lock on threshold condition


thresholdVal = config.getfloat("SearchSweepRateParameters","thresholdVal") # value over which lock condition should be engauged (later this should be updated to be a range)
stepRate = config.getfloat("SearchSweepRateParameters","stepRate") # time between actuator steps when searching for lock point (seconds)
stepSize = config.getfloat("SearchSweepRateParameters","stepSize") # size of steps in search
loopsToFullRange = config.get("SearchSweepRateParameters","loopsToFullRange") # Experimental feature, search sweeps increase in range to the full sweep range over loopsTeoFullRange.  Set to 1 to set autolocker to just do full range. Set to larger (interger) number to take longer to get to full range. 
sweepRange = [config.getfloat("SearchSweepRateParameters","sweepRangeUpper"),config.getfloat("SearchSweepRateParameters","sweepRangeLower")] # set bounds on search range
numAttempts = config.get("SearchSweepRateParameters","numAttempts")  # NOT IMPLEMENTED YET: Number of times to sweep range before giving up, default zero is forever

# Handle defult events (there maybe a smarter way to do this with config parser, for now this wil just be if/then statements
if requiredstates == "0":  # temporary dummy variables until multi channel AND can be performed from ini file channel values
  AutoLockerEnable = 1  # there will be a loop engauge when 1's are reported on all required binary channels.  


if args.debug == 1:  # debug mode: display config values as they were read in
  print("----------------------------------------")
  print("Channel configuration:")
  print(config.items("EPICSChannelConfig"))
  print(".")
  print("Search Parameters:")
  print(config.items("SearchSweepRateParameters"))
  print("----------------------------------------")




ACC = Ezca(ifo=None) # Initialize ezca python access to channels object

def main():
  while(1):  # Start searching for lock point
    if abs(ACC.read(monitor, log=False))>thresholdVal:  # while monitor is above threshold leave loop locked and poll periodically 
      time.sleep(CheckPeriod)  # wait for CheckPeriod seconds
    else: # case threshold is not met
      ACC.write(loopStateEnable,0,monitor=False)  # drop the lock engage
      buffRange = sweepRange  ### FIX THIS 
      while(threshSearch(sweepRange) == 0):  # loop over the threshold sweeper until it hits affirmative exit state
        pass  # no action
      else:  # exitstate has been met
        ACC.write(loopStateEnable,1,monitor=False)  # re engauge lock 



# Search for the threshold return when reached or after one full range has been cycled
def threshSearch(srange):
  swpDir = -1.0 # Set the direction of sweep, defult is start downward  
  while(abs(ACC.read(monitor, log=False))<thresholdVal): # start searching. Takes absolute to cope with inverting amplifiers.
    time.sleep(stepRate) # wait a predetermined time before stepping to next value
    ActuatorState = ACC.read(actuator, log=False) #Grabs current actuator state as a starting point, this is done on every loop so that the script is always working from real value. Someone might tweek the sliders or another script may change value, we want our script to step real values.
    ActuatorState_stepped = rail(ActuatorState + swpDir * stepSize,srange[0],srange[1])
    ACC.write(actuator, ActuatorState_stepped, monitor=False) # write out to channel next step value
    if ActuatorState_stepped == ActuatorState: # check if railed, change direction
      swpDir = swpDir + 2.0 # Flips sign of sweep direction or puts it in the exit state of 3.0
    if swpDir == 3.0: # If sweep direction value is three then a full scan range has taken place, exit loop
      exitstate = 0  # exit state 0 means failed to find threshold
      break  # break out of while loop to return function value
  else:  # while exit condition of threshold no longer exeeding monitor channel, exit with affirmative state
    exitstate = 1  # exit state 1 means threshold has been met    
  return exitstate
  


# Check number within range and rail it to limits
def rail(n, minn, maxn):
    return max(min(maxn, n), minn)

if __name__ == "__main__": #triger input parser on call
  mai()
