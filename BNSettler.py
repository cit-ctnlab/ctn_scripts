
from time import sleep
# from ezca import Ezca
import ConfigParser
import argparse
import numpy as np

from epics import caget, caput

'''
Author: Andrew Wade
Date: 2018 17 September

This is a simple script for settling the CTN beatnote down to 26.0 Mhz
while we figure out how to intelligently control thermal systems with large
and slow non-linear responses
'''

# Input argument parser
parser = argparse.ArgumentParser(
    description='This script finds and settles the BN from a known good '
                'starting point.  It is an initial BN settler before we'
                'get the full intelligent controls up and running.')
parser.add_argument(
    'configfile',
    type=str,
    help='Here you must enter a config file that contains the channels for'
         'acting on the BN and adjusting PID values')
parser.add_argument(
    '--debug',
    action='store_true')
args = parser.parse_args()  # Grabs arguments and puts into a local variable


#  Grab config file settings and put in local variables
config = ConfigParser.ConfigParser()
config.read(args.configfile)

chan_PreCavBN = config.get("EPICSChannelConfig",
                           "chan_PreCavBN")
chan_DiffHeater = config.get("EPICSChannelConfig",
                             "chan_DiffHeater")
chan_HeaterLoopStateEnable = config.get("EPICSChannelConfig",
                                        "chan_HeaterLoopStateEnable")
chan_HeaterPID_Setpoint = config.get("EPICSChannelConfig",
                                     "chan_HeaterPID_Setpoint")
chan_HeaterPID_P = config.get("EPICSChannelConfig",
                              "chan_HeaterPID_P")
chan_HeaterPID_I = config.get("EPICSChannelConfig",
                              "chan_HeaterPID_I")
chan_HeaterPID_D = config.get("EPICSChannelConfig",
                              "chan_HeaterPID_D")
try:    # get required states for autolock or default none
    requiredstates = config.get("EPICSChannelConfig",
                                "requiredstates").splitlines()
except Exception:
    requiredstates = []


pollRate = config.getfloat("ProcessControl", "pollRate")  # sec between checks
LPCorner = config.getfloat("ProcessControl", "LPCorner")  # corner freq LP
ConvergenceTime = config.getfloat("ProcessControl", "ConvergenceTime")

# pollRate = 1.0
# LPCorner = 0.01  # currently 100 seconds of averaging
# ConvergenceTime = 1000  # time to wait before case not converging, restart
# ACC = Ezca(ifo=None, logger=False)  # Initialize ezca python object

LockingSteps = {0: {'freqThresh': 500.0,
                    'freqTol': 300.0,
                    'diffHeaterIniSet': 0.7740,
                    'PIDSetpoint': 500.0,
                    'PID_P': 0.0,
                    'PID_I': 0.0,
                    'PID_D': 0.0},
                1: {'freqThresh': 500.0,
                    'freqTol': 10.0,
                    'diffHeaterIniSet': None,
                    'PIDSetpoint': 500.0,
                    'PID_P': -0.00025,
                    'PID_I': -0.00001,
                    'PID_D': 0.0},
                2: {'freqThresh': 300.0,
                    'freqTol': 5.0,
                    'diffHeaterIniSet': None,
                    'PIDSetpoint': 300.0,
                    'PID_P': -0.0025,
                    'PID_I': -0.00001,
                    'PID_D': 0.0},
                3: {'freqThresh': 100.0,
                    'freqTol': 35.0,
                    'diffHeaterIniSet': None,
                    'PIDSetpoint': 100.0,
                    'PID_P': -0.0025,
                    'PID_I': -0.00001,
                    'PID_D': 0.0},
                4: {'freqThresh': 75.0,
                    'freqTol': 5.0,
                    'diffHeaterIniSet': None,
                    'PIDSetpoint': 75.0,
                    'PID_P': -0.0025,
                    'PID_I': -0.00001,
                    'PID_D': 0.0},
                5: {'freqThresh': 50.0,
                    'freqTol': 3.0,
                    'diffHeaterIniSet': None,
                    'PIDSetpoint': 50.0,
                    'PID_P': -0.0025,
                    'PID_I': -0.00001,
                    'PID_D': 0.0},
                6: {'freqThresh': 35.0,
                    'freqTol': 2.0,
                    'diffHeaterIniSet': None,
                    'PIDSetpoint': 35.0,
                    'PID_P': -0.0025,
                    'PID_I': -0.00001,
                    'PID_D': 0.0},
                7: {'freqThresh': 28.0,
                    'freqTol': 3.0,
                    'diffHeaterIniSet': None,
                    'PIDSetpoint': 28.0,
                    'PID_P': -0.0025,
                    'PID_I': -0.000002,
                    'PID_D': 0.0},
                8: {'freqThresh': 28.0,
                    'freqTol': 0.001,
                    'diffHeaterIniSet': None,
                    'PIDSetpoint': 28.0,
                    'PID_P': -0.0025,
                    'PID_I': -0.00001,
                    'PID_D': 0.0}
                }


# ACC.read()
# initiate BN average values
BNAvg = caget(chan_PreCavBN)


def main():
    global BNAvg
    while(1):
        # Find best lock level to start with (defaulting to level zero)
        iniLockLevel = 0
        for LockLevel, LockPar in LockingSteps.items():
            if abs(BNAvg - LockPar['freqThresh']) <= LockPar['freqTol']:
                iniLockLevel = LockLevel
                print(BNAvg)
                print('Initial lock level {} determined'.format(iniLockLevel))

        while(Apples(iniLockLevel=iniLockLevel)):
            pass
        UpdateBNAvg()  # update the BN average value


def Apples(iniLockLevel=0):
    global BNAvg
    '''Loop over sequence of lock levels conditional on thresholds reached.
       Skips over levels below iniLockLevel to prevent long resets'''
    for LockLevel, LockPar in LockingSteps.items():
        if not ANDChannels(requiredstates):  # return false when req not met
            return False

        if LockLevel < iniLockLevel:  # skip over lock levels below init
            continue

        print('Lock level {} reached'.format(LockLevel))

        # Set initial heater value, if none given then time to start PID
        if LockPar['diffHeaterIniSet'] is not None:
            caput(chan_DiffHeater, LockPar['diffHeaterIniSet'])
            caput(chan_HeaterLoopStateEnable, False)
        else:
            caput(chan_HeaterLoopStateEnable, True)
        caput(chan_HeaterPID_Setpoint, LockPar['PIDSetpoint'])
        caput(chan_HeaterPID_P, LockPar['PID_P'])
        caput(chan_HeaterPID_I, LockPar['PID_I'])
        caput(chan_HeaterPID_D, LockPar['PID_D'])

        timeCounter = 0
        while(abs(BNAvg - LockPar['freqThresh']) > LockPar['freqTol']):
            # count itt, if time exceeds threshold then break and reset search
            timeCounter += 1
            if timeCounter >= int(ConvergenceTime * pollRate):
                return False

            if not ANDChannels(requiredstates):  # break return function False
                return False
            UpdateBNAvg()  # update the BN average value

    return True  # return true if reached final step and threshold met


def UpdateBNAvg():
    global BNAvg
    BNAvg = LPFilter(caget(chan_PreCavBN),
                     BNAvg,
                     LPCorner,
                     pollRate)
    sleep(pollRate)
    return BNAvg


def LPFilter(xi, yp, fc, deltaT):
    alpha = deltaT/(deltaT+1/2/np.pi/fc)
    return yp * (1-alpha) + xi * alpha


def ANDChannels(chanList):
    '''Find boolean AND of a list of binary EPICs channels and returns true
       if all are 1 and false if any are 0 '''
    return all([caget(ii) for ii in chanList])


def rail(n, minn, maxn):    # Check number within range and rail it to limits
        return max(min(maxn, n), minn)


if __name__ == "__main__":  # triger main on call
    main()
