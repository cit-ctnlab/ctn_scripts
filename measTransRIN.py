from SRmeasure import main as SRmeasure
from epics import caget
import os
import argparse

STransDCCH = 'C3:PSL-SCAV_TRANS_DC'
NTransDCCH = 'C3:PSL-NCAV_TRANS_DC'


def measTransRIN(shortMeas='/home/controls/Git/cit_ctnlab/'
                           'ctn_scripts/TransRINSpecShort.yml',
                 longMeas='/home/controls/Git/cit_ctnlab/'
                          'ctn_scripts/TransRINSpecLong.yml',
                 filename=None, numAvg=None):
    SDCval = caget(STransDCCH)
    NDCval = caget(NTransDCCH)
    header = ('North DC Val: ' + str(NDCval) + ' Volts'
              + 'South DC Val: ' + str(SDCval) + ' Volts')
    fn2 = SRmeasure(paramFile=shortMeas,
                    extraHeader=header,
                    filename=filename, numAvg=numAvg)
    if fn2 == 'Control-C':
        return 'Error'
    SDCval = caget(STransDCCH)
    NDCval = caget(NTransDCCH)
    header = ('North DC Val: ' + str(NDCval) + ' Volts'
              + 'South DC Val: ' + str(SDCval) + ' Volts')
    fn1 = SRmeasure(paramFile=longMeas,
                    extraHeader=header,
                    filename=filename, numAvg=numAvg)
    if fn1 == 'Control-C':
        os.remove(fn2)
        return 'Error'
    with open(fn1, 'r') as f:
        fn1lines = f.readlines()
    with open(fn2, 'r') as f:
        fn2lines = f.readlines()
    for ind in range(len(fn2lines)):
        if fn2lines[ind].find('--- Measurement Data ---') != -1:
            break
    headerEnd = ind + 1
    dataStart = ind + 2
    fn1LastFreq = float(fn1lines[-1].split(' ')[0])
    for ind in range(dataStart, len(fn2lines)):
        freq = float(fn2lines[ind].split(' ')[0])
        if freq >= fn1LastFreq:
            break
    dataCont = ind
    allLines = (fn1lines
                + ['#\n', '#\n']
                + fn2lines[:headerEnd+1]
                + fn2lines[dataCont:])
    os.remove(fn1)
    os.remove(fn2)
    with open(fn1, 'w') as f:
        f.writelines(allLines)
    return fn1


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script takes a transimission RIN measurement.')
    parser.add_argument('--configShort', type=str,
                        help='Short measurement configuration file.',
                        default='/home/controls/Git/cit_ctnlab/'
                             'ctn_scripts/TransRINSpecShort.yml')
    parser.add_argument('--configLong', type=str,
                        help='Long measurement configuration file.',
                        default='/home/controls/Git/cit_ctnlab/'
                             'ctn_scripts/TransRINSpecLong.yml')
    parser.add_argument('-f', '--filename', type=str,
                        help='Alternate File Root Name.',
                        default=None)
    parser.add_argument('-n', '--numAvg', type=int,
                        help='Number of averages if different from config '
                             'file.',
                        default=-1)
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    if args.numAvg == -1:
        args.numAvg = None
    fn = measTransRIN(shortMeas=args.configShort,
                      longMeas=args.configLong,
                      filename=args.filename, numAvg=args.numAvg)
    print('Data written at ' + fn)
