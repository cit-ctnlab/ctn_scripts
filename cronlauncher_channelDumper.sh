#!/bin/bash

# LIGO EPICS enviroment variables
export EPICS_HOST_ARCH=linux-x86_64
export epicstop=/ligo/apps/ubuntu12/epics-3.14.12.2_long
export EPICS_BASE=${epicstop}/base
export EPICS_BASEDIR=${epicstop}/base
export EPICS_MODULES=${epicstop}/modules
export EPICS_SEQ=${EPICS_MODULES}/seq
export EPICS_EXTENSIONS=${epicstop}/extensions
export EPICSBIN=${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${EPICS_EXTENSIONS}/bin/${EPICS_HOST_ARCH}
export EPICSLIB=${EPICS_BASE}/lib/${EPICS_HOST_ARCH}:${EPICS_EXTENSIONS}/lib/${EPICS_HOST_ARCH}
export PYEPICS_LOCATION=${epicstop}/pyext/pyepics
export PCASPY_LOCATION=${epicstop}/pyext/pcaspy
export EPICS_DB_INCLUDE_PATH=${epicstop}/base/dbd
export RPN_DEFNS=${epicstop}/etc/defns.rpn

export PATH=${EPICS_EXTENSIONS}/bin/${EPICS_HOST_ARCH}:${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${EPICS_SEQ}/bin/${EPICS_HOST_ARCH}:${PATH}
LD_LIBRARY_PATH=${EPICS_EXTENSIONS}/lib/${EPICS_HOST_ARCH}:${EPICS_BASE}/lib/${EPICS_HOST_ARCH}:${EPICS_SEQ}/lib/${EPICS_HOST_ARCH}:${LD_LIBRARY_PATH}

exec /usr/bin/python /home/controls/scripts/channelDumper.py
