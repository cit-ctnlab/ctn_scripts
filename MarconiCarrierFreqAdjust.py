#! /usr/bin/env python
# Craig Cahillane - November 7, 2017

import os
import sys
import time
import select
import argparse
import netgpib
from ezca import Ezca

# Usage text
usage = """
python MarconiCarrierFreqAdjust.py -c <CARRIER FREQ IN MHz> -i <IP_ADDRESS> -a <GPIB_ADDRESS> 

Example. To set Marconi carrier frequency to 100 MHz:
python Marconi2023A_BeatnoteTrack.py -i 10.0.1.63 -a 17 -c 100
"""

# Parse options
parser = argparse.ArgumentParser(usage=usage)
parser.add_argument('-a', '--gpibaddress', type=int, default=17,
                  help='GPIB device address.  Default is 17')
parser.add_argument('-i', '--ip', default='10.0.1.63',
                  help='IP address/Host name.  Default is 10.0.1.63')
parser.add_argument('-c', '--carrier', type=float, 
                  help='Carrier Frequency in Hz. Default is current Marconi settings.')
args = parser.parse_args()

##################################################

# Create/connect to netGPIB object
print 'Connecting to %s...' % (args.ip),
gpibObj = netgpib.netGPIB(args.ip,
                          args.gpibaddress,
                          '\004', 0,
                          log=args.log)
print 'Done'

# Set user defined arguments
if args.carrier is not None: # Change to user defined initial carrier frequency
    carrierFreqStr = str(args.carrier)
    cmd = 'CFRQ ' + carrierFreqStr
    gpibObj.command(cmd)
#if args.rflevel is not None: # Change to user defined radio frequency signal level
#    rfStr = str(args.rflevel)
#    cmd = 'RFLV ' + rfStr 
#    gpibObj.command(cmd)
#if args.fm is not None: # Change to user defined frequency modulation
#    fmStr = str(args.fm)
#    cmd = 'FM:DEVN ' + fmStr
#    gpibObj.command(cmd)

# Set up the ezca channel
channel = 'C3:PSL-PLL_CONTROL_SIGNAL'
ez = Ezca(ifo=None)
curCV = ez.read(channel)
# Query the Marconi for initial settings
queryCF = gpibObj.query('CFRQ?')
curCF = float(queryCF.split(';')[0].split(' ')[-1])
queryFM = gpibObj.query('FM:DEVN?')
curFM = float(queryFM)*0.7071068  #awade: added correction to Vpp units

# ez.write to the soft PLL channels
channelCF = 'C3:PSL-PLL_CARRIER_FREQ'
channelFM = 'C3:PSL-PLL_FM_DEVN'
ez.write(channelCF, curCF*1e-6)
ez.write(channelFM, curFM*1e-3)

# This variable controls what way we push the carrier frequency when the control voltage
# starts to get too high or too low
# For the PSL lab right now, decreasing the carrier freq increases the control voltage.
if args.actuation is not None:
    actuationDirection = args.actuation
else:
    actuationDirection = 1.0

# These variables are the voltage rails at which the PLL will lose lock.
if args.topRail is not None:
    highRail = args.topRail
else:
    highRail = 2.0 # PLL control signal volts
if args.bottomRail is not None:
    lowRail = args.bottomRail
else:
    lowRail = -2.0 # PLL control signal volts
railRange = highRail - lowRail

lockLoss = False # If we run out of the rail range, switch lock loss to True and try to reacquire
startTime = time.time()
while True:    
    controlVoltage = ez.read(channel)
    controlPercent = (controlVoltage - lowRail) / railRange # calculate how far through the range we are, normalized
    if controlPercent < 0.2: # if we are on the lower fifth of the range
        # Get carrier frequency and frequency modulation
        queryFM = gpibObj.query('FM:DEVN?')
        curFM = float(queryFM)*0.7071068
        queryCF = gpibObj.query('CFRQ?')
        curCF = float(queryCF.split(';')[0].split(' ')[-1])
        
        # Set Marconi to new carrier frequency
        curCF -= actuationDirection * curFM
        cmd = 'CFRQ ' + str(curCF)
        gpibObj.command(cmd)

        # Write new carrier frequency to soft epics channels
        ez.write(channelCF, curCF*1e-6)
        ez.write(channelFM, curFM*1e-3)

    elif controlPercent > 0.8: # if we are on the upper fifth of the range
        # Get carrier frequency and frequency modulation
        queryFM = gpibObj.query('FM:DEVN?')
        curFM = float(queryFM)*0.7071068
        queryCF = gpibObj.query('CFRQ?')
        curCF = float(queryCF.split(';')[0].split(' ')[-1])
        
        # Set Marconi to new carrier frequency
        curCF += actuationDirection * curFM
        cmd = 'CFRQ ' + str(curCF)
        gpibObj.command(cmd)

        # Write new carrier frequency to soft epics channels
        ez.write(channelCF, curCF*1e-6)
        ez.write(channelFM, curFM*1e-3)

    if controlPercent < 0.0 or controlPercent > 1.0:
        time.sleep(2)
        if lockLoss == False:
            print 'PLL may have lost lock'
            print 'Trying to reacquire...'
            lockLoss = True
            continue
        elif lockLoss == True:
            print 'Reacquire failed'
            print 'Exiting script...'
            break

    lockLoss = False # If we reach here, lockLoss is not activated
    os.system('cls' if os.name == 'nt' else 'clear')
    print 'I am locking the PLL.'
    print 'Press ENTER to stop me!  Please do not press Control-C.'
    print
    os.system('cls' if os.name == 'nt' else 'clear')
    print 'I am locking the PLL.'
    print 'Press ENTER to stop me!  Please do not press Control-C.'
    print
    print 'Carrier Frequency is ' + str(curCF*1.0e-6) + ' MHz'
    print 'Time: ' + str((time.time() - startTime)/3600.0) + ' Hours'
    if sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
        line = raw_input()
        break
    time.sleep(0.2)
channelNCAVHeaterPID = 'C3:PSL-HEATER_PID_SHIELD_NCAV_EN'
ez.write(channelNCAVHeaterPID, 'OFF')
gpibObj.close()


