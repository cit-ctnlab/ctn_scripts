'''
Jan 03, 2019
author: agupta
This code reads data measured by measBNnoise.py and saves and plots a
corresponding median beatnote spectrum
Most part of median calculation and plotting has been taken from iris.
'''
import numpy as np
import os
import argparse
import matplotlib.pyplot as plt                 #For plotting
from matplotlib.backends.backend_pdf import PdfPages       #For saving figures to single pdf

#*******************************************************************************************************
#Setting RC Parameters for figure size and fontsizes
import matplotlib.pylab as pylab
params = {'legend.fontsize': 'xx-large',
          'figure.figsize': (20, 10),
         'axes.labelsize': 'xx-large',
         'axes.titlesize':'xx-large',
         'xtick.labelsize':'xx-large',
         'ytick.labelsize':'xx-large'}
pylab.rcParams.update(params)
#********************************************************************************************************
'''
plotBNspecLikeIris(fileroot,plotTitle,PLLslope = 10e3)
Calculates median and uncertainity from a batch of measurments with same
fileroot
Input arguments:
fileroot: Root name of measurment data files. All files starting with this
          string would be attempted for reading.
plotTitle: Title of the plot to be given.
PLLslope: Slope of marconi actuation in Hz/V
partSingleSpecPlots: Fraction of all (passed) single spectrum plots to be
                     plotted.
The code assumes that measurement is done in V/sqrt(Hz) (non-db units)
'''
def plotBNspecLikeIris(fileroot,plotTitle,PLLslope = 10e3,partSingleSpecPlots = 1):

    # Nice colors and alphas (https://xkcd.com/color/rgb/)
    tcolor  = '#d62728' # color of each sweep
    mcolor  = '#040273'  #'#1f3b4d' # color of the median
    uncolor = '#040273' # color of uncertainty bars

    alpha1 = 0.1  # sweeps transparency
    alpha2 = 0.8  # medians
    alpha3 = 0.4  # uncertainties

    ###     Argument Defaults     ###
    #################################

    xAxisLabel  = 'Frequency [Hz]'
    yAxisLabel  = 'Hz/$\sqrt{Hz}$'
    y2AxisLabel = 'Relative Mag [%]'

    plotTopTitle  = 'BN Spectrum Measurements $\pm$ Uncertainty'
    plotBottomTitle = 'Relative Meas $\pm$ Uncertainty'
    plotSaveName = fileroot[fileroot.rfind('/')+1:]
    ################################

    datadir = fileroot[0:fileroot.rfind('/')]
    cp = os.getcwd()
    os.chdir(datadir)
    fl =os.listdir(datadir)
    selList = []
    for fn in fl:
        if fn.find(plotSaveName) is not -1:
            if fn.find(plotSaveName + '_MedianData.txt') == -1:
                if fn.find('.txt') is not -1:
                    selList += [fn]
    freqLenList = {}
    for fn in selList:
        try:
            tempFreq = np.loadtxt(fn)[:,0]
        except:
            tempFreq = np.zeros(0)
        freqLenList[fn]=len(tempFreq)
        del tempFreq

    maxFreqLen = max(freqLenList.values())
    for fn in selList:
        if freqLenList[fn] != maxFreqLen:
            print(fn+' Doesnot have full frequency range. Ignoring  this file.')
            freqLenList.pop(fn)

    del maxFreqLen
    del selList
    selList = freqLenList.keys()
    del freqLenList

    # Find the distribution, assuming all frequency vectors are the same
    measNum = len(selList)
    freq    = np.loadtxt(selList[0])[:,0]
    freqLen = len(freq)

    spec     = np.zeros([measNum, freqLen])
    for ii,fn in enumerate(selList):
        tempTxt = np.loadtxt(fn)
        spec[ii,:] = tempTxt[:,1]

    print('Read data from '+str(measNum)+' files.')
    os.chdir(cp)

    specArea = np.zeros(measNum)
    for ii in range(measNum):
        specArea[ii] = np.sum((freq[1:]-freq[0:-1])*spec[ii,0:-1])
    cutoff = np.mean(specArea) + 2*np.std(specArea)
    jj=0
    tempspec = np.zeros([measNum, freqLen])
    for ii in range(measNum):
        if specArea[ii]<=cutoff:
            tempspec[jj,:] = spec[ii,:]
            jj=jj+1
    spec = tempspec[0:jj,:]*PLLslope
    measNum=len(spec)

    specMedian = np.zeros(freqLen)
    specUnc  = np.zeros(freqLen)
    for ii in range(freqLen):
        specMedian[ii] = np.median(spec[:,ii])
        specUnc[ii] = np.sqrt(np.mean((spec[:,ii]-specMedian[ii])**2))

    #Save the analyzed data
    saveArray = np.zeros([freqLen,3])
    saveArray[:,0] = freq
    saveArray[:,1] = specMedian
    saveArray[:,2] = specUnc
    np.savetxt(plotSaveName + '_MedianData.txt',saveArray)

    # Make labels for each spectrum measurement
    labels = np.array([])
    for ii in range(measNum):
        if ii == 0:
            labels = np.append(labels, 'FFTs')
        else:
            labels = np.append(labels, '')

    # Make da plot
    h = plt.figure(figsize = [16,12])
    f1 = h.add_subplot(211)
    f2 = h.add_subplot(212)
    max1Range = -np.inf
    min1Range = np.inf
    max2Range = -np.inf
    min2Range = np.inf

    maxDomain  = -np.inf
    minDomain  = np.inf

    # Plot every single spectrum measurement
    for ii in range(0,measNum,int(1/partSingleSpecPlots)):
        tempFreq = freq
        tempSpec = spec[ii,:]
        tempRelS = tempSpec/specMedian

        f1.loglog(tempFreq, tempSpec, color=tcolor, label=labels[ii],alpha=alpha1, rasterized=True)
        f2.semilogx(tempFreq, 100 * (tempRelS-1.0),color=tcolor, alpha = alpha1, rasterized=True)

        if max(tempSpec) > max1Range:
            max1Range = max(tempSpec)
        if min(tempSpec) < min1Range:
            min1Range = min(tempSpec)

        if max(100.0*(tempRelS-1.0)) > max2Range:
            max2Range = max(100.0*(tempRelS-1.0))
        if min(100.0*(tempRelS-1.0)) < min2Range:
            min2Range = min(100.0*(tempRelS-1.0))

        if max(tempFreq) > maxDomain:
            maxDomain = max(tempFreq)
        if min(tempFreq) < minDomain:
            minDomain = min(tempFreq)

    # Plot calculated median and uncertainty
    f1.loglog(tempFreq, specMedian,color=mcolor, label='Median', alpha=alpha2, rasterized=True)
    f1.loglog(tempFreq, specMedian + specUnc,ls='--', color=uncolor, rasterized=True, label='$\pm 1 \sigma$ Unc',alpha=alpha3)
    f1.loglog(tempFreq, specMedian - specUnc, rasterized=True,ls='--', color=uncolor, alpha=alpha3)

    #f2.semilogx(tempFreq, np.zeros(freqLen),
    #            color=mcolor, label='Median', alpha = alpha2)
    f2.semilogx(tempFreq, 100 *  specUnc/specMedian,
                ls='--', color=uncolor, label='$\pm 1 \sigma$ Unc', alpha = alpha3, rasterized=True)
    f2.semilogx(tempFreq, 100 * -specUnc/specMedian,
                ls='--', color=uncolor, alpha = alpha3, rasterized=True)

    if max(specMedian+specUnc) > max1Range:
        max1Range = max(specMedian+specUnc)
    #if min(specMedian-specUnc) < min1Range:
    #    min1Range = min(specMedian-specUnc)

    if max(100 *  specUnc/specMedian) > max2Range:
        max2Range = max(100 *  specUnc/specMedian)
    if min(100 * -specUnc/specMedian) < min2Range:
        min2Range = min(100 * -specUnc/specMedian)

    # Plot settings and titles
    f1.legend()
    f2.legend()

    f1.set_xlim([minDomain, maxDomain])
    f2.set_xlim([minDomain, maxDomain])

    f1.set_ylim([min1Range, max1Range])
    f2.set_ylim([min2Range, max2Range])

    f1.grid(which='minor')
    f1.set_axisbelow(True)
    f2.grid(which='minor')
    f2.set_axisbelow(True)

    f1.set_ylabel(yAxisLabel)
    f2.set_ylabel(y2AxisLabel)

    f1.set_title(plotTopTitle)
    f2.set_title(plotBottomTitle)

    f2.set_xlabel(xAxisLabel)

    h.suptitle(plotTitle,fontsize='xx-large')

    h.subplots_adjust(wspace=0.25)

    plt.savefig(plotSaveName + '_Iris.pdf',
            bbox_inches='tight', pad_inches=0.2)
    return h

parser = argparse.ArgumentParser(description='Usage: python BNspectrum.py '+
                                 '--fileroot <fileroot str>'+
                                 '--plotTitle <plot title str>'+
                                 '--PLLslope <PLL slope in Hz/V>')
parser.add_argument('-f',
                    '--fileroot', type=str, nargs=1,
                    help='Measurement Data Files root name')
parser.add_argument('-t',
                    '--plotTitle', type=str, nargs=1,
                    help='Title name of plot')
parser.add_argument('-s',
                    '--PLLslope', type=float, nargs=1,
                    help='PLL actuation slope in Hz/V',
                    default=[10e3])

args = parser.parse_args()

if args.plotTitle is not None:
    plotBNspecLikeIris(args.fileroot[0],args.plotTitle[0],args.PLLslope[0])
