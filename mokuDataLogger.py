'''
mokuDataLogger
This function logs voltage data using Moku.
Input Arguments:
chan:       {'ch1'(default),'ch2' or 'both'}. Case insensitive.
ipAddress:  IP Address of Moku. Default is 10.0.1.81.
duration:   Duration of measurement in seconds.
sampleRate: Sampling rate in Sa/s.
fiftyr:     50 Ohm inputs. {True, False(default)}. Can be a list for different
            values for different channels.
atten:      Turn on 10x attenuation. {True, False(default)}. Changes the
            dynamic range between 1Vpp(False) and 10Vpp(True). Can be a list
            for different calues for different channels.
ac:         AC coupling. {True, False(Default)}. Can be a list for different
            values for different channels.
precisionMode:  If true, data will be measured at max sampling rate and
                decimated to requried sample rate. If false, extra data points
                will be thrown away.
outChan:    {None(default), 'ch1','ch2' or 'both'}. Case insensitive. Output
            channels whose settings are being changed.
wf:         Waveform type. {'sine'(default), 'ramp' or 'square'}. Case
            insensitive. Can be a list for different values for different
            channels.
amp:        Amplitude in volts. {float, default=0.1}. Can be a list for
            different values for different channels.
freq:       Frequency in Hz. {float, default=10e3}. Can be a list for
            different values for different channels.
off:        Turn off output. {True, False(Default)}. Can be a list for
            different values for different channels.
altFileRoot: Moku's default fileroot is MokuPhasemeterData which can be
             replaced by providing an alternate file root. Default is None.
useExternal: Attempt to lock to an external reference clock.
             {True(default), False}.
useSD:       Whether to log to the SD card, else the internal Moku filesystem.
             {True, False(default)}. Using SD card limits sampling rate.
fileType:    Log file type. {'csv'(default),'bin','mat','npy'} corresponding to
             {CSV(default), Binary, MATLAB, NPY (Numpy Data)} respectively.
Output:
dataFileName: File name of the measurement data.
'''

from pymoku import Moku, StreamException
from pymoku.instruments import Datalogger
import argparse
import numpy as np
import time
import os


def mokuDataLogger(chan='ch1', ipAddress='10.0.1.81', duration=60,
                   sampleRate=500e6, fiftyr=True, atten=False, ac=False,
                   precisionMode=True, outChan=None, wf='sine', amp=0.1,
                   freq=10e3, off=False, altFileRoot=None, useExternal=True,
                   useSD=False, fileType='csv'):
    chan = chan.lower()
    if chan == 'ch1':
        ch1 = True
        ch2 = False
    elif chan == 'ch2':
        ch1 = False
        ch2 = True
    elif chan == 'both':
        ch1 = True
        ch2 = True
    if not isinstance(fiftyr, list):
        fiftyr = [fiftyr, fiftyr]
    if not isinstance(atten, list):
        atten = [atten, atten]
    if not isinstance(ac, list):
        ac = [ac, ac]

    if outChan is None:
        outch1 = False
        outch2 = False
    else:
        outChan = outChan.lower()
        if outChan == 'ch1':
            outch1 = True
            outch2 = False
        elif outChan == 'ch2':
            outch1 = False
            outch2 = True
        elif outChan == 'both':
            outch1 = True
            outch2 = True
    outChList = [outch1, outch2]

    if not isinstance(wf, list):
        wf = [wf.lower(), wf.lower()]
    else:
        wf = [s.lower() for s in wf]
    if not isinstance(amp, list):
        amp = [amp, amp]
    if not isinstance(freq, list):
        freq = [freq, freq]

    if not isinstance(off, list):
        off = [off, off]

    for s in wf:
        if s not in ['sine', 'ramp', 'square']:
            raise RuntimeError('Only sine, ramp and square waves supported.')

    try:
        m = Moku(ipAddress)
        i = m.deploy_instrument(Datalogger)
        i.set_defaults()

        # First set output channels if required.
        for ii, ch in enumerate(outChList):
            chNo = ii+1
            if off[ii]:
                i.gen_off(chNo)
                print('Switching off Channel', chNo)
            elif ch:
                # Set front end settings
                i._set_frontend(chNo, fiftyr=fiftyr[ii], atten=atten[ii],
                                ac=ac[ii])
                if wf[ii] == 'sine':
                    i.gen_sinewave(chNo, amplitude=amp[ii], frequency=freq[ii])
                    print('Ch', chNo, ': Sine wave of Amplitude=',
                          amp[ii], 'V at ', ftoss(freq[ii]) + 'Hz')
                elif wf[ii] == 'ramp':
                    i.gen_rampwave(chNo, amplitude=amp[ii], frequency=freq[ii])
                    print('Ch', chNo, ': Ramp wave of Amplitude=',
                          amp[ii], 'V at ', ftoss(freq[ii]) + 'Hz')
                elif wf[ii] == 'square':
                    i.gen_squarewave(chNo, amplitude=amp[ii],
                                     frequency=freq[ii])
                    print('Ch', chNo, ': Square wave of Amplitude=',
                          amp[ii], 'V at ', ftoss(freq[ii]) + 'Hz')

        for ii, ch in enumerate(outChList):
            chNo = ii+1
            if ch:
                # Set front end settings
                i._set_frontend(chNo, fiftyr=fiftyr[ii], atten=atten[ii],
                                ac=ac[ii])
                # Set precision mode
        i.set_precision_mode(precisionMode)
        # Set sampling rate
        i.set_samplerate(sampleRate)
        # Stop any previous data logging
        i.stop_data_log()
        # Start data logging
        print('Starting data log at', 'Ch1' if ch1 else '',
              'and' if ch1 and ch2 else '', 'Ch2' if ch2 else '',
              'for', ftoss(duration)+'s at', ftoss(sampleRate)+'Sa/s.')
        print('Writing at', 'SD Card' if useSD else 'RAM', 'with', fileType,
              'extension.')
        i.start_data_log(duration=duration, use_sd=useSD, ch1=ch1, ch2=ch2,
                         filetype=fileType)
        # Track progress percentage of the data logging session
        progress = 0
        while progress < 100:
            # Wait for the logging session to progress
            time.sleep(duration/10)
            # Get current progress percentage and print it out
            progress = i.progress_data_log()
            print('Progress {}%'.format(progress))
        # Upload the log file to the local directory
        i.upload_data_log()
        print('Uploaded log file to local directory.')
        # Denote that we are done with the data logging session so resources
        # may be cleand up
        i.stop_data_log()
        # Rename logged datafile root to altFileRoot
        defFileName = i.data_log_filename() + '.' + fileType
        if altFileRoot is not None:
            dataFileName = defFileName.replace('MokuPhasemeterData',
                                               altFileRoot)
            os.rename(defFileName, dataFileName)
        else:
            dataFileName = defFileName
    except StreamException as e:
        dataFileName = 'Error'
        print('Error occured: %s' % e)
    finally:
        try:
            m.close()
        except BaseException:
            print('Failed to connect to moku properly.')
            dataFileName = 'Error'
        finally:
            return dataFileName


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This function logs voltage data using Moku.')
    parser.add_argument('-i', '--ipAddress',
                        help='IP address of Moku. Default is 10.0.1.81',
                        default='10.0.1.81')
    parser.add_argument('-c', '--chan', type=str,
                        help='ch1(default), ch2 or both. Case insensitive.',
                        default='ch1')
    parser.add_argument('-d', '--duration', type=int,
                        help='Measurement time in seconds. Default 60.',
                        default=60)
    parser.add_argument('-s', '--sampleRate', type=float,
                        help='Sampling Rate between 0 to 1 MSa/s.'
                             'Default is 1e6',
                        default=1e6)
    parser.add_argument('--MOhmr1',
                        help='1 MOhm input impedance for Ch 1. Default: False',
                        action='store_true')
    parser.add_argument('--MOhmr2',
                        help='1 MOhm input impedance for Ch 2. Default: False',
                        action='store_true')
    parser.add_argument('--atten1',
                        help='10x Attenuation for Ch1.'
                             'True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('--atten2',
                        help='10x Attenuation for Ch2.'
                             'True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('--ac1',
                        help='AC Coupling. Default: False',
                        action='store_true')
    parser.add_argument('--ac2',
                        help='AC Coupling for channel 2. Default: False',
                        action='store_true')
    parser.add_argument('--noDecimation',
                        help='Switches off precision mode. Default: False',
                        action='store_true')
    parser.add_argument('--outChan', type=str,
                        help='ch1, ch2 or both. Case insensitive.'
                             'Default is None',
                        default=None)
    parser.add_argument('--wf1', type=str,
                        help='Channel 1 waveform. Ramp, Sine, Square.'
                             'Case insensitive. Default is Sine.',
                        default='sine')
    parser.add_argument('--wf2', type=str,
                        help='Channel 2 waveform. Ramp, Sine, Square.'
                             'Case insensitive. Default is Sine.',
                        default='sine')
    parser.add_argument('--amp1', type=float,
                        help='Channel 1 amplitude in volts. Default 0.1',
                        default=0.1)
    parser.add_argument('--amp2', type=float,
                        help='Channel 2 amplitude in volts. Default 0.1',
                        default=0.1)
    parser.add_argument('--f1', type=float,
                        help='Channel 1 frequency in Hz. Default 10e3',
                        default=10e3)
    parser.add_argument('--f2', type=float,
                        help='Channel 2 frequency in Hz. Default 10e3',
                        default=10e3)
    parser.add_argument('--off1',
                        help='Turn off ch 1 output. Default: False',
                        action='store_true')
    parser.add_argument('--off2',
                        help='Turn off ch 2 output. Default: False',
                        action='store_true')
    parser.add_argument('--altFileName', type=str,
                        help='Alternate File Root Name.',
                        default=None)
    parser.add_argument('--useInternal',
                        help='Use internal 10 MHz clock only. Default: False',
                        action='store_true')
    parser.add_argument('--useSD',
                        help='Use SD Card for storing data. Default: False',
                        action='store_true')
    parser.add_argument('--fileType', type=str,
                        help='File Type: csv(default), bin, mat, npy',
                        default='csv')
    return parser.parse_args()


def ftoss(value):
    sgn = np.sign(value)
    if sgn == 0:
        return '0'
    else:
        absv = np.abs(value)
        ex = np.minimum(12, np.maximum(-15, np.floor(np.log10(absv))))
        sv = absv/(10**ex)
        sc = str(int(np.floor(ex/3)))
        unit = {
            '-5': 'f',
            '-4': 'p',
            '-3': 'n',
            '-2': 'u',
            '-1': 'm',
            '0': '',
            '1': 'k',
            '2': 'M',
            '3': 'G',
            '4': 'T'
        }[sc]
        val = str(sgn*sv*10**(ex % 3))
        return val+unit


if __name__ == '__main__':
    args = grabInputArgs()
    fiftyr = [not args.MOhmr1, not args.MOhmr2]
    atten = [args.atten1, args.atten2]
    ac = [args.ac1, args.ac2]
    precisionMode = not args.noDecimation
    wf = [args.wf1, args.wf2]
    amp = [args.amp1, args.amp2]
    freq = [args.f1, args.f2]
    off = [args.off1, args.off2]
    useExternal = not args.useInternal
    mokuDataLogger(chan=args.chan, ipAddress=args.ipAddress,
                   duration=args.duration, sampleRate=args.sampleRate,
                   fiftyr=fiftyr, atten=atten, ac=ac,
                   precisionMode=precisionMode, outChan=args.outChan,
                   wf=wf, amp=amp, freq=freq, off=off,
                   altFileRoot=args.altFileName,
                   useExternal=not args.useInternal,
                   useSD=args.useSD,
                   fileType=args.fileType)
