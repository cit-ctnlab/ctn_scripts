#!/usr/bin/env python

import re
import time
import os
from epics import caget, caput
try:
    import ConfigParser
except BaseException:
    import configparser as ConfigParser
import argparse
import numpy as np

'''
Author: Andrew Wade (awade) - awade@ligo.caltech.edu
Created: ~ Nov 21, 2017

Change log:
Feb 6th, 2020:
Anchal: Modified to serve as function to be used by other scripts
'''

ChannelUpdateRate  = 0.100 # hard coded channel update rate

def gainCycle(param=None, comCH=None, fastCH=None,
              comHardStops=[-10, 30], fastHardStops=[-10, 30],
              comSlew=6, fastSlew=6):
    '''
    Grab current state of sliders
    Slew down smoothly in dB scale or maybe linear
    Slew down second slider
    Slider should rail to lower limit (check rail function)
    Reverse order and exit in state 0
    '''
    if param is None:
        if comCH is None or fastCH is None:
            raise RuntimeError('Need to provide params of Gain Channels.')
    else:
        comCH = param.comCH
        fastCH = param.fastCH
        comHardStops = param.comHardStops
        fastHardStops = param.fastHardStops
        comSlew = param.comSlew
        fastSlew = param.fastSlew
    # Get initial values and rail gain sliders to bottom
    comgain_init = caget(comCH)
    fastgain_init = caget(fastCH)

    caput(comCH, comHardStops[0])
    caput(fastCH, fastHardStops[0])

    comNoP = ((comgain_init - comHardStops[0])
                            / (ChannelUpdateRate * comSlew))
    fastNoP = ((fastgain_init - fastHardStops[0])
                             / (ChannelUpdateRate * fastSlew))

    fastgain_sweep = np.linspace(fastHardStops[0], fastgain_init, fastNoP)
    comgain_sweep = np.linspace(comHardStops[0], comgain_init, comNoP)

    for ii in fastgain_sweep:
        caput(fastCH, ii)
        time.sleep(ChannelUpdateRate)

    for ii in comgain_sweep:
        caput(comCH, ii)
        time.sleep(ChannelUpdateRate)

def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='Python script for temporary killing gain in FSS loop and '
                    'softly ramping back into original state. Should be used '
                    'to killing ringing of loops when actuators rail.')
    parser.add_argument(
        'configfile',
        type=str,
        help='Provide a config .ini file name and location. Config file must '
             'include channel names and config settings for ramp on and off '
             'rates.')
    parser.add_argument(
        '--debug',
        action='store_true')
    return parser.parse_args()


class ImportConfig():
    def __init__(self, configfile):  # initiate class and generate structure for object to call channel names from

        config = ConfigParser.ConfigParser()
        config.read(configfile)
        self.comCH = config.get(
            "EPICSChannelConfig",
            "CommonGainChannel")
        self.fastCH = config.get(
            "EPICSChannelConfig",
            "FastGainChannel")
        self.comSlew = config.getfloat(
            "GainRampingSettings",
            "common_gainSlewRate")
        self.fastSlew = config.getfloat(
            "GainRampingSettings",
            "fast_gainSlewRate")
        self.comHardStops = [config.getfloat(
            "GainRampingSettings",
            "common_hardstops_lower"),
                                  config.getfloat(
            "GainRampingSettings",
            "common_hardstops_upper")]
        self.fastHardStops = [config.getfloat(
            "GainRampingSettings",
            "fast_hardstops_lower"),
                                   config.getfloat(
            "GainRampingSettings",
            "fast_hardstops_upper")]

        self.EPICS_channel_config_all = config.items("EPICSChannelConfig")
        self.Gain_ramping_settings_all = config.items("GainRampingSettings")


if __name__ == "__main__":
    args = grabInputArgs()
    param = ImportConfig(args.configfile)
    if args.debug == 1:
        print("Started with config file {}.\n".format(args.configfile))

    if args.debug == 1:
        print("-----------------------------------------------")
        print("Channel configuration:")
        print(EPICS_channel_config_all)
        print("")
        print("-----------------------------------------------")

    gainCycle(param)
