#!/usr/bin/env/ python

# Author: Andrew Wade
# Date: 2017 1 September

import time
from epics import caget, caput
import ConfigParser
import argparse
from gainCycle import gainCycle

# Set frac of total range to jump at start of search
iniJump = 0.1


# Input argument parser
def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='Auto locker script that scans a proccess variable until'
                    'either a threshold is met or any on of a list of binary'
                    'channels are state false.')
    parser.add_argument(
        'configfile',
        type=str,
        help='Here you must enter a config file that contains the channels '
             'for error signal, actuator, engauge and hard limits. You may '
             'also optionally specify binary channels, the autolocker will '
             'only engauge when these are in the True state.')
    parser.add_argument('--debug',
                        action='store_true')
    return parser.parse_args() # Grabs arguments and puts into a local variable

class ImportConfig():
    # initiate class and generate structure for object to call
    # channel names from
    def __init__(self, configfile):

        config = ConfigParser.ConfigParser()
        config.read(args.configfile)
        # Monitor for threshold
        self.monitor = config.get("EPICSChannelConfig",
                             "monitor")
        # process variable
        self.actuator = config.get("EPICSChannelConfig",
                                   "actuator")
        # Binary engage
        self.loopStateEnable = config.get("EPICSChannelConfig",
                                          "loopStateEnable")
        # Upper sweep range
        self.sweepRangeUpperChan = config.get("EPICSChannelConfig",
                                              "sweepRangeUpperChan")
        # Lower sweep range
        self.sweepRangeLowerChan = config.get("EPICSChannelConfig",
                                              "sweepRangeLowerChan")
        # Sweep step size
        self.sweepStepSizeChan = config.get("EPICSChannelConfig",
                                            "sweepStepSizeChan")
        # Enable Autolock
        self.autolockEnable = config.get("EPICSChannelConfig",
                                         "autolockEnable")
        try: # get required states for autolock or default none
            self.requiredstates = config.get("EPICSChannelConfig",
                                        "requiredstates").splitlines()
        except Exception:
            self.requiredstates = []

        try: # Soft channel that blinks to show script alive
            self.ALTrackChan = config.get("EPICSChannelConfig",
                                        "AutolockerTrackEN")
        except Exception:
            self.ALTrackChan = None

        self.thresholdValChan = config.get("EPICSChannelConfig",
                                           "thresholdValChan")

        try: # direction of threshold
            self.thresholdSign = config.getfloat("SearchSweepRateParameters",
                                                 "thresholdSign")
        except Exception:
            self.thresholdSign = 1.0

        try: # settle time of actuator
            self.settleTime = config.getfloat("SearchSweepRateParameters",
                                              "settleTime")
        except Exception:  # case a settle time isn't in config file
            self.settleTime = 5.0    # defult settle time

        try:
            self.numAttempts = config.getfloat("SearchSweepRateParameters",
                                               "numAttempts")
        except Exception:
            self.numAttempts = 1

        # steprate (seconds)
        self.stepRate = config.getfloat("SearchSweepRateParameters",
                                        "stepRate")
        # sec between checks
        self.pollRate = config.getfloat("ProcessControl", "pollRate")

        # Parameters for Gain Cycle
        try:
            self.comCH = config.get("EPICSChannelConfig", "CommonGainChannel")
            self.fastCH = config.get("EPICSChannelConfig", "FastGainChannel")
            self.comSlew = config.getfloat("GainRampingSettings",
                                           "common_gainSlewRate")
            self.fastSlew = config.getfloat("GainRampingSettings",
                                            "fast_gainSlewRate")
            self.comHardStops = [config.getfloat("GainRampingSettings",
                                                 "common_hardstops_lower"),
                                 config.getfloat("GainRampingSettings",
                                                 "common_hardstops_upper")]
            self.fastHardStops = [config.getfloat("GainRampingSettings",
                                                  "fast_hardstops_lower"),
                                  config.getfloat("GainRampingSettings",
                                                  "fast_hardstops_upper")]
            self.doGainCycle = True
        except BaseException:
            self.comgainCH = None
            self.fastgainCH = None
            self.com_gainslew = None
            self.fast_gainslew = None
            self.comgain_hardstops = None
            self.fastgain_hardstops = None
            self.doGainCycle = False

def main(args, param):
    while(1):
        while(caget(param.autolockEnable) and
              xnor(ANDChannels(param.requiredstates),
                   param.thresholdSign*(caget(param.monitor)
                                        -caget(param.thresholdValChan)) > 0) ):
            if args.debug == 1:  # debug mode: print message
                print('Monitor met threshold AND required states ALL true,'
                      'waiting {period} seconds to check again'
                      ''.format(period=param.pollRate))
            if param.ALTrackChan is not None:
                if caget(param.ALTrackChan):
                    caput(param.sweepRangeUpperChan,caget(param.actuator)+0.03)
                    caput(param.sweepRangeLowerChan,caget(param.actuator)-0.03)
            time.sleep(param.pollRate) # wait for pollRate seconds rechecking
        else:  # case threshold is not met and channels are engauged
            if caget(param.autolockEnable):
                if args.debug == 1:  # debug mode: print message
                    print("Thresh reached or one required states False")


                caput(param.loopStateEnable, 0)  # drop the lock engage
                sweepRangeUpperValue = caget(param.sweepRangeUpperChan)
                sweepRangeLowerValue = caget(param.sweepRangeLowerChan)
                sweepRange = [sweepRangeLowerValue, sweepRangeUpperValue]
                # Shifts loop actuator just above last known
                # lock within rail range
                iniOffset = rail((caget(param.actuator)
                                  + iniJump * (sweepRange[1]-sweepRange[0])),
                                 sweepRange[0],
                                 sweepRange[1])
                caput(param.actuator, iniOffset)  # write out new offset value
                # Wait time settleTime in seconds for plant to settle.
                # Can set low for fast responding plants.
                time.sleep(param.settleTime)
                # Call the threshold sweeper search function
                # if exit state 1 then threshold reached and should reengage
                if threshSearch(param) == 1:
                    caput(param.loopStateEnable,1)  # re engauge lock
                    # wait to settle from transients
                    time.sleep(param.settleTime)
                    # do a gain cycle if params present
                    if param.doGainCycle:
                        gainCycle(param)
                    if args.debug == 1: # debug mode: print message
                        print("Loop autolocker engaged")

# Sweep actuator values and search for the threshold.
# Return when reached or after one full range has been cycled
# with binary exit value
def threshSearch(param):
    swpDir = -1.0  # Set the direction of sweep, defult is start downward
    noHitRails = 0
    attempts = 0
    while(caget(param.autolockEnable) and
          ANDChannels(param.requiredstates) and
          param.thresholdSign*(caget(param.monitor)
                               -caget(param.thresholdValChan)) < 0):
        time.sleep(param.stepRate)    # wait
        sweepRangeUpperValue = caget(param.sweepRangeUpperChan)
        sweepRangeLowerValue = caget(param.sweepRangeLowerChan)
        srange = [sweepRangeLowerValue, sweepRangeUpperValue]
        stepSize = caget(param.sweepStepSizeChan)
        ActuatorState = caget(param.actuator)
        # Compute next value to write based on direction of sweep, step size
        # and railed to hard limits set in the .ini file
        ActuatorState_stepped = rail(ActuatorState + swpDir * stepSize,
                                     srange[0], srange[1])
        # write out to channel next step value
        caput(param.actuator, ActuatorState_stepped)
        # check if railed, change direction
        if ActuatorState_stepped == ActuatorState:
            noHitRails = noHitRails + 1
            if noHitRails%2==0:
                # Rails were hit 2 times. Another sweep is complete.
                attempts = attempts + 1
                if attempts < param.numAttempts:
                    # Dynamic expansion of search region until numAttempts
                    # is reached.
                    caput(param.sweepRangeLowerChan,
                          0.5*(3*srange[0] - srange[1]))
                    caput(param.sweepRangeUpperChan,
                          0.5*(-srange[0] + 3*srange[1]))
                    # Continue search in same direction
            else:
                # Change direction to complete sweep.
                swpDir = swpDir*(-1)
        # If Number of full sweeps has reached maximum attempts without
        # Exit with state 0
        if attempts == param.numAttempts:
            exitstate = 0  # exit state 0 means failed to find threshold
            break  # break out of while loop to return function value

    # while exit condition of threshold no longer exeeding monitor channel,
    # exit with affirmative state
    else:
        if caget(param.autolockEnable) and ANDChannels(param.requiredstates):
                exitstate = 1  # exit state 1 means threshold has been met
        else:
            exitstate = 0
    return exitstate

def rail(n, minn, maxn):    # Check number within range and rail it to limits
        return max(min(maxn, n), minn)

def ANDChannels(chanList):
    '''
    Find boolean AND of a list of binary EPICs channels.
    Rreturns true if all are 1 and false if any are 0
    '''
    return all([caget(ii) for ii in chanList])

def xnor(condA,condB):
    return (condA and condB) or ((not condA) and (not condB))

if __name__ == "__main__": #triger input parser on call
    args = grabInputArgs()
    param = ImportConfig(args.configfile)
    if args.debug == 1:
        # debug mode: display config values as they were read in
        print("----------------------------------------")
        print("Channel configuration:")
        print(config.items("EPICSChannelConfig"))
        print(".")
        print("Search Parameters:")
        print(config.items("SearchSweepRateParameters"))
        print("----------------------------------------")
    main(args, param)
