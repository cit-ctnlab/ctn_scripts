'''
Author: Anchal Gupta (agupta) - anchal@caltech.edu
Created:  July 12, 2019
'''

import nds2
import argparse
import time
import numpy as np


def fromFBread(CHList=None, CHListFile=None, ipaddress='10.0.1.14', port=8088,
               start=None, stop=None, interval=10, stopNow=False, decimate=1,
               filename='fromFBread', saveFile=True, verbose=True):
    if verbose:
        print('Reading channels:')
    if CHListFile is not None:
        # If file is provided, CHList is overrided
        with open(CHListFile, 'r') as clf:
            CHList = clf.readlines()
        # Remove any CR or LF and then read the channels and store in a dict
        for ii, ch in enumerate(CHList):
            ch = ch.replace('\n', '')
            CHList[ii] = ch.replace('\r', '')
    if verbose:
        print(CHList)
        print('at ' + ipaddress + ' on port ' + str(port))
    if stop is None:
        if stopNow:
            stop = int(np.floor(time.time() - 315964784))
        else:
            stop = start + interval
    if start is None:
        start = stop - interval
    c = nds2.connection(ipaddress, port)
    c.set_parameter('GAP_HANDLER', 'STATIC_HANDLER_NAN')
    data = c.fetch(start, stop, CHList)
    filename = (filename + '_'
                + time.strftime('%d-%m-%Y', time.localtime())
                + time.strftime('_%H%M%S', time.localtime())+'.txt')
    dataToWrite = np.zeros((len(data[0].data), len(CHList)+1))
    timeSeries = np.linspace(start, stop, len(data[0].data)+1)[:-1]
    dataToWrite[:, 0] = timeSeries
    header = 'TimeSeries(s)  '
    for ii, ch in enumerate(CHList):
        dataToWrite[:, ii+1] = data[ii].data
        header = header+ch+'  '

    retData = {}
    if decimate != 1:
        dataToWrite = decimateData(dataToWrite, decimate, verbose)
        for ii, ch in enumerate(CHList):
            header = header + ch + '_STD  '
            retData['time'] = dataToWrite[:, 0]
            retData[ch] = dataToWrite[:, ii + 1]
            retData[ch + '_STD'] = dataToWrite[:, ii + 1 + len(CHList)]
    else:
        for ii, ch in enumerate(CHList):
            retData['time'] = dataToWrite[:, 0]
            retData[ch] = dataToWrite[:, ii + 1]

    if saveFile:
        if verbose:
            print('Writing data to file '+filename+' ...')
        np.savetxt(filename, dataToWrite, header=header)

    return retData


def decimateData(data, block_size, verbose=True):
    redData = np.zeros((np.shape(data)[0]//block_size, np.shape(data)[1]*2-1))
    for ii in range(np.shape(redData)[0]):
        dataChunk = data[ii*block_size:(ii+1)*block_size, :]
        redData[ii, 0] = np.mean(dataChunk[:, 0])
        for jj in range(1, np.shape(data)[1]):
            uncjj = jj + np.shape(data)[1] - 1
            if (ii+1)*block_size <= np.shape(data)[0]:
                redData[ii, jj] = np.mean(dataChunk[:, jj])
                redData[ii, uncjj] = np.std(dataChunk[:, jj])
    if verbose:
        print(np.shape(data), ' Reduced to ', np.shape(redData))
    return redData


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='Python script for reading channels from a frame builder '
                    'computer and write them to a .txt file.')
    parser.add_argument('-c', '--CHList',
                        nargs='+',
                        help='List of channels to read. The output file will '
                        'have channels in the same order. Just give all '
                        'channel names separated with a space.')
    parser.add_argument('--CHListFile',
                        type=str,
                        default=None,
                        help='Channel List filename')
    parser.add_argument('-f',
                        '--filename',
                        help='Stem of output filename.',
                        default='fromFBread')
    parser.add_argument('--start', type=int, help='Start gpstime.',
                        default=None)
    parser.add_argument('--stop', type=int, help='End gpstime.', default=None)
    parser.add_argument('--stopNow',
                        help='Stop at current time.',
                        action='store_true')
    parser.add_argument('--interval', type=int, help='Interval in seconds',
                        default=10)
    parser.add_argument('-i',
                        '--ipaddress',
                        type=str,
                        default='10.0.1.14',
                        help='IP Address of framebuilder.')
    parser.add_argument('-p',
                        '--port',
                        type=int,
                        default=8088,
                        help='Port number for connection to framebuilder.')
    parser.add_argument('-d',
                        '--decimate',
                        type=int,
                        default=1,
                        help='Decimation factor. Default is 1 for'
                             ' no decimation.')
    parser.add_argument('-s', '--silent',
                        help='Run silently without displaying output',
                        action='store_false')
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()

    if isinstance(args.CHList, bool) and args.CHListFile is None:
        raise RuntimeError('Either CHList or CHListFile must be provided')
    if args.interval == None:
        if args.start == None and args.stop == None:
            raise RuntimeError('Atleast one of start and stop time need to be '
                               'provided along with interval or both start and'
                               ' stop should be provided.')
        elif args.start == None or args.stop == None:
            raise RuntimeError('If only one of start or stop is provided, you'
                               ' must provide interval too.')
    elif args.start == None and args.stop == None and not args.stopNow:
        raise RuntimeError('Either provide a start or stop time or use '
                           'stopNow to get latest data.')
    fromFBread(CHList=args.CHList, CHListFile=args.CHListFile,
               ipaddress=args.ipaddress, port=args.port, start=args.start,
               stop=args.stop, interval=args.interval, stopNow=args.stopNow,
               decimate=args.decimate, filename=args.filename, saveFile=True,
               verbose=args.silent)
