#!/usr/bin/python
import time
import datetime
import sys
import usb.core                 # https://walac.github.io/pyusb/
import usb.util
import logging

from subprocess import call

dev = usb.core.find(idVendor=0x20ce, idProduct=0x0010)

if dev is None:
    raise ValueError('Device not found')

if dev.is_kernel_driver_active(0):
    reattach = True
    dev.detach_kernel_driver(0)

cfg = dev.get_active_configuration()
intf = cfg[(0,0)]

epi = usb.util.find_descriptor(
    intf,
    # match the first IN endpoint
    custom_match = \
    lambda e: \
        usb.util.endpoint_direction(e.bEndpointAddress) == \
        usb.util.ENDPOINT_IN)

epo = usb.util.find_descriptor(
    intf,
    # match the first OUT endpoint
    custom_match = \
    lambda e: \
        usb.util.endpoint_direction(e.bEndpointAddress) == \
        usb.util.ENDPOINT_OUT)

request_freq_cmd = b'\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'

while 1:
    time.sleep(0.5)    
    try:
        epo.write(request_freq_cmd.encode('utf-8'),100)
	cnt_read = epi.read(epi.bEndpointAddress,epi.wMaxPacketSize)
        freq = float(''.join(chr(i) for i in cnt_read[17:26]))
	#call("caput X1:CRY-BEAT_FREQ_COUNTER "+str(freq)+" >/dev/null", shell=True)
	print str(freq) + " MHz"
    except:
	continue
