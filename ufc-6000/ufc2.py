#!/usr/bin/python
from time import sleep
import usb.core                 # https://walac.github.io/pyusb/
import usb.util
import argparse

from subprocess import call

''' This script queries a USB Frequency Counter UFC-6000 and writes out to
    a soft IOC channel at regular intervals.  Usage is given by running
    >python ufc.py --help '''

dev = usb.core.find(idVendor=0x20ce, idProduct=0x0010)  # Generate device obj

# Raise error if device cannot be found
if dev is None:
    raise ValueError('Device not found.')

if dev.is_kernel_driver_active(0):
    reattach = True
    dev.detach_kernel_driver(0)

cfg = dev.get_active_configuration()
intf = cfg[(0, 0)]

epi = usb.util.find_descriptor(
    intf,
    # match the first IN endpoint
    custom_match=lambda e: \
    usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_IN)

epo = usb.util.find_descriptor(
    intf,
    # match the first OUT endpoint
    custom_match=lambda e: \
    usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_OUT)


def main(channelName, pollPeriod):
    setSamplingTime()
    while(1):
        sleep(pollPeriod)
        freq = acquireFreq()
        call("caput " + channelName + " " + str(freq) + " >/dev/null",
             shell=True)


# Calls USB device and grabs the current frequency reading
def acquireFreq():
    request_freq_cmd = '\x02' + '\x00' * 63
    # Above is bad long line, spread over a few lines please

    try:
        epo.write(request_freq_cmd.encode('utf-8'), 100)
        cnt_read = epi.read(epi.bEndpointAddress, epi.wMaxPacketSize)
        freq = float(''.join(chr(i) for i in cnt_read[17:26]))
        print(str(freq) + " MHz")
    except Exception:
        print('there was an error')
        freq = None
    return freq

# Calls USB device and sets the sampling time to 0.1s (Minimum possible)
def setSamplingTime():
    set_st_cmd = '\x03\x01' + '\x00' * 62
    # Above is bad long line, spread over a few lines please
    try:
        epo.write(set_st_cmd.encode('utf-8'), 100)
    except Exception:
        print('there was an error')
        freq = None


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='Python script for polling a UFC-6000 USB frequency '
                    'counter. Writes out to a supplied EPICS IOC channel.')
    parser.add_argument('channelName',
                        type=str,
                        help='Enter channel name to write frequency to')
    parser.add_argument('--pollPeriod',
                        type=float,
                        default=1.0)
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    main(args.channelName, args.pollPeriod)
