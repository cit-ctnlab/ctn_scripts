#!/usr/bin/env python

import re
import time
import os
from epics import caget, caput
import ConfigParser
import argparse
import numpy as np
from gainCycle import gainCycle

'''
Author: Craig Cahillane - craig.cahillane@ligo.caltech.edu
Created: ~ Mar 06, 2018

Change log:
May 14, 2019: Changed to a standalone code using pyepics
'''

ChannelUpdateRate  = 0.100 # hard coded channel update rate

def main(args, param):
    '''
    Start an eternal monitor for the RMS values of the channel in the .ini file.
    If the RMS value exceeds the rail defined in the .ini file, our cavity FSS is "ringing".
    Ringing means that the EOM and PZT are fighting each other to control the laser frequency, pushing the EOM to rail.
    This script will automatically call gaincycle.py for that cavity to stop the ringing.
    '''

    # Eternal loop
    monitorTime = 2 # seconds of data to monitor
    monitorArrayLength = int(monitorTime/ChannelUpdateRate)
    monitorArray = np.zeros([ monitorArrayLength ])
    populated = False
    ii = 0

    while(True):
        startTime = time.time()
        monitorArray[ii] = caget(param.monitorCH)
        rms = np.sqrt(np.mean((monitorArray-np.mean(monitorArray))**2))
        caput(param.fastmonRMSCH,rms)

        if (ANDChannels(param.requiredStates) and
            rms > caget(param.fastmonRMSThresCH) and
            populated == True):
            gainCycle(param=param)
            monitorArray = np.zeros([ monitorArrayLength ])
            populated = False
            ii = 0

        ii = (ii + 1) % monitorArrayLength
        if ii == 0:
            populated = True
            if args.debug:
                print(param.monitorCH, 'rms =', rms, 'V')
        time.sleep(np.max([ChannelUpdateRate - (time.time() - startTime),0]))

def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='Python script for temporary killing gain in FSS loop and '
                    'softly ramping back into original state. Should be used '
                    'to killing ringing of loops when actuators rail.')
    parser.add_argument(
        'configfile',
        type=str,
        help='Provide a config .ini file name and location. Config file must '
             'include channel names and config settings for ramp on and off '
             'rates.')
    parser.add_argument(
        '--debug',
        action='store_true')
    return parser.parse_args()


class ImportConfig():
    def __init__(self, configfile):  # initiate class and generate structure for object to call channel names from

        config = ConfigParser.ConfigParser()
        config.read(configfile)
        self.monitorCH = config.get(
            "EPICSChannelConfig",
            "FastMonitorChannel")
        self.requiredStates = config.get(
            "EPICSChannelConfig",
            "requiredStates").splitlines()
        self.comCH = config.get(
            "EPICSChannelConfig",
            "CommonGainChannel")
        self.fastCH = config.get(
            "EPICSChannelConfig",
            "FastGainChannel")
        self.fastmonRMSCH = config.get(
            "EPICSChannelConfig",
            "FastmonRMSChannel")
        self.fastmonRMSThresCH = config.get(
            "EPICSChannelConfig",
            "FastmonRMSThreshold")
        self.comSlew = config.getfloat(
            "GainRampingSettings",
            "common_gainSlewRate")
        self.fastSlew = config.getfloat(
            "GainRampingSettings",
            "fast_gainSlewRate")
        self.comHardStops = [config.getfloat(
            "GainRampingSettings",
            "common_hardstops_lower"),
                                  config.getfloat(
            "GainRampingSettings",
            "common_hardstops_upper")]
        self.fastHardStops = [config.getfloat(
            "GainRampingSettings",
            "fast_hardstops_lower"),
                                   config.getfloat(
            "GainRampingSettings",
            "fast_hardstops_upper")]

        self.EPICS_channel_config_all = config.items("EPICSChannelConfig")
        self.Gain_ramping_settings_all = config.items("GainRampingSettings")


def ANDChannels(chanList):
    '''Find boolean AND of a list of binary EPICs channels and returns true if
       all are 1 and false if any are 0'''
    return all([caget(ii) for ii in chanList])


if __name__ == "__main__":
    args = grabInputArgs()
    param = ImportConfig(args.configfile)
    if args.debug == 1:
        print("Started with config file {}.\n".format(args.configfile))

    if args.debug == 1:
        print("-----------------------------------------------")
        print("Channel configuration:")
        print(param.EPICS_channel_config_all)
        print("")
        print("-----------------------------------------------")

    main(args, param)
