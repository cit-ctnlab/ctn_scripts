'''
mokuWG
This function sets outputs for waveform generator in Moku.
Input Arguments:
chan:       {'ch1'(default),'ch2' or 'both'}. Case insensitive.
ipAddress:  IP Address of Moku. Default is 10.0.1.81.
wf:         Waveform type. {'sine'(default), 'ramp' or 'square'}. Case
            insensitive. Can be a list for different values for different
            channels.
amp:        Amplitude in volts. {float, default=0.1}. Can be a list for
            different values for different channels.
freq:       Frequency in Hz. {float, default=10e3}. Can be a list for
            different values for different channels.
fiftyr:     50 Ohm inputs. {True, False(default)}. Can be a list for different
            values for different channels.
atten:      Turn on 10x attenuation. {True, False(default)}. Changes the
            dynamic range between 1Vpp(False) and 10Vpp(True). Can be a list
            for different calues for different channels.
ac:         AC coupling. {True, False(Default)}. Can be a list for different
            values for different channels.
off:        Turn off output. {True, False(Default)}. Can be a list for
            different values for different channels.
useExternal: Attempt to lock to an external reference clock.
             {True(default), False}.
'''

from pymoku import Moku
from pymoku.instruments import WaveformGenerator
import argparse
import numpy as np

def mokuWG(chan='ch1', ipAddress='10.0.1.81', wf='sine', amp= 0.1, freq=10e3,
           fiftyr=True, atten=False, ac=False, off=False, useExternal=True):
    chan = chan.lower()
    if chan == 'ch1':
        ch1 = True
        ch2 = False
    elif chan == 'ch2':
        ch1 = False
        ch2 = True
    elif chan == 'both':
        ch1 = True
        ch2 = True
    chList = [ch1, ch2]
    if not isinstance(wf, list):
        wf = [wf.lower(), wf.lower()]
    else:
        wf = [s.lower() for s in wf]
    if not isinstance(amp, list):
        amp = [amp, amp]
    if not isinstance(freq, list):
        freq = [freq, freq]
    if not isinstance(fiftyr, list):
        fiftyr = [fiftyr, fiftyr]
    if not isinstance(atten, list):
        atten = [atten, atten]
    if not isinstance(ac, list):
        ac = [ac, ac]
    if not isinstance(off, list):
        off = [off, off]

    for s in wf:
        if s not in ['sine', 'ramp', 'square']:
            raise RuntimeError('Only sine, ramp and square waves supported.')

    try:
        m = Moku(ipAddress)
        i = m.deploy_instrument(WaveformGenerator, use_external=useExternal)
        i.set_defaults()

        for ii, ch in enumerate(chList):
            chNo = ii+1
            if off[ii]:
                i.gen_off(chNo)
                print('Switching off Channel', chNo)
            elif ch:
                # Set front end settings
                i._set_frontend(chNo, fiftyr=fiftyr[ii], atten=atten[ii],
                                ac=ac[ii])
                if wf[ii]=='sine':
                    i.gen_sinewave(chNo, amplitude=amp[ii], frequency=freq[ii])
                    print('Ch',chNo,': Sine wave of Amplitude=',
                          amp[ii],'V at ', ftoss(freq[ii])+'Hz')
                elif wf[ii]=='ramp':
                    i.gen_rampwave(chNo, amplitude=amp[ii], frequency=freq[ii])
                    print('Ch',chNo,': Ramp wave of Amplitude=',
                          amp[ii],'V at ', ftoss(freq[ii])+'Hz')
                elif wf[ii]=='square':
                    i.gen_squarewave(chNo, amplitude=amp[ii],
                                     frequency=freq[ii])
                    print('Ch',chNo,': Square wave of Amplitude=',
                          amp[ii],'V at ', ftoss(freq[ii])+'Hz')
    finally:
        try:
            m.close()
        except BaseException:
            print('Failed to connect to moku properly.')

def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This function sets outputs for waveform generator in'
                    'Moku.')
    parser.add_argument('-i', '--ipAddress',
                        help='IP address of Moku. Default is 10.0.1.81',
                        default='10.0.1.81')
    parser.add_argument('-c', '--chan', type=str,
                        help='ch1(default), ch2 or both. Case insensitive.',
                        default='ch1')
    parser.add_argument('--wf1', type=str,
                        help='Channel 1 waveform. Ramp, Sine, Square.'
                             'Case insensitive. Default is Sine.',
                        default='sine')
    parser.add_argument('--wf2', type=str,
                        help='Channel 2 waveform. Ramp, Sine, Square.'
                             'Case insensitive. Default is Sine.',
                        default='sine')
    parser.add_argument('--amp1', type=float,
                        help='Channel 1 amplitude in volts. Default 0.1',
                        default=0.1)
    parser.add_argument('--amp2', type=float,
                        help='Channel 2 amplitude in volts. Default 0.1',
                        default=0.1)
    parser.add_argument('--f1', type=float,
                        help='Channel 1 frequency in Hz. Default 10e3',
                        default=10e3)
    parser.add_argument('--f2', type=float,
                        help='Channel 2 frequency in Hz. Default 10e3',
                        default=10e3)
    parser.add_argument('--MOhmr1',
                        help='1 MOhm input impedance for Ch 1. Default: False',
                        action='store_true')
    parser.add_argument('--MOhmr2',
                        help='1 MOhm input impedance for Ch 2. Default: False',
                        action='store_true')
    parser.add_argument('--atten1',
                        help='10x Attenuation for Ch1.'
                             'True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('--atten2',
                        help='10x Attenuation for Ch2.'
                             'True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('--ac1',
                        help='AC Coupling. Default: False',
                        action='store_true')
    parser.add_argument('--ac2',
                        help='AC Coupling for channel 2. Default: False',
                        action='store_true')
    parser.add_argument('--off1',
                        help='Turn off ch 1 output. Default: False',
                        action='store_true')
    parser.add_argument('--off2',
                        help='Turn off ch 2 output. Default: False',
                        action='store_true')
    parser.add_argument('--useInternal',
                        help='Use internal 10 MHz clock only. Default: False',
                        action='store_true')

    return parser.parse_args()

def ftoss(value):
    sgn = np.sign(value)
    if sgn ==0:
        return '0'
    else:
        absv = np.abs(value)
        ex = np.minimum(12,np.maximum(-15,np.floor(np.log10(absv))))
        sv = absv/(10**ex)
        sc = str(int(np.floor(ex/3)))
        unit = {
            '-5': 'f',
            '-4': 'p',
            '-3': 'n',
            '-2': 'u',
            '-1': 'm',
            '0' : '',
            '1' : 'k',
            '2' : 'M',
            '3' : 'G',
            '4' : 'T'
        }[sc]
        val = str(sgn*sv*10**(ex%3))
        return val+unit

if __name__ == '__main__':
    args = grabInputArgs()
    wf = [args.wf1, args.wf2]
    amp = [args.amp1, args.amp2]
    freq = [args.f1, args.f2]
    fiftyr = [not args.MOhmr1, not args.MOhmr2]
    atten = [args.atten1, args.atten2]
    ac = [args.ac1, args.ac2]
    off = [args.off1, args.off2]
    useExternal = not args.useInternal
    mokuWG(chan=args.chan, ipAddress=args.ipAddress, wf=wf, amp= amp,
           freq=freq, fiftyr=fiftyr, atten=atten, ac=ac, off=off,
           useExternal=useExternal)
