'''
BNspec
This script will take a beatnote timeseries measurement using Moku or SR785.
Then it will run mokuReadFreqNoise and will also create
a log file containing experiment configuration details in yaml format.
'''

from mokuReadFreqNoise import mokuReadFreqNoise
from mokuPhaseMeterTimeSeries import mokuPhaseMeterTimeSeries
from measTransRIN import measTransRIN
from measBNASDfromSR import measBNASDfromSR
from writeBNLog import writeLog
import argparse
import time
from epics import caget, caput
import os

BNFreqCH = "C3:PSL-PRECAV_BEATNOTE_FREQ"
gainCycleNCH = "C3:PSL-NCAV_FSS_FASTMON_RMS_GAINCYCLE_EN"
gainCycleSCH = "C3:PSL-SCAV_FSS_FASTMON_RMS_GAINCYCLE_EN"
requiredStates = ["C3:PSL-SCAV_FSS_RAMP_EN",
                  "C3:PSL-NCAV_FSS_RAMP_EN",
                  "C3:PSL-NCAV_PMC_SW2_IN",
                  "C3:PSL-SCAV_PMC_SW1_IN"]


def BNspec(detector='SN101', instrument='moku', fileRoot='Beatnote',
           ipAddress='10.0.1.81', duration=60, sampleRate='veryfast',
           bandWidth=10e3, atten=False, paramFile='CTN_BNSpec_SR785.yml',
           measRIN=False, reduceTSfile=False,
           chListFile='/home/controls/Git/cit_ctnlab/'
                      'ctn_scripts/BNSpecLogChannelList.txt'):
    if detector == 'SN101':
        BNuppLimit = 37.34   # MHz
        BNlowLimit = 17.34   # MHz
    else:
        BNuppLimit = 120     # MHz
        BNlowLimit = 0       # MHz
    dataFileName = None

    gainCycleNState = caget(gainCycleNCH)
    gainCycleSState = caget(gainCycleSCH)
    caput(gainCycleNCH, 0)
    caput(gainCycleSCH, 0)
    attemptNo = 1
    while ((dataFileName is None)
           and (attemptNo < 10)
           and ANDChannels(requiredStates)):
        print('Attempt No ', attemptNo)
        currBN = caget(BNFreqCH)
        withinLimits = ((currBN < BNuppLimit)
                        and (currBN > BNlowLimit))
        if withinLimits:
            if instrument == 'moku':
                dataFileName = mokuPhaseMeterTimeSeries(
                                    chan='ch1',
                                    ipAddress=ipAddress,
                                    duration=duration,
                                    sampleRate=sampleRate,
                                    bandWidth=bandWidth,
                                    fiftyr=True, atten=atten, ac=False,
                                    altFileRoot=fileRoot + 'TimeSeries',
                                    useExternal=True, useSD=False,
                                    fileType='bin')
                if dataFileName != 'Error':
                    if measRIN:
                        tempRINFile = measTransRIN()
                        if tempRINFile != 'Error':
                            RINFileName = dataFileName.replace('TimeSeries',
                                                               'TransRIN')
                            RINFileName = RINFileName.replace('.li', '.txt')
                            os.rename(tempRINFile, RINFileName)
                    print('Converting binary file into csv.')
                    os.system('~/Git/lireader/liconvert '
                              + dataFileName)
                    os.remove(dataFileName)
                    dataFileName = dataFileName.replace('.li', '.csv')
                    print(dataFileName + ' created.')
            elif instrument == 'sr785':
                dataFileName = measBNASDfromSR(
                                           paramFile,
                                           fileRoot=fileRoot + 'Spectrum')
                asdFileName = dataFileName
                if measRIN:
                    tempRINFile = measTransRIN()
                    if tempRINFile != 'Error':
                        RINFileName = dataFileName.replace('Spectrum',
                                                           'TransRIN')
                        os.rename(tempRINFile, RINFileName)

        # If BN frequency was in limits and measurement was succesful
        if dataFileName != 'Error' and withinLimits:
            print('Measurement succesful! Calculating ASD...')
            if instrument == 'moku':
                asdFileName = dataFileName.replace('.csv', '.txt')
                asdFileName = asdFileName.replace('TimeSeries',
                                                  'Spectrum')
                dataFileName = mokuReadFreqNoise(dataFileName=dataFileName,
                                                 asdFileName=asdFileName,
                                                 reduceTSfile=reduceTSfile)
            print('Saving Experiment Configuration...')
            logFileName = writeLog(asdFileName, chListFile, detector,
                                   instrument, duration)
            fileList = [dataFileName, asdFileName, logFileName]
        else:
            if not withinLimits:
                print('Beatnote frequency found to be',
                      currBN, 'MHz is outside detection range.')
                if dataFileName != 'Error':
                    if dataFileName is not None:
                        os.remove(dataFileName)
            print('Trying again in 5 seconds ...')
            dataFileName = None
            attemptNo = attemptNo + 1
            time.sleep(5)
    if measRIN and (tempRINFile != 'Error'):
        fileList += [RINFileName]
    # Reset gainCycle states.
    caput(gainCycleNCH, gainCycleNState)
    caput(gainCycleSCH, gainCycleSState)
    if not ANDChannels(requiredStates):
        print('Required States not met.')
    return fileList


# find boolean AND of a list of binary EPICs channels
def ANDChannels(chanList):
    return all([caget(ii) for ii in chanList])


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script runs moku or SR785 to take beatnote'
                    'Then it will run mokuReadFreqNoise and will also create'
                    'a log file containing experiment configuration details'
                    'in yaml format.')
    parser.add_argument('--instrument', type=str,
                        help='Moku or SR785',
                        default='Moku')
    parser.add_argument('-i', '--ipAddress',
                        help='IP address of Moku or SR785',
                        default='10.0.1.81')
    parser.add_argument('-b', '--bandWidth', type=float,
                        help='Phasemeter tracking bandwidth.'
                             'Default is 10kHz.',
                        default=10e3)
    parser.add_argument('--duration', type=int,
                        help='Measurement time in seconds. Default 60.',
                        default=60)
    parser.add_argument('--sampleRate', type=str,
                        help='Sampling Rate. {veryslow, slow , medium ,'
                             'fast, veryfast, ultrafast}. Refer moku manual.'
                             'Default is veryfast.',
                        default='veryfast')
    parser.add_argument('-a', '--atten',
                        help='10x Attenuation. True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('--detector', type=str,
                        help='Detector used. NF1811 or SN101(default)',
                        default='SN101')
    parser.add_argument('-c', '--chListFile', type=str,
                        help='ChannelList file with path to write in logs.',
                        default='/home/controls/Git/cit_ctnlab/'
                                'ctn_scripts/BNSpecLogChannelList.txt')
    parser.add_argument('--paramFile', nargs='?',
                        help='The parameter file for the measurement by SR785',
                        default='CTN_BNSpec_SR785.yml')
    parser.add_argument('--fileroot', type=str,
                        help='Alternate File Root Name. Default is Beatnote',
                        default='Beatnote')
    parser.add_argument('--measRIN',
                        help='Measure transmission RIN after beatnote '
                             'measurement.',
                        action='store_true')
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    args.instrument = args.instrument.lower()
    if args.instrument != 'moku' and args.instrument != 'sr785':
        raise RuntimeError('Instrument can be Moku or SR785 only.')
    BNspec(detector=args.detector, instrument=args.instrument,
           fileRoot=args.fileroot, ipAddress=args.ipAddress,
           duration=args.duration, sampleRate=args.sampleRate,
           bandWidth=args.bandWidth, atten=args.atten,
           paramFile=args.paramFile, measRIN=args.measRIN,
           chListFile=args.chListFile)
