#!/usr/bin/env python

import re
import time
import os
from epics import caget, caput
import ConfigParser
import argparse
import numpy as np

'''
Author: Anchal Gupta anchal.gupta@ligo.caltech.edu
Created: ~ May 15, 2019
'''

ChannelUpdateRate  = 0.100 # hard coded channel update rate

def main(args, param):
    '''
    Calculates lock quality for PDH locks. Uses the rms value of provided
    channels calculated over 10s of data and writes to a corresponding
    error rms and lock quality channel.
    Lock quality is defined as:
            Q_lock = -log10(rms(PDHErrorSignal)/|mean(DC_Reflection_Value)|)

    The rms of PDH error signal is scaled to meet the scale of DC refl value.
    '''

    # Eternal loop
    monitorTime = 10 # seconds of data to monitor
    arrayLength = int(monitorTime/ChannelUpdateRate)
    errorArray = np.zeros([ arrayLength ])
    DCvalArray = np.zeros([ arrayLength ])
    populated = False
    ii = 0 # create an index for monitorArray, start at zero
    while(True):
        startTime = time.time()
        for key,arr in param.channel.items():
            errorCH = arr[0]
            DCvalCH = arr[1]
            scaleVal = arr[2]
            rmsCH = arr[3]
            lqCH = arr[4]
            errorArray[ii] = caget(errorCH)
            DCvalArray[ii] = caget(DCvalCH)
            rms = np.std(errorArray)*scaleVal
            meanDCval = np.abs(np.mean(DCvalArray))
            lockQuality = max(-np.log10(rms/meanDCval),0)
            caput(rmsCH,rms)
            caput(lqCH,lockQuality)
        time.sleep(np.max([ChannelUpdateRate - (time.time() - startTime),0]))

def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='Python script for temporary killing gain in FSS loop and '
                    'softly ramping back into original state. Should be used '
                    'to killing ringing of loops when actuators rail.')
    parser.add_argument(
        'configfile',
        type=str,
        help='Provide a config .ini file name and location. Config file must '
             'include channel names and config settings for ramp on and off '
             'rates.')
    return parser.parse_args()


class ImportConfig():
    def __init__(self, configfile):
        '''
        initiate class and generate structure for object to call
        channel names from
        '''
        config = ConfigParser.ConfigParser()
        config.read(configfile)
        ii=0
        self.channel = {}
        while(True):
            ii=ii+1
            chan = 'CH'+str(ii)
            erChan = 'error'+chan
            DCChan = 'DCVal'+chan
            scaleVal = 'scale'+chan
            rmsChan = 'errorRMS'+chan
            lqChan = 'lockQual'+chan
            try:
                self.channel[chan] = [config.get("errorEPICSChannel",erChan),
                                      config.get("DCvalEPICSChannel",DCChan),
                                      config.getfloat("ScalerValues",scaleVal),
                                      config.get("errorRMSEPICSChannel",
                                                 rmsChan),
                                      config.get("lockQualEPICSChannel",
                                                 lqChan)]
            except:
                break

if __name__ == "__main__":
    args = grabInputArgs()
    param = ImportConfig(args.configfile)
    main(args, param)
