#! /usr/bin/env python
# Craig Cahillane - November 7, 2017

import os
import sys
import time
import select
import numpy as np
import argparse
sys.path.append('/dep')
import netgpib
import ConfigParser
from epics import caget, caput

usage = """

Stabilizes the Marconi-controlled PLL to a moving frequency reference.
This script relies on the precav beatnote frequency counter to first bring the PLL to the linear locking region.
Run this script for easy PLL locking with beatnote tracking.

Press ENTER to exit the script.  DO NOT PRESS CTRL-C, it will exit without closing the connection to the GPIB.

example:
python PLLautolocker exampleConfigFile.ini -b -2.0 -t 2.0 -s 1.5
"""

#ez = Ezca(ifo=None, logger=False) # start a global EPICS channel reader

# rails x to the limit of given range
def rail(x, minx, maxx):
        return max(min(maxx, x), minx)

def ANDChannels(chanList):  # find boolean AND of a list of binary EPICs chans
    return all([caget(ii) for ii in chanList])

def grabInputArgs():
    # Parse options
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('configfile', type=str,
                      help="Enter a config .ini file + path")
    parser.add_argument('-a', '--gpibaddress', type=int, default=17,
                      help='GPIB device address.  Default is 17')
    parser.add_argument('-i', '--ip', default='10.0.1.63',
                      help='IP address/Host name.  Default is 10.0.1.63')
    parser.add_argument('-c', '--carrier', type=float,
                      help='Carrier Frequency in Hz. Default is current Precav Frequency Counter Beatnote Freq.')
    parser.add_argument('-r', '--rflevel', type=float,
                      help='Carrier Frequency Strength in dBm. Default is current Marconi settings.')
    parser.add_argument('-f', '--fm', type=float,
                      help='Frequency Modulation (FMDevn) in Hz/Vrms. Default is current Marconi settings.')
    parser.add_argument('-s', '--actuation', type=float,
                      help='Float. Actuation strength scaler on the FM Devn number. Make negative to actuate in opposite direction.')
    parser.add_argument('-b', '--bottomRail', type=float,
                      help='Float. Defines the bottom voltage rail at which the PLL loses lock.  Default is -2.0.')
    parser.add_argument('-t', '--topRail', type=float,
                      help='Float. Defines the top voltage rail at which PLL loses lock.  Default is 2.0.')
    parser.add_argument('-l', '--log', action='store_true',
                      help='Flag.  If set, script will log GPIB commands.')
    parser.add_argument('-d', '--debug', action='store_true',
                      help='Flag.  If set, print statements to console.')
    return parser.parse_args()

class ImportConfig():
    def __init__(self, configfile):
        config = ConfigParser.ConfigParser()
        config.read(configfile)
        self.PrecavBeatnoteFreqChannel = config.get("EPICSChannelConfig","PrecavBeatnoteFreqChannel") # Read only
        self.PLLControlSignalChannel = config.get("EPICSChannelConfig","PLLControlSignalChannel") # Read only
        self.PLLCarrierFreqChannel = config.get("EPICSChannelConfig","PLLCarrierFreqChannel") # Read Write
        self.PLLFreqModChannel = config.get("EPICSChannelConfig", "PLLFreqModChannel") # Read Write
        self.PLLBeatnoteFreqChannel = config.get("EPICSChannelConfig", "PLLBeatnoteFreqChannel") # Read Only
        self.PLLBottomRailChannel = config.get("EPICSChannelConfig", "PLLBottomRailChannel") # Read Write
        self.PLLTopRailChannel = config.get("EPICSChannelConfig", "PLLTopRailChannel") # Read Write
        self.PLLActuationChannel = config.get("EPICSChannelConfig", "PLLActuationChannel") # Read Write
        self.PLLBlinkChannel = config.get("EPICSChannelConfig", "PLLBlinkChannel") # Read Only
        self.PLLTimestepChannel = config.get("EPICSChannelConfig", "PLLTimestepChannel") # Read Only
        self.EPICS_channel_config_all = config.items("RequiredANDChannels") # Read Only
def main():
    # Setup
    args = grabInputArgs() # get command line args
    param = ImportConfig(args.configfile)  # import config file
    FMScaler = 1.0#agupta: Made values in software same as actual   0.7071068 #awade: added correction to Hz_peak units

    # Create/connect to netGPIB object
    if args.debug:
        print 'Connecting to %s...' % (args.ip),
    gpibObj = netgpib.netGPIB(args.ip, args.gpibaddress, '\004', 0, log=args.log)
    if args.debug:
        print 'Done'
    gpibObj.command('*CLS')
    gpibObj.command('*RST')
    gpibObj.command('FSTD EXT10DIR')
    gpibObj.command('MODE FM')
    gpibObj.command('FM:EXTDC')
    gpibObj.command('FM:ON')
    gpibObj.command('MOD:ON')
    gpibObj.command('RFLV 13')

    # Make channel list
    ANDChannelList = np.array([])
    for tempChan in param.EPICS_channel_config_all:
        ANDChannelList = np.append(ANDChannelList, tempChan[1])


    # Set user defined arguments on the actual Marconi
    if args.carrier is not None: # Change to user defined initial carrier frequency
        carrierFreqStr = str(args.carrier)
        cmd = 'CFRQ ' + carrierFreqStr
        gpibObj.command(cmd)
    else: # if no carrier frequency argument, use the frequency counter which is reported in MHz
        carrierFreqStr = str(caget(param.PrecavBeatnoteFreqChannel) * 1e6) # read the soft channel and convert to Hz
        cmd = 'CFRQ ' + carrierFreqStr
        gpibObj.command(cmd)
    if args.rflevel is not None: # Change to user defined radio frequency signal level
        rfStr = str(args.rflevel)
        cmd = 'RFLV ' + rfStr
        gpibObj.command(cmd)
    if args.fm is not None: # Change to user defined frequency modulation
        fmStr = str(args.fm)
        cmd = 'FM:DEVN ' + fmStr
        gpibObj.command(cmd)

    ### Write user arguments to the soft channels designated for controlling the autolocker ###
    # PLL actuation controls what way we push the carrier frequency when the control voltage starts to get too high or too low
    # For the PSL lab right now, decreasing the carrier freq increases the control voltage.
    if args.actuation is not None:
        caput(param.PLLActuationChannel, args.actuation)
    # These variables are the voltage rails at which the PLL will lose lock.
    if args.topRail is not None:
        caput(param.PLLTopRailChannel, args.topRail)
    if args.bottomRail is not None:
        caput(param.PLLBottomRailChannel, args.bottomRail)

    # Query the Marconi for initial settings
    queryCF = gpibObj.query('CFRQ?') # always returns in units of Hz
    curCF = float(queryCF.split(';')[0].split(' ')[-1])
    queryFM = gpibObj.query('FM:DEVN?')
    curFM = float(queryFM)

    # caput to the soft PLL channels
    caput(param.PLLCarrierFreqChannel, curCF*1e-6)
    caput(param.PLLFreqModChannel, curFM*1e-3*FMScaler)

    checkTime = 10 # how many seconds between beatnote reality checks
    deltaBeatnote = 0.1 # max allowable difference in beatnote frequencies (in MHz, so this is 100 kHz)
    startTime = time.time()
    lastCheckTime = time.time()
    while True:
        if not ANDChannels(ANDChannelList):
            if args.debug:
                print 'PLL Autolocker not engaged.'
                print 'The following required channels are not activated:'
                for chan in ANDChannelList:
                    if chan == False:
                        print chan
            continue

        # Set Marconi FM to whatever the user wants all the time
        queryFM = gpibObj.query('FM:DEVN?')
        curFM = float(queryFM)
        softFM = caget(param.PLLFreqModChannel)*1e3/FMScaler
        if np.abs(softFM - curFM) > 0.01: # If EPICS soft FM channel is not the same as the Marconi FM
            cmd = 'FM:DEVN ' + str(softFM)
            gpibObj.command(cmd)
            curFM = softFM

        # Read controller EPICS channel values
        controlVoltage = caget(param.PLLControlSignalChannel)
        highRail = caget(param.PLLTopRailChannel)
        lowRail = caget(param.PLLBottomRailChannel)

        railRange = highRail - lowRail # get total control signal range
        controlPercent = (controlVoltage - lowRail) / railRange # calculate how far through the range we are, normalized

        if controlPercent < 0.0: # if we are past the bottom rail
            # Get carrier frequency and actuation strength from soft channels
            actuation = caget(param.PLLActuationChannel)
            curCF = caget(param.PLLCarrierFreqChannel)*1e6 # units of Hz from MHz

            # Set Marconi to new carrier frequency using current FM value
            curCF -= actuation * curFM
            cmd = 'CFRQ ' + str(curCF)
            gpibObj.command(cmd)

            # Write new carrier frequency to soft epics channels
            caput(param.PLLCarrierFreqChannel, curCF*1e-6)

        elif controlPercent > 1.0: # if we are above the top rail
            # Get carrier frequency and actuation strength from soft channels
            actuation = caget(param.PLLActuationChannel)
            curCF = caget(param.PLLCarrierFreqChannel)*1e6 # units of Hz from MHz

            # Set Marconi to new carrier frequency using current FM value
            curCF += actuation * curFM
            cmd = 'CFRQ ' + str(curCF)
            gpibObj.command(cmd)

            # Write new carrier frequency to soft epics channels
            caput(param.PLLCarrierFreqChannel, curCF*1e-6)

        if (time.time() - lastCheckTime) > checkTime: # If it's been 10 seconds since the last check
            PLLBeatnoteFreq = caget(param.PLLBeatnoteFreqChannel) # read PLL Autolocker calculated beatnote
            PrecavBeatnoteFreq = caget(param.PrecavBeatnoteFreqChannel) # read Precav frequency counter beatnote
            if np.abs(PLLBeatnoteFreq - PrecavBeatnoteFreq) > deltaBeatnote: # if the difference is too high
                if args.debug:
                    print 'PLL lost lock'
                    print 'Calculated PLL Beatnote Frequency = ', PLLBeatnoteFreq, 'MHz'
                    print 'Precav Beatnote Frequency = ', PrecavBeatnoteFreq, 'MHz'
                    print 'Setting Marconi Carrier Frequency to Precav Beatnote Frequency:'
                cmd = 'CFRQ ' + str(PrecavBeatnoteFreq*1e6) # Always send Hz to the Marconi
                gpibObj.command(cmd)
                caput(param.PLLCarrierFreqChannel, PrecavBeatnoteFreq) # write Precav BN to PLL BN
            lastCheckTime = time.time() # reset the check time

        if args.debug:
            print 'Carrier Frequency is ' + str(curCF*1.0e-6) + ' MHz'
            if sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
                line = raw_input()
                break
        # Blink the light, read the timestep, and sleep
        caput(param.PLLBlinkChannel, not caget(param.PLLBlinkChannel))
        timestep = caget(param.PLLTimestepChannel) # read the time step, probs 0.1 seconds
        time.sleep(timestep) # sleep for remaining time of the loop
    gpibObj.close()

if __name__ == "__main__":
    main()
