'''
syncTSfile
Copies all time series data from ws1 at CTN to local computer for pushing them
to google cloud. Automatically gets triggered at 01:00 AM everyday and would
commit the new data generated from WS1, put it in local computer and then
copy the time series files to local computer.
'''
import os
import argparse
import time
from os.path import expanduser as eu


def syncTSfile(gitDir, triggerTime='01', triggerNow=False,
               daysBack=1,
               branchName='master',
               commitMessage='Automatically adding last day data'):
    if gitDir[0] == '/':
        secondSlash = gitDir[1:].find('/') + 1
        thirdSlash = gitDir[secondSlash+1:].find('/') + secondSlash
        gitDir = '~' + gitDir[thirdSlash+1:]
    while(1):
        timeStr = time.strftime('%H', time.localtime())
        if timeStr == triggerTime or triggerNow:
            print('Triggered for file transfer at', timeStr , 'Hr')
            yesterday = time.localtime(time.mktime(time.localtime())
                                       - daysBack * 24 * 3600)
            direcName = time.strftime('Data%m%d', yesterday)
            with open('temp.sh', 'w') as f:
                f.write('#!/bin/sh\n')
                # Script running in remote computer
                f.write('ssh controls@131.215.115.216 << HERE\n')
                f.write('#!/bin/sh\n')
                # Add ssh agent
                f.write('ssh-agent bash\n')
                f.write('ssh-add /home/controls/.ssh/anchal_id_rsa \n')
                f.write('cd '+ gitDir + '\n')
                # switch to branch you want to use
                f.write('git checkout ' + branchName + '\n')
                # Git pull first
                f.write('git pull\n')
                # List all time series files
                f.write('cd '+ direcName + '\n')
                f.write('stat -c "%n %s" ')
                f.write('*TimeSeries* >> list_Time_Series_Files.txt\n')
                f.write('cd ..\n')
                # add all added/modified files
                f.write('git add '+ direcName + '/.\n')
                # commit changes
                f.write('git commit -m \"' + commitMessage + '\"\n')
                # push to git remote repository
                f.write('git push\n')
                f.write('HERE\n')
                # Back to local computer
                f.write('cd '+ gitDir + '\n')
                # switch to branch you want to use
                f.write('git checkout ' + branchName + '\n')
                # Git pull first
                f.write('git pull\n')
                # Go to new directory
                f.write('cd '+ direcName + '\n')
                # Copy time series files
                f.write('scp controls@131.215.115.216:'
                        + os.path.join(gitDir, direcName) + '/*TimeSeries* ./')
            os.system('chmod +x temp.sh')
            os.system('./temp.sh')
            os.remove('temp.sh')
            # Check if all files got transferred
            fn = eu(os.path.join(gitDir, direcName,
                                 'list_Time_Series_Files.txt'))
            with open(fn, 'r') as f:
                allLines = f.read().splitlines()
                fileList = {}
                for line in allLines:
                    fn = line.split(' ')[0]
                    size = line.split(' ')[1]
                    fileList[fn] = int(size)
            allGood = True
            for fn in fileList:
                fnPath = eu(os.path.join(gitDir, direcName, fn))
                if not os.path.exists(fnPath):
                    print(fn, ' not found')
                    allGood = False
                elif os.path.getsize(fnPath) != fileList[fn]:
                    print(fn, ' not trasferred properly')
                    print('Size on remote:', fileList[fn])
                    print('Size on local:', os.path.getsize(fnPath))
                    allGood = False
            if allGood:
                print('All files were transferred succesfully')
            else:
                print('Stopping. Please inspect files.')
        if triggerNow:
            break

def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='Copies all time series data from ws1 at CTN to local '
                    'computer for pushing them to google cloud. Automatically '
                    'gets triggered at 01:00 AM everyday and would commit the '
                    'new data generated from WS1, put it in local computer '
                    'and then copy the time series files to local computer.')
    parser.add_argument('gitDir', type=str,
                        help='Git directory path with ~. Must be same in ws1 '
                             'and local computer.')
    parser.add_argument('-t', '--triggerTime', type=str,
                        help='hh string when to start. Default is 01.',
                        default='01')
    parser.add_argument('--triggerNow',
                        help='Would trigger once and exit.',
                        action='store_true')
    parser.add_argument('-d', '--daysBack', type=int,
                        help='Number of days ago data to be synced.'
                             'Default is 1',
                        default=1)
    parser.add_argument('-b', '--branchName', type=str,
                        help='git branch name. Default is master.',
                        default='master')
    parser.add_argument('-m', '--commitMessage', type=str,
                        help='Commit message. '
                             'Default is: Automatically adding last day data.',
                        default='Automatically adding last day data')
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    syncTSfile(gitDir=args.gitDir, triggerTime=args.triggerTime,
               triggerNow=args.triggerNow,
               daysBack=args.daysBack,
               branchName=args.branchName,
               commitMessage=args.commitMessage)
