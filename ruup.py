#!/usr/bin/env/ python

import time
import subprocess, platform
from ezca import Ezca
import ConfigParser
import argparse

'''
Author: Andrew Wade (awade) - awade@ligo.caltech.edu
Created: Sep 30, 2016
Script pings a list of IP and channel pairs (<IP>:<ChannelName>), defined in an external config file and updates the state of binary soft channels to show if networked devices are online. These can be used as conditions for other scripts or displaced in medm screens on sites.

Change log:
'''

RCPID = Ezca(ifo=None)  # initialize ezca python access to channels object

def main():
  args = grabInputArgs()  # import cmdline/bash argments
  param = ImportConfig(args.configfile)  # import config file into variable object called param
  if args.debug == 1:  # debug mode: print message
    print("Are-you-up started with config file {}. \n".format(args.configfile))
  
  blinkystatus = 0  # initiate blink status, used to show script is alive (if soft channel is in config file)
  while(1): 
      for IPval, chanval in param.pingList:
        RCPID.write(chanval, pingResponse(IPval), monitor=False)
      
      blinkystatus = 0 if blinkystatus else 1  # blink the blinky light
      if (not param.EPICSBlinkChan == ""):  # check if a blink channel is given
         RCPID.write(param.EPICSBlinkChan, blinkystatus, monitor=False)  # write out to blink inidicator channel, if it is specified 
      time.sleep(param.pingInterval) # wait to poll again






# this def pings a host and returns true/flase state if ping responds/doesn't respond
def pingResponse(sHost):
  try:
    output = subprocess.check_output("ping -c 1 -W 0.2 {}".format(sHost), shell=True)  # add -W flag to put upper bound, in seconds, for wait time on a ping, this might be needed if large number of hosts are down.
  
  except Exception, e:

    return False
  
  return True



def grabInputArgs():  # get input argments passed to the script from the command line 
  parser = argparse.ArgumentParser(description="python script pings list of IP addresses and updates state of binary EPICS channels")
  parser.add_argument('configfile',type=str,help="Here you must enter a config .ini file name and location that contains the list of IP addresses and EPICS channels to write states out to. ")
  parser.add_argument('--debug', action='store_true')
  
  return parser.parse_args()  # Grabs arguments parsed in this function and puts into object variables of initiated instance


class ImportConfig():
  def __init__(self,configfile):  # initiate class and generate structure for object to call channel names from
    config = ConfigParser.ConfigParser()
    config.read(configfile)
    
    try: 
      self.pingInterval = config.getfloat("ScriptConfig","pingInterval")  # channel name of time step in seconds between loop iterations except:  # case user has not specified a soft channel for loop update rate
    except:
      self.pingInterval = 1 # defults time parameter to 1 second (slow control)

    try:
      self.EPICSBlinkChan = config.get("ScriptConfig","EPICSBlinkChan")  # Define soft channel used to blink on front pannel, this is useful to have a soft channel blink a medm indicator.
    except:  # case user has not specified a blink channel defult to none
      self.EPICSBlinkChan = ""
    
    # Collect the list of IP and EPICS channel name pairs to poll
    buff_IPChanPairs = config.get("IPAndChannelList","IPChanPair").splitlines() # IP and channel list split into a list item per line.
    self.pingList = []  # initializ
    for elemt in buff_IPChanPairs:
      self.pingList.append(elemt.split(":"))  # break up list elements using : delimiter
    
    # obtain list of config settings for debug mode print
    self.IPAndCHannelList_alll = config.items("IPAndChannelList")
    self.ScriptConfig_all = config.items("ScriptConfig")


if __name__ == "__main__": #triger main on call
  main()
