#!/usr/bin/env python

import re
import time
import os
from ezca import Ezca
import ConfigParser
import argparse
from numpy import sin
from math import pi


'''
Author: Kira
'''

RCPID = Ezca(ifo=None)  # initialize ezca python access to channels object


#def main():
#  args = grabInputArgs()  # import cmdline/bash argments
#  param = ImportConfig(args.configfile)  # import config file into variables
#  param.actuator = 'C3:PSL-HEATERN_OUT'
#  if args.debug == 1:  # debug mode: print message
#    print("-----------------------------------------------")
#    print("Channel configuration:")
#    print(param.EPICS_channel_config_all)
#    print("")
#    print("Hard stops and slew rate settings for  actuator:")
#    print(param.hard_actuator_limits_all)
#    print("-----------------------------------------------")


  # Initiate some variables
  
  #u_init = RCPID.read(param.actuator, log=False)
  # Variables
  #u = [u_init]*4 # outputs to the actuator
  #e = [0]*4 # error signal
      
 #perform the actuation
  #RCPID.write(param.actuator, u[0], monitor=False)


#def grabInputArgs():  # get input argments passed to the script from the command line
#  parser = argparse.ArgumentParser(description="PID control script that reads out and controls EPICS channels")
#  parser.add_argument('configfile',type=str,help="Here you must enter a config .ini file name and location that contains the channels names for process, actuator and set point as well as the soft channels for setting PID settings and hard limits. Also included should be an list of channels that must be true for loop to engage")
#  parser.add_argument('--debug', action='store_true')
#  return parser.parse_args()  # Grabs arguments and puts into a local variable

channel = 'C3:PSL-HEATERN_OUT'
print 'channel ' + channel + ' = ', RCPID.read(channel)

t=0;
n=1;
tn = [];
f = [];
while (t<=14):
  start_time = time.time()
  tn.append(t);
  f.append(0.2*sin(2*pi*t/10));
  n = n+1;
  t = t+0.1;

  RCPID.write(channel, f[-1], monitor=True)

  time.sleep( 0.1 - (time.time() - start_time) );

def rail(n, minn, maxn):  # test if argument is outside and range and rails it to the limit of that range if it goes over
    return max(min(maxn, n), minn)


#if __name__ == "__main__": #triger main on call
#  main()
