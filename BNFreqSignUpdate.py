import time
from epics import caget, caput
import ConfigParser
import argparse
import numpy as np

'''
Author: Anchal Gupta (agupta) - anchal@caltech.edu
Created:  Dec 25, 2018

This code just checks BN Frequency all the time and sets the EPICS channel
for its sign in case a flip has occured
'''
BNFreqCH = "C3:PSL-PRECAV_BEATNOTE_FREQ"
BNFreqSignCH = "C3:PSL-PRECAV_BEATNOTE_FREQ_SIGN"
timestep = 1
lookBackTime = 5
# Keep this time 2 times the polling period of precav bn freq
investigationTime = 2
# Sign will be changed below cutoff only
freqCutOff = 5  # MHz
# Reset to looking for trigger after
# Long times will miss sudden twice changes
# But they are rare, so it is fine.
resetTime = 60 # s

recLen = int(lookBackTime/timestep)
BNFreqTS = np.zeros(recLen)


def checkFreqFlip(BNFreq):
    #Freq Counter has 1 Hz resolution from 1-40 MHz.]
    if not np.any(BNFreq-np.roll(BNFreq,1)):
        print("Investigating frequency flip...")
        val1 = caget(BNFreqCH)
        time.sleep(investigationTime)
        val2 = caget(BNFreqCH)
        if np.abs(val1 - val2)<0.000002:
            if BNFreq[0]<10:
                print("Frequency flip found.")
                return True
            else:
                print("False Alarm.")
                return False
    else:
        return False


while(1):
    Sign  = caget(BNFreqSignCH)
    BNFreqTS = np.roll(BNFreqTS,1)
    BNFreqTS[0] = caget(BNFreqCH)
    if checkFreqFlip(BNFreqTS):
        Sign = int(np.mod(Sign+1,2))
        caput(BNFreqSignCH,Sign)
        print("Sign changed")
        print("Going down for ",resetTime, " seconds...")
        time.sleep(resetTime)
        print("Trigger ready again.")
    time.sleep(timestep)
