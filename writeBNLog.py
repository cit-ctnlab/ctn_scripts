'''
writeLog
This script reads channels in the given channelListFile and reads them from
EPICS channel to create a log file for an associated ASD measurement. Mainly
for the beatnote spectrum measurement.
Used by dailyBNspec.py
'''

import argparse
import yaml
from epics import caget
import numpy as np


def writeLog(asdFileName, chListFile, detector, instrument, duration):
    logFileName = asdFileName.replace('.txt', '.yml')
    logFileName = logFileName.replace('Spectrum', 'ExpConfig')
    with open(chListFile, 'r') as clf:
        chList = clf.readlines()

    dictToWrite = {}
    # Remove any CR or LF and then read the channels and store in a dict
    for ch in chList:
        ch = ch.replace('\n', '')
        ch = ch.replace('\r', '')
        value = caget(ch)
        dictToWrite[ch] = value

    # Add Detector and instrument
    dictToWrite['detector'] = detector
    dictToWrite['instrument'] = instrument
    dictToWrite['duration'] = duration
    with open(logFileName, 'w') as logFile:
        yaml.safe_dump(dictToWrite, logFile)

    return logFileName


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script runs mokuReadFreqNoise on frequency data of '
                    'beatnote measured by moku. In addition, it also reads '
                    'channels written in ChannelList file and logs them in '
                    'a log file. Detector information is also written on '
                    'the log file.')
    parser.add_argument('channelListFile', type=str,
                        help='Channel List File')
    parser.add_argument('-f',
                        '--asdFileName',
                        help='ASD filename with .txt extension.'
                             'Used to create corresponding log file.',
                        default='BeatnoteSpectrum.txt')
    parser.add_argument('-d', '--detector', type=str,
                        help='Detector used. NF1811 or SN101(default)',
                        default='SN101')
    parser.add_argument('-i', '--instrument', type=str,
                        help='Moku(default) or SR785',
                        default='Moku')
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    writeLog(args)
