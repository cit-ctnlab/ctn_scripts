import re
import time
import os
from ezca import Ezca
import ConfigParser
import sys
import argparse

# Craig Cahillane 7/31/17
# Build a laser frequency sweeper to slowly automatically look for a beatnote between the lasers.
# This works by incrementing the laser slow control voltage by a little bit each loop time step.
# This reads in the "{Path}_WindshieldWiper.ini" channel files, where {Path} is "North" or "South". 
# Potentially this could be used with an automatic beatnote finder with demodulation at a certain frequency 
# and a monitor which looks for a "spike" corresponding to a beatnote being found at the demod freq.

#test if argument is outside and range and rails it to the limit of that range if it goes over
def rail(number, lower_limit, upper_limit):
  if number<lower_limit:
    number = lower_limit
  elif number>upper_limit:
    number = upper_limit
  return(number)

# Read in arguments
parser = argparse.ArgumentParser()
parser.add_argument('path', type=str, nargs='+', help='Choose which path to sweep the slow laser control voltage. Either "North" or "South"')
parser.add_argument('-d', '--direction', type=int, help='Choose which direction you want the laser to start sweeping.  "-1" means down, literally anything else means up')
args = parser.parse_args()
print args.path
print args.direction

if args.direction == -1:
  upOrDown = -1.0
else:
  upOrDown = 1.0

path = args.path[0] #argparse always returns a list.  This gets the first item in that list
if path[0] == 'N' or path[0] == 'n':
  pathName = 'North'
elif path[0] == 'S' or path[0] == 's':
  pathName = 'South'
else:
  print
  print 'Neither the North or South path was selected, using South path automatically in '
  for ii in range(5):
    print str(5-ii)
    sys.stdout.flush()
    time.sleep(1)
  pathName = 'South'

configFileName = pathName + '_WindshieldWiper.ini'
#####Actual Process Code Starts Here#####

RCPID = Ezca(ifo=None)

config = ConfigParser.ConfigParser()
config.read(configFileName)

laserpath = config.get("EPICSChannelConfig", "laserpath")
timestep  = config.get("EPICSChannelConfig", "timestep")

# Load more actuator limit parameters -- these ones actually have to be numbers
hard_stops = [config.getfloat("HardActuatorLimits","hardstops_upper"),config.getfloat("HardActuatorLimits","hardstops_lower")]
increment_limit = config.getfloat("HardActuatorLimits","incrementlimit")  # this is like a slew rate limit on the actuator

currentVoltage = RCPID.read(laserpath, log=False)
print('Starting '+ pathName[0] +'CAV voltage = ' + str(currentVoltage) + '\n')

# Start wipers
while(1):
  tt = RCPID.read(timestep, log=False)
  # Sleep for the rest of the time interval
  time.sleep(tt)
  
  currentVoltage = RCPID.read(laserpath, log=False)  
  nextVoltage = currentVoltage + upOrDown * increment_limit
  
  checkIfRailed = rail(nextVoltage, hard_stops[0], hard_stops[1])  
  if checkIfRailed is not nextVoltage: # if the next voltage is too high or too low, reverse wipers
    upOrDown = -1.0 * upOrDown  
    continue

  RCPID.write(laserpath, nextVoltage, monitor=False)





