#!/usr/bin/env python

import time
from epics import caget, caput
try:
    import ConfigParser
except BaseException:
    import configparser as ConfigParser
import argparse
import numpy as np

'''
Author: Andrew Wade (awade) - awade@ligo.caltech.edu
Created:  Feb 13, 2018 (from an earlier port of a perl script used at 40m)

This PID looper has been improved with insight drawn from xxx, chapter 10.  It
includes filtered derivative of the process output and anti-windup protection.
These changes will help to limit theimpact of higher frequency noise on the
sensor and prevent instablity induced by hitting the saturation limits of the
actuator.  It also does away with the finite difference approximation that can
lead to rounding error induced non-orthogonality of P, I and D components.

Change log:
    20180214 (awade): removed all debug and comment junk
'''

#RCPID = Ezca(ifo=None, logger=False)  # ezca python access to channels object


def main(args, param):

    blinkystatus = 0

    u_init = caget(param.actuator)
    print("Starting  value of actuator = " + str(u_init) + "\n")
    #  init Variables
    D = u = 0
    uprev = u_init
    avoidedJump = 0
    pprev = caget(param.process)
    I = caget(param.actuator)

    while(1):
        timestep = caget(param.timestep)

        blinkystatus = 0 if blinkystatus else 1  # blink the blinky light
        if (not param.EPICSBlinkChan == ""):
            caput(param.EPICSBlinkChan, blinkystatus)

        if ANDChannels(param.requiredstates):  # all binary chans true
            # Read sign of PID parameters to use
            if param.PIDsign is not None:
                PIDsign = -2.0*(caget(param.PIDsign)) + 1.0
            else:
                PIDsign = 1.0
            # Read the PID parameters, in case they have changed
            Kp = caget(param.KpParam)*PIDsign
            Ki = caget(param.KiParam)*PIDsign
            Kd = caget(param.KdParam)*PIDsign

            if param.TfParam is not None:
                Tf = caget(param.TfParam)
            else:
                Tf = 0

            if param.TtParam is not None:
                Tt = caget(param.TtParam)
            else:
                Tt = 0  # Set default, case zero hard rails intergrator

            # Scale by loop update rate and wind-up and filter time constants
            kf = Tf / (Tf + timestep)  # kf defines filtering on D term
            kd = Kd / (Tf + timestep)
            ki = Ki * timestep
            if Tt < 0.0001:
                kt = 1.0
            else:
                kt = timestep / Tt  # kt defines strength of windup supression

            # Read the current process and setpoint, compute error
            s = caget(param.setpoint)
            p = caget(param.process)
            e = s - p

            #  handle case where user manually changed actuator value
            ManChangeThresh = 0.001  # A manual threshold trigger reset
            if abs(caget(param.actuator) - u) > ManChangeThresh:
                I = caget(param.actuator)  # kill intergrator history
                pprev = p  # kill derivative history
                D = 0
                #  print("Manual slider  change")

            #  compute P and D values and actuator output
            P = Kp * e
            D = kf * D - kd * (p - pprev)
            v = P + I + D + param.offset
            u = np.clip(v, param.hard_stops[0], param.hard_stops[1])

            caput(param.actuator, u)

            # Compute values for next iteration of loop
            I = I + ki * e + kt * (u - v)
            pprev = p
            uprev = u
        else:
            I = caget(param.actuator)  # kill intergrator history
            pprev = caget(param.process)  # kill derivative history
            D = 0
        time.sleep(timestep)


def RelayAutoTune(args, param):
    ''' This function finds a best first guess of PID parameters by driving
        plant with a relay function to find the critical frequency.

        The method for estimating critical period at 180 degree by counting
        steps between relay flips and taking maximum/minimum values of each
        cycle.  This avoids the need for any fancy filtering or fitting.
        To exclude the initial settling time and false triggers induced by
        sensor noise median averaging is used to find the most likely value.

        The period and amplitude of the induced oscillations can be use to
        estimate Kp, Ki and Kd using modified Zeigler-Nichols estimates.
        Standard text book values that use the critical period and amplitude
        values typically give values that are excessively oscilllatory.
        Values used in this function use the David Wilson's "Relay-based PID
        Tuning" technical note Table I values (see http://www.processcontr
        olstuff.net/wp-content/uploads/2015/02/relay_autot-2.pdf).

     '''

    # initiate some values
    if args.relayOffset is None:
        I0 = caget(param.actuator)  # initial offset from current value
    else:
        I0 = args.relayOffset
    relayAmp = args.relayAmp
    runtime = args.autolockDuration
    timestep = caget(param.timestep)

    eprev = epprev = 0  # initiate values that track previous two values
    countCrossings = maxCycle = minCycle = np.array([])
    count = maxRelay = minRelay = 0  # reset counters that track period and amp

    # run the relay test
    for ii in range(int(runtime/timestep)):  # loop for runtime seconds
        # read and compute error signal
        s = caget(param.setpoint)
        p = caget(param.process)
        e = s - p

        # count steps since last upward crossing and hold max/min
        count += 1
        maxRelay = max(maxRelay, e)
        minRelay = min(minRelay, e)

        # simple relay func drives actuator according to the sign of the error
        caput(param.actuator, I0 + relayAmp * np.sign(e))

        # Use previous two points to reject spurious crossings from noise
        # This is crude, averaging would be better, but it works ok
        if e > 0 and not eprev > 0 and not epprev > 0:  # case xing first time
            if args.debug:
                print("Count = {count},"
                      "min = {min},"
                      "max = {max}".format(count=count,
                                           min=minRelay,
                                           max=maxRelay))
            countCrossings = np.append(countCrossings, count)  # save num steps
            minCycle = np.append(minCycle, minRelay)  # save min in this cycle
            maxCycle = np.append(maxCycle, maxRelay)  # save max in this cycle
            count = maxRelay = minRelay = 0  # clear counts for next cycle
        epprev = eprev
        eprev = e  # shuffle val for next itteration of loop

        time.sleep(timestep)  # wait one unit step in time

    # Compute median average values, median is better at rejecting glitches
    Tc = np.median(countCrossings) * timestep  # Tc critical period
    ampProcess = np.median(maxCycle) - np.median(minCycle)
    Kc = 4 * relayAmp/ampProcess/np.pi  # compute critical gain

    # These are modified PID Zeigler-Nichols tuning constants suggest by by
    # by David I. Wilson in "Relay-based PID Tuning" technical note
    kp = 0.2 * Kc
    ki = kp / (0.5 * Tc)
    kd = kp * (Tc/3)

    if args.debug:
        print("Time between relay flip = {}".format(countCrossings * timestep))
        print("Min amplitudes = {}".format(minCycle))
        print("Max amplitudes = {}".format(maxCycle))
        print("Critical period Tc = {}".format(Tc))
        print("Critical gain Kc = {}".format(Kc))
        print("Suggested kp, ki, kd are {kp}, {ki}, {kd}".format(kp=kp,
                                                                 ki=ki,
                                                                 kd=kd))

    if args.estimateOnly:
        print("Critical period Tc = {}".format(Tc))
        print("Critical gain Kc = {}".format(Kc))

    caput(param.actuator, I0)  # return to original value

    return kp, ki, kd


def ANDChannels(chanList):  # find boolean AND of a list of binary EPICs chans
    return all([caget(ii) for ii in chanList])


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description="PID control script that reads out and controls EPICS"
                    " channels")
    parser.add_argument('configfile',
                        type=str,
                        help="Enter a config .ini file + path")
    parser.add_argument('--autotune',
                        action='store_true',
                        help="This flag activates autotuner.")
    parser.add_argument('-d', '--relayAmp',
                        type=float,
                        help="Define when using the relay autotuner feature."
                             "Provide value for size of relay step function "
                             "about the actuator mean value. When choosing "
                             "value start small enough that oscillations do "
                             "not saturate the actuator.")
    parser.add_argument('-o', '--relayOffset',
                        type=float,
                        help="Optionally define when using the relay "
                             "autotuner feature. Defines a mean value for the "
                             "relay function.  If not provided the default is "
                             "to grab the current process value.")
    parser.add_argument('-t', '--autolockDuration',
                        type=float,
                        help="Define when using the relay autotuner feature."
                             "Provide a time in seconds for autolocker to run "
                             "for. If the plant has a very long response time "
                             "it may be necessary to set this to a very large "
                             "value to obtain a good estimate for PID values.")
    parser.add_argument('-e', '--estimateOnly',
                        action='store_true',
                        help="Activate this flag when running autotuner and "
                             "you dont want to write values out to channels "
                             "at the end of autotune cycle.")
    # todo: remove debug, this is handled by built in __debug__
    parser.add_argument('--debug',
                        action='store_true')
    return parser.parse_args()


class ImportConfig():
    def __init__(self, configfile):
        config = ConfigParser.ConfigParser()
        config.read(configfile)
        self.process = config.get("EPICSChannelConfig",
                                  "process")
        self.actuator = config.get("EPICSChannelConfig",
                                   "actuator")
        try:
            self.PIDsign = config.get("EPICSChannelConfig",
                                      "Sign")
        except Exception:
            self.PIDsign = None
        self.KpParam = config.get("EPICSChannelConfig",
                                  "KpParam")
        self.KiParam = config.get("EPICSChannelConfig",
                                  "KiParam")
        self.KdParam = config.get("EPICSChannelConfig",
                                  "KdParam")
        try:
            self.TtParam = config.get("EPICSChannelConfig",
                                      "TtParam")
        except Exception:
            self.TtParam = None

        try:
            self.TfParam = config.get("EPICSChannelConfig",
                                      "TfParam")
        except Exception:
            self.TfParam = None

        try:
            self.setpoint = config.get("EPICSChannelConfig",
                                       "setpoint")
        except Exception:  # case that user has not specified a setpoint
            self.setpoint = None

        try:
            self.timestep = config.get("EPICSChannelConfig",
                                       "timestep")
        except Exception:  # case no soft channel for loop update rate
            self.timestep = 1  # defults 1 second

        try:
            self.EPICSBlinkChan = config.get("EPICSChannelConfig",
                                             "EPICSBlinkChan")
        except Exception:
            self.EPICSBlinkChan = None

        try:  # try to get list of channels for engauge
            self.requiredstates = config.get("EPICSChannelConfig",
                                             "requiredstates").splitlines()
        except Exception:
            self.requiredstates = []

        self.hard_stops = [config.getfloat("HardActuatorLimits",
                                           "hardstops_lower"),
                           config.getfloat("HardActuatorLimits",
                                           "hardstops_upper")]
        try:
            self.offset = config.getfloat("HardActuatorLimits",
                                          "offset")
        except Exception:
            self.offset = 0.0
        # increment_limit obsolete
        self.increment_limit = config.getfloat("HardActuatorLimits",
                                               "incrementlimit")

        self.EPICS_channel_config_all = config.items("EPICSChannelConfig")
        self.hard_actuator_limits_all = config.items("HardActuatorLimits")


if __name__ == "__main__":
    args = grabInputArgs()  # import cmdline arguments
    param = ImportConfig(args.configfile)  # import config file

    if args.autotune:
        kp, ki, kd = RelayAutoTune(args, param)
        print("Suggested kp, ki, kd are {kp}, {ki}, {kd}".format(kp=kp,
                                                                 ki=ki,
                                                                 kd=kd))
        if not args.estimateOnly:
            caput(param.KpParam, kp)
            caput(param.KiParam, ki)
            caput(param.KdParam, kd)
        else:
            reqChanList = [ch for ch in param.requiredstates]
            caput(reqChanList[0], 1)

    if not args.estimateOnly:
        main(args, param)
