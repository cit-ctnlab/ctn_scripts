'''
mokuGetLastFile
Loads the latest measurement file from moku RAM.
Input Arguments:
ipAddress:  IP Address of Moku. Default is 10.0.1.81.
mokuName:    If used, moku will be searched for by name.
'''

from pymoku import Moku
import argparse
import time


def mokuGetLastFile(ipAddress='10.0.1.81', mokuName=None):
    try:
        if mokuName is None:
            m = Moku(ipAddress)
        else:
            m = Moku.get_by_name(mokuName)
        fileList = m._fs_list('i')
        fileList.sort(key=fnTomktime)
        latestFN = fileList[-1][0]
        print('Latest file found as:', latestFN)
        # Receive the file. length=0 implies receive complete file.
        try:
            m._receive_file(mp='i', fname=latestFN, length=0)
        except BaseException:
            print('Error in uploading file.')
    except BaseException as e:
        print('Error occured: ', e)
    finally:
        try:
            m.close()
        except BaseException:
            print('Failed to connect to moku properly.')
        finally:
            return latestFN


def fnTomktime(ele):
    fn = ele[0]
    tstamp = fn[fn.rfind('_')-8:fn.rfind('_')+7]
    tstruc = time.strptime(tstamp, '%Y%m%d_%H%M%S')
    return time.mktime(tstruc)


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='Loads the latest measurement file from moku RAM.')
    parser.add_argument('-i', '--ipAddress',
                        help='IP address of Moku. Default is 10.0.1.81',
                        default='10.0.1.81')
    parser.add_argument('--mokuName', type=str,
                        help='Connect to Moku by Name.',
                        default=None)
    return parser.parse_args()


if __name__ == '__main__':
    args = grabInputArgs()
    mokuGetLastFile(args.ipAddress, args.mokuName)
