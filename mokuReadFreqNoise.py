'''
mokuReadFreqNoise
This script reads csv data converted using liconvert.exe from the binary
data saved through phasemeter instrument in mokulab.
It automatically changes resolution of frequency bins as frequency increases
to keep 90 data points in each decade.
In the end, the program writes a data file with same name but _ASD attached
in the end witht he frequency ASD data.
'''

import numpy as np
from modifiedPSD import modPSD
import argparse
import os


def mokuReadFreqNoise(dataFileName, asdFileName=None, asdDir=None,
                      verbose=False, reduceTSfile=False):
    saveDir = ''
    saveFileName = dataFileName.replace('.csv', '_ASD.txt')
    if dataFileName.find('/') is not -1:
        saveDir = dataFileName[:dataFileName.rfind('/')+1]
        saveFileName = dataFileName[dataFileName.rfind('/')+1:].replace(
                                                            '.csv', '_ASD.txt')
    if dataFileName.find('\\') is not -1:    # For Windows
        saveDir = dataFileName[:dataFileName.rfind('\\')+1]
        saveFileName = dataFileName[dataFileName.rfind('\\')+1:].replace(
                                                            '.csv', '_ASD.txt')
    if asdFileName is not None:
        saveFileName = asdFileName
        if saveFileName.find('/') is not -1:
            saveDir = ''
        elif saveFileName.find('\\') is not -1:
            saveDir = ''
        elif asdDir is not None:
            saveDir = asdDir
    elif asdDir is not None:
        saveDir = asdDir
    # Till this point saveFilename doesn't have path if saveDir is something.
    saveFileName = saveDir+saveFileName
    if verbose:
        print('Reading data. This might take some time.')
    Data = np.loadtxt(dataFileName, comments='%', delimiter=',')
    twoChannels = False
    if np.shape(Data)[1] > 6:
        twoChannels = True
        if verbose:
            print('Two channels detected.')
    header = ['Duration of measurement: ' + str(Data[-1, 0]) + ' s\n']
    if twoChannels:
        sfstr = 'Set Frequencies1: '
        for sf in np.unique(Data[:, 1]):
            sfstr += str(sf) + 'Hz; '
        sfstr += '\nSet Frequencies2: '
        for sf in np.unique(Data[:, 6]):
            sfstr += str(sf) + 'Hz; '
    else:
        sfstr = 'Set Frequencies: '
        for sf in np.unique(Data[:, 1]):
            sfstr += str(sf) + 'Hz; '
    header += [sfstr+'\n']
    header += ['Comments from moku data file:\n']
    mokuComments = []
    with open(dataFileName, 'r') as f:
        for line in f.readlines():
            if line[0] == '%':
                mokuComments += [line+'\n']
    header += mokuComments
    header += ['#########################################################\n']
    header += ['ASD calculated by mokuReadFreqNoise.py\n']
    if twoChannels:
        header += ['Frequency [Hz]         '
                   'Freq ASD1 [Hz/rtHz]      '
                   'Freq ASDlb1 [Hz/rtHz]    '
                   'Freq ASDub1 [Hz/rtHz]    '
                   'Freq ASD2 [Hz/rtHz]      '
                   'Freq ASDlb2 [Hz/rtHz]    '
                   'Freq ASDub2 [Hz/rtHz]    '
                   'Del Freq ASD [Hz/rtHz]   '
                   'Del Freq ASDlb [Hz/rtHz] '
                   'Del Freq ASDub [Hz/rtHz] \n']
    else:
        header += ['Frequency [Hz]         '
                   'Freq ASD1 [Hz/rtHz]      '
                   'Freq ASDlb1 [Hz/rtHz]    '
                   'Freq ASDub1 [Hz/rtHz]    \n']
    if verbose:
        print('Done reading data. Calculating ASD...')

    timeSeries = Data[:, 0]
    SampleRate = 1/(timeSeries[1] - timeSeries[0])
    if verbose:
        print('Measured Sampling Rate is '+str(np.round(SampleRate, 2))+' Hz')
    # Measured Frequency - Set Frequency to make DC term small.
    freqNoiseSig = Data[:, 2]-Data[:, 1]
    Freq, PSD, lowB, uppB = modPSD(timeSeries, freqNoiseSig,
                                   average='median')
    ASD = np.sqrt(PSD)
    ASDlb = np.sqrt(lowB)
    ASDub = np.sqrt(uppB)
    saveData = np.zeros((len(Freq), 4))
    if twoChannels:
        freqNoiseSig2 = Data[:, 7]-Data[:, 6]
        delta = Data[:, 7]-Data[:, 2]
        Freq, PSD2, lowB2, uppB2 = modPSD(timeSeries, freqNoiseSig2,
                                          average='median')
        Freq, delPSD, delLowB, delUppB = modPSD(timeSeries, delta,
                                                average='median')
        ASD2 = np.sqrt(PSD2)
        ASDlb2 = np.sqrt(lowB2)
        ASDub2 = np.sqrt(uppB2)
        delASD = np.sqrt(delPSD)
        delASDlb = np.sqrt(delLowB)
        delASDub = np.sqrt(delUppB)

        saveData = np.zeros((len(Freq), 10))
        saveData[:, 4] = ASD2
        saveData[:, 5] = ASDlb2
        saveData[:, 6] = ASDub2
        saveData[:, 7] = delASD
        saveData[:, 8] = delASDlb
        saveData[:, 9] = delASDub
    saveData[:, 0] = Freq
    saveData[:, 1] = ASD
    saveData[:, 2] = ASDlb
    saveData[:, 3] = ASDub

    if verbose:
        print('Saving ASD data at '+saveFileName)
    np.savetxt(saveFileName, saveData, header=''.join(header))

    if reduceTSfile:
        newDataFileName = dataFileName.replace('.csv', '.txt')
        if twoChannels:
            timeSeriesData = np.zeros((len(timeSeries), 3))
            timeSeriesData[:, 0] = timeSeries
            timeSeriesData[:, 1] = freqNoiseSig
            timeSeriesData[:, 2] = freqNoiseSig2
            tsheader = mokuComments
            tsheader += ['#################################################\n']
            tsheader += ['File reduced by mokuReadFreqNoise\n']
            tsheader += ['Time (s)    Frequency 1 [Hz]    Frequency 2 [Hz]\n']
            np.savetxt(newDataFileName, timeSeriesData,
                       header=''.join(tsheader))
            return newDataFileName
        else:
            timeSeriesData = np.zeros((len(timeSeries), 2))
            timeSeriesData[:, 0] = timeSeries
            timeSeriesData[:, 1] = freqNoiseSig
            tsheader = mokuComments
            tsheader += ['#################################################\n']
            tsheader += ['File reduced by mokuReadFreqNoise\n']
            tsheader += ['Time (s)    Frequency 1 [Hz]']
            np.savetxt(newDataFileName, timeSeriesData,
                       header=''.join(tsheader))
            return newDataFileName
        os.remove(dataFileName)
    return dataFileName


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script reads csv data converted using liconvert.exe '
                    'from the binary data saved through phasemeter instrument '
                    'in mokulab. It automatically changes resolution of '
                    'frequency bins as frequency increases to keep 90 data '
                    'points in each decade. In the end, the program writes a '
                    'data file with same name but _ASD attached in the end '
                    'with the frequency ASD data.')
    parser.add_argument('dataFileName', type=str,
                        help='Data file in csv format. Use liconvert to '
                             'convert .li file to  .csv')
    parser.add_argument('-f', '--saveFileName', type=str,
                        help='File name to save with. You can also add path '
                             'to the filename here if different from data '
                             'file name. Default is data file with _ASD.txt',
                        default=None)
    parser.add_argument('-s', '--saveDir', type=str,
                        help='Directory address where to save generated file. '
                             'Default is same as data file directory.',
                        default=None)
    parser.add_argument('-v', '--verbose',
                        help='Will diplay some calculation parameters.',
                        action='store_true')
    return parser.parse_args()


if __name__ == "__main__":
    args = grabInputArgs()
    mokuReadFreqNoise(dataFileName=args.dataFileName,
                      asdFileName=args.saveFileName, asdDir=args.saveDir,
                      verbose=args.verbose)
