import time
from epics import caget, caput
import ConfigParser
import argparse
import numpy as np

'''
Author: Anchal Gupta (agupta) - anchal@caltech.edu
Created:  Sep 13, 2019

This code just checks BN Frequency all the time and
calculates its slope.
'''
BNFreqCH = "C3:PSL-PRECAV_BEATNOTE_FREQ"
BNFreqSlopeCH = "C3:PSL-PRECAV_BEATNOTE_FREQ_SLOPE"
BNFreqSlope2CH = "C3:PSL-PRECAV_BEATNOTE_FREQ_SLOPE2"
BNFreqDriftCH = "C3:PSL-PRECAV_BEATNOTE_FREQ_DRIFT"
timestep = 0.1
averagingTime = 10    # Averaging time for slop calculations
observeTime = 60      # Observation time for drift calculation
recLen = int(2*averagingTime/timestep)
obsLen = int(2*observeTime/timestep)
midInd = int(np.floor(recLen/2))
BNFreqTS = np.zeros(max(recLen, obsLen))
BNFreqSlopeTS = np.zeros(recLen)
TS = np.zeros(recLen)

while(1):
    BNFreqTS = np.roll(BNFreqTS,1)
    BNFreqSlopeTS = np.roll(BNFreqSlopeTS,1)
    TS = np.roll(TS,1)
    BNFreqTS[0] = caget(BNFreqCH)
    BNFreqSlopeTS[0] = caget(BNFreqSlopeCH)
    TS[0] = time.time()
    initVal = np.mean(BNFreqTS[midInd:recLen])
    initSlopeVal = np.mean(BNFreqSlopeTS[midInd:recLen])
    initTime = np.mean(TS[midInd:recLen])
    finalVal = np.mean(BNFreqTS[:midInd])
    finalSlopeVal = np.mean(BNFreqSlopeTS[:midInd])
    finalTime = np.mean(TS[:midInd])
    caput(BNFreqSlopeCH, 1000*(finalVal-initVal)/(finalTime-initTime))
    caput(BNFreqSlope2CH,
          1000*(finalSlopeVal-initSlopeVal)/(finalTime-initTime))
    caput(BNFreqDriftCH, 1000*np.ptp(BNFreqTS[:obsLen]))
    time.sleep(timestep)
