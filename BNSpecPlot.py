from noiseBudgetModule import noiseBudget
import numpy as np
import argparse

def BNSpecPlot(spectrum, savedEstPSDs = None,
               label = 'Measured Beatnote', title = 'CTN Noise Budget',
               saveFileName = 'CTN_Noise_Budget_With_Beatnote_Spectrum.pdf',
               savePlot = True, plotList = None, **kwargs):
    nosbud = noiseBudget(lightInit=True)
    if savedEstPSDs is not None:
        nosbud.loadPSD(savedEstPSDs)
        if plotList is None:
            plotList = nosbud.calcPSDList + ['total']
    if plotList is None:
        plotList = []
    beat = np.loadtxt(spectrum)
    if np.shape(beat)[1] == 2:
        nosbud.PSDList['Beat'] = [beat[:, 1]**2, beat[:, 0], label]
    elif np.shape(beat)[1] == 4:
        print('Found lower and upper bounds')
        nosbud.PSDList['Beat'] = [beat[:, 1]**2, beat[:, 0], label,
                                  beat[:, 2]**2, beat[:, 3]**2]
    plotList += ['Beat']
    fig = nosbud.plotPSD(plotList=plotList, title=title,
                         filename=saveFileName, savePlot=savePlot,
                         doTotal=False, **kwargs)
    return fig

def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script reads a file containing beatnote spectrum '
                    '(maybe measured using BNSpec.py) and plots it with '
                    'a noise budget if provided.')
    parser.add_argument('spectrum', type=str,
                        help='File containing beatnote spectrum')
    parser.add_argument('--savedEstPSDs',
                        help='Saved Estimated PSDs (Noise Budget)',
                        default=None)
    parser.add_argument('-l', '--label', type=str,
                        help='Label name for beatnote.',
                        default='Measured Beatnote')
    parser.add_argument('-t', '--title', type=str,
                        help='Title of the plot.',
                        default='CTN Noise Budget')
    parser.add_argument('-s', '--saveFileName', type=str,
                        help='Filename of the saved figure.',
                        default='CTN_Noise_Budget_With_Beatnote_Spectrum.pdf')
    parser.add_argument('-p','--plotList', nargs='+',
                        help='PSD keys to be plotted.',
                        default=None)
    return parser.parse_args()

if __name__ == "__main__":
    args = grabInputArgs()
    BNSpecPlot(spectrum = args.spectrum, savedEstPSDs = args.savedEstPSDs,
               label = args.label, title = args.title,
               saveFileName = args.saveFileName, plotList = args.plotList)
