from epics import caget
import os
import time

def float_to_str(f):
    float_string = repr(f)
    if 'e' in float_string:  # detect scientific notation
        digits, exp = float_string.split('e')
        digits = digits.replace('.', '').replace('-', '')
        exp = int(exp)
        zero_padding = '0' * (abs(int(exp)) - 1)  # minus 1 for decimal point in the sci notation
        sign = '-' if f < 0 else ''
        if exp > 0:
            float_string = '{}{}{}.0'.format(sign, digits, zero_padding)
        else:
            float_string = '{}0.{}{}'.format(sign, zero_padding, digits)
    return float_string

def ftoswprec(f,prec=None):
    if prec is None:
        return float_to_str(f)
    else:
        s = float_to_str(f)
        dp = s.find(".")
        return s[:dp+prec+1]

DirListFile = 'dbfilesdir.txt'

with open(DirListFile) as f:
    DirList = [line.rstrip('\n') for line in f]
    DirList = [line.replace('/dbdir','/home/controls/Git/cit_ctnlab/modbus') for line in DirList]
for dir in DirList:
    try:
        os.chdir(dir)
        FileList = os.listdir(dir)
        for file in FileList:
            if file.find('.db')!=-1:
                #print('Reading '+file+' ...')
                with open(file) as f:
                    lines = f.readlines()
                    nol = len(lines)
                    ii=0
                    copiedLines = []
                    while(ii<nol):
                        copiedLines.append(lines[ii])
                        if lines[ii].find('C3:PSL-')!=-1:
                            line = lines[ii]
                            start = line.find("\"")+1
                            stop = line[start:].find("\"")+start
                            ChName = line[start:stop]
                            #print(ChName)
                            if ii<(nol-1):
                                ii=ii+1
                                ChPrec = None
                                while(lines[ii].find('record')==-1):
                                    if lines[ii].find('field(PREC')!=-1:
                                        #print('Found PREC field')
                                        precLine = lines[ii]
                                        start = precLine.find("\"")+1
                                        stop = precLine[start:].find("\"")+start
                                        ChPrec = int(precLine[start:stop])
                                    if lines[ii].find('field(VAL')!=-1:
                                        #print('Found VAL field')
                                        try:
                                            newVal = caget(ChName)
                                            #newVal = float(ii)
                                            valLine = lines[ii]
                                            start = valLine.find(",")+1
                                            stop = valLine[start:].find(")")+start
                                            newValLine = valLine[:start]+' '+ftoswprec(newVal,ChPrec)+valLine[stop:]
                                            copiedLines.append(newValLine)
                                            #print('Changed value to '+str(newVal))
                                        except Exception:
                                            print(ChName+' not hosted right now.')
                                            copiedLines.append(lines[ii])
                                    else:
                                        copiedLines.append(lines[ii])
                                    ii=ii+1
                                    if ii==nol:
                                        break
                        else:
                            ii = ii+1
                #print('Rewriting '+file+' ...')
                with open(file,'w') as f:
                    f.writelines(copiedLines)
    except Exception:
        print(dir+' was not found.')
