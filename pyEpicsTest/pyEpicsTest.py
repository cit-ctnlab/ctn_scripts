print('Hello World')

#!/usr/bin/env/ python

# Author: Andrew Wade
# Date: 2017 1 September

import time
import ConfigParser
import argparse
from epics import caget

# Input argument parser
parser = argparse.ArgumentParser(
    description='Auto locker script that scans a proccess variable until'
                'either a threshold is met or any on of a list of binary'
                'channels are state false.')
parser.add_argument(
    'configfile',
    type=str,
    help='Here you must enter a config file that contains the channels for'
         'error signal, actuator, engauge and hard limits. You may also'
         'optionally specify binary channels, the autolocker will only engauge'
         'when these are in the True state.')
parser.add_argument('--debug',
                    action='store_true')
args = parser.parse_args()  # Grabs arguments and puts into a local variable


#  Grab config file settings and put in local variables
config = ConfigParser.ConfigParser()
config.read(args.configfile)

monitor = config.get("EPICSChannelConfig",
                     "monitor")  # Monitor for threshold
actuator = config.get("EPICSChannelConfig",
                      "actuator")  # process variable
loopStateEnable = config.get("EPICSChannelConfig",
                             "loopStateEnable")  # Binary engage
sweepRangeUpperChan = config.get("EPICSChannelConfig",
                                 "sweepRangeUpperChan")  # Upper sweep range
sweepRangeLowerChan = config.get("EPICSChannelConfig",
                                 "sweepRangeLowerChan")  # Lower sweep range
sweepStepSizeChan = config.get("EPICSChannelConfig",
                               "sweepStepSizeChan")  # Sweep step size

def main():
    print(monitor)
    print(caget(monitor))
    print(actuator)
    print(caget(actuator))

if __name__ == "__main__": #triger input parser on call
    main()
