'''
mokuPhaseMeterTF
This function runs a time series measurement of phase phasemeter in the two
Moku channels while sweeping function generator in one/both of the output
channels. Ideally, one would use channel 1 output as excitation to the DUT.
The output of the DUT should then be fed to channel 1 input.

Input Arguments:
chan:       {'ch1'(default),'ch2' or 'both'}. Case insensitive.
ipAddress:  IP Address of Moku. Default is 10.0.1.81.
sampleRate: {'veryslow', 'slow', 'medium', 'fast'(default), 'veryfast',
            'ultrafast'} corresponding to {30.5176 smp/s, 122.0703 smp/s,
            488smp/s, 1.9531 ksmp/s(default), 15.625 ksmp/s, 125 ksps/s}.
            Case insensitive.
bandWidth:  {10, 40, 150, 600, 2.5e3, 10e3(default)} in Hz. (Will be rounded
            up to to the nearest multiple 10kHz / 2^N with N = [0,10]). Can be
            a list for different values for different channels.
fiftyr:     50 Ohm inputs. {True, False(default)}. Can be a list for different
            values for different channels.
atten:      Turn on 10x attenuation. {True, False(default)}. Changes the
            dynamic range between 1Vpp(False) and 10Vpp(True). Can be a list
            for different calues for different channels.
ac:         AC coupling. {True, False(Default)}. Can be a list for different
            values for different channels.
altFileRoot: Moku's default fileroot is MokuPhasemeterData which can be
             replaced by providing an alternate file root. Default is None.
dataDirNameRoot: Folder/directory name of the measurement data. If the dir
             doesn't exist, one will be created.
             Default is MokuPhaseMeterTFData.
useExternal: Attempt to lock to an external reference clock.
             {True(default), False}.
useSD:       Whether to log to the SD card, else the internal Moku filesystem.
             {True, False(default)}. Using SD card limits sampling rate.
outChan:     {'ch1'(default),'ch2' or 'both'}. Case insensitive.
amp:         Amplitude in volts. {float, default=0.1}. Can be a list for
             different values for different channels.
startFreq:   Start Frequency of sweep in Hz. {float, default=0.1}.
             Can be a list for different values for different channels.
stopFreq:    Stop Frequency of sweep in Hz. {float, default=10e3}.
             Can be a list for different values for different channels.
noFreqPoints:Number of frequency points in sweep. {int, default=51}.
noIntCycles: Number of integration cycles for each sweep point.
             {int, default=10}
verbose:     If True, print statements will get activated.
Output:
dataDir:     Folder/directory name of the measurement data.
'''

import os
import time
from mokuPhaseMeterTimeSeries import mokuPhaseMeterTimeSeries
import argparse
import numpy as np
import yaml


def mokuPhaseMeterTF(chan='ch1', ipAddress='10.0.1.81',
                     sampleRate='fast', bandWidth=10e3,
                     fiftyr=True, atten=False, ac=False,
                     altFileRoot=None, dataDirNameRoot='MokuPhaseMeterTFData',
                     useExternal=True, useSD=False, outChan='ch1',
                     amp=0.1, startFreq=0.1, stopFreq=10e3, noFreqPoints=51,
                     noIntCycles=10, verbose=False):
    dataDir = dataDirNameRoot + time.strftime("_%Y%m%d_%H%M%S")
    if not os.path.exists(dataDir):
        os.makedirs(dataDir)
    curDir = os.getcwd()
    os.chdir(dataDir)

    if not isinstance(startFreq, list):
        startFreq = [startFreq, startFreq]
    if not isinstance(stopFreq, list):
        stopFreq = [stopFreq, stopFreq]

    freqArr1 = np.logspace(np.log10(startFreq[0]), np.log10(stopFreq[0]),
                           noFreqPoints)
    freqArr2 = np.logspace(np.log10(startFreq[1]), np.log10(stopFreq[1]),
                           noFreqPoints)

    metaData = {}
    for sweepNo in range(noFreqPoints):
        duration = np.maximum(noIntCycles/freqArr1[sweepNo],
                              noIntCycles/freqArr2[sweepNo])
        freq = [freqArr1[sweepNo], freqArr2[sweepNo]]
        fn = mokuPhaseMeterTimeSeries(chan=chan, ipAddress=ipAddress,
                                      duration=duration, sampleRate=sampleRate,
                                      bandWidth=bandWidth, fiftyr=fiftyr,
                                      atten=atten, ac=ac,
                                      altFileRoot=altFileRoot,
                                      useExternal=useExternal, useSD=False,
                                      fileType='bin', outChan=outChan,
                                      wf='sine', amp=amp, freq=freq,
                                      phase=0, phaseLocked=False,
                                      verbose=verbose)
        if fn == 'Error':
            raise RuntimeWarning('Error occured at frequency '
                                 'points'+str(freq)+'\nIgnoring and '
                                 'moving on.')
        else:
            if verbose:
                print('Converting binary file into csv.')
            os.system('~/Git/lireader/liconvert ' + fn)
            os.remove(fn)
            fn = fn.replace('.li', '.csv')
            if verbose:
                print(fn + ' created.')
            if not isinstance(amp, list):
                ampList = [amp, amp]
            metaData[str(sweepNo)] = [str(freq[0]), str(freq[1]),
                                      str(ampList[0]), str(ampList[1]), fn]

    metaDataFileName = dataDir + '.yml'
    with open(metaDataFileName, 'w') as logFile:
        yaml.safe_dump(metaData, logFile)
    os.chdir(curDir)
    return dataDir


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description='This script runs a time series measurement of phase'
                    'using phasemeter in Moku.')
    parser.add_argument('-c', '--chan', type=str,
                        help='ch1(default), ch2 or both. Case insensitive.',
                        default='ch1')
    parser.add_argument('-i', '--ipAddress',
                        help='IP address of Moku. Default is 10.0.1.81',
                        default='10.0.1.81')
    parser.add_argument('-b', '--bandWidth', type=float,
                        help='Phasemeter tracking bandwidth.'
                             'Default is 10kHz.',
                        default=10e3)
    parser.add_argument('--bandWidth2', type=float,
                        help='Phasemeter tracking bandwidth for channel 2',
                        default=None)
    parser.add_argument('-s', '--sampleRate', type=str,
                        help='Sampling Rate. {veryslow, slow , medium ,'
                             'fast, veryfast, ultrafast}. Refer moku manual.'
                             'Default is fast.',
                        default='fast')
    parser.add_argument('--MOhmr',
                        help='1 MOhm input impedance. Default: False',
                        action='store_true')
    parser.add_argument('--MOhmr2',
                        help='1 MOhm input impedance for Ch 2. Default: False',
                        action='store_true')
    parser.add_argument('-a', '--atten',
                        help='10x Attenuation. True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('--atten2',
                        help='10x Attenuation for Ch2.'
                             'True: input range of 10Vpp.'
                             'False: inpute range of 1Vpp. Default: False',
                        action='store_true')
    parser.add_argument('--ac',
                        help='AC Coupling. Default: False',
                        action='store_true')
    parser.add_argument('--ac2',
                        help='AC Coupling for channel 2. Default: False',
                        action='store_true')
    parser.add_argument('--altFileName', type=str,
                        help='Alternate File Root Name.',
                        default=None)
    parser.add_argument('--dataDirNameRoot', type=str,
                        help='Directory root name. Default is '
                             'MokuPhaseMeterTFData',
                        default='MokuPhaseMeterTFData')
    parser.add_argument('--useInternal',
                        help='Use internal 10 MHz clock only. Default: False',
                        action='store_true')
    parser.add_argument('--useSD',
                        help='Use SD Card for storing data. Default: False',
                        action='store_true')
    parser.add_argument('--outChan', type=str,
                        help='ch1(default), ch2 or both.'
                             'Case insensitive.',
                        default='ch1')
    parser.add_argument('--amp1', type=float,
                        help='Channel 1 amplitude in volts. Default 0.1',
                        default=0.1)
    parser.add_argument('--amp2', type=float,
                        help='Channel 2 amplitude in volts. Default 0.1',
                        default=0.1)
    parser.add_argument('--startFreq1', type=float,
                        help='Channel 1 frequency in Hz. Default 0.1',
                        default=0.1)
    parser.add_argument('--startFreq2', type=float,
                        help='Channel 2 frequency in Hz. Default 0.1',
                        default=0.1)
    parser.add_argument('--stopFreq1', type=float,
                        help='Channel 1 frequency in Hz. Default 10e3',
                        default=10e3)
    parser.add_argument('--stopFreq2', type=float,
                        help='Channel 2 frequency in Hz. Default 10e3',
                        default=10e3)
    parser.add_argument('--noFreqPoints', type=int,
                        help='Number of frequency sweep points. Default is 51',
                        default=51)
    parser.add_argument('--noIntCycles', type=int,
                        help='Number of integration cycles at each sweep point'
                             ' .Default is 10.',
                        default=10)
    parser.add_argument('--verbose',
                        help='Print statements will get activated. '
                             'Default: False',
                        action='store_true')
    return parser.parse_args()


def ftoss(value):
    sgn = np.sign(value)
    if sgn == 0:
        return '0'
    else:
        absv = np.abs(value)
        ex = np.minimum(12, np.maximum(-15, np.floor(np.log10(absv))))
        sv = absv/(10**ex)
        sc = str(int(np.floor(ex/3)))
        unit = {
            '-5': 'f',
            '-4': 'p',
            '-3': 'n',
            '-2': 'u',
            '-1': 'm',
            '0': '',
            '1': 'k',
            '2': 'M',
            '3': 'G',
            '4': 'T'
        }[sc]
        val = str(sgn*sv*10**(ex % 3))
        return val+unit


if __name__ == '__main__':
    args = grabInputArgs()
    fiftyr = [not args.MOhmr, not args.MOhmr2]
    atten = [args.atten, args.atten2]
    ac = [args.ac, args.ac2]
    if args.outChan.lower() == 'both':
        amp = [args.amp1, args.amp2]
        startFreq = [args.startFreq1, args.startFreq2]
        stopFreq = [args.stopFreq1, args.stopFreq2]
    elif args.outChan.lower() == 'ch1':
        amp = args.amp1
        startFreq = args.startFreq1
        stopFreq = args.stopFreq1
    elif args.outChan.lower() == 'ch2':
        amp = args.amp2
        startFreq = args.startFreq2
        stopFreq = args.stopFreq2
    else:
        raise RuntimeError('outChan can be only ch1, ch2 or both')

    if args.bandWidth2 is None:
        bandWidth = args.bandWidth
    else:
        bandWidth = [args.bandWidth, args.bandWidth2]
    output = mokuPhaseMeterTF(chan=args.chan,
                              ipAddress=args.ipAddress,
                              sampleRate=args.sampleRate,
                              bandWidth=bandWidth,
                              fiftyr=fiftyr, atten=atten, ac=ac,
                              altFileRoot=args.altFileName,
                              dataDirNameRoot=args.dataDirNameRoot,
                              useExternal=not args.useInternal,
                              useSD=args.useSD, outChan=args.outChan,
                              startFreq=startFreq, stopFreq=stopFreq,
                              noFreqPoints=args.noFreqPoints,
                              noIntCycles=args.noIntCycles,
                              verbose=args.verbose)
    print('Data Directory Name:', output)
