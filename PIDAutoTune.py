#!/usr/bin/env python

import re
import time
import os
from ezca import Ezca
import ConfigParser
import argparse

import numpy as np  # trying to avoid this to keep as bare bones as possible, maybe strip later

'''
Author: Andrew Wade (awade) - awade@ligo.caltech.edu
Created:  March 05, 2018 (from an earlier port of a perl script used at 40m)

Change log:
    20180214 (awade): removed all debug and comment junk
'''

RCPID = Ezca(ifo=None, logger=False)  # initialize ezca python access to channels object


def main():
    args = grabInputArgs()  # import cmdline arguments
    param = ImportConfig(args.configfile)  # import config file

    timestep = RCPID.read(param.timestep, log=False)
    
    try:
        relayAmplitude = param.relayAmplitude
    except Exception:
        relayAmplitude = -0.005
    print("Doing Relay Test now...")

    Tc, a = RelayTester(args, param, relayAmplitude, timestep)
    Kc = 4*relayAmplitude/a/np.pi
    
    print("mean estimated amplitude a = {}".format(a))
    print("Kc = {}".format(Kc))
    print("Critical Period Tc  = {}".format(Tc))
    # These are modified PID Zeigler-Nichols tuning constants suggest by by
    # by David I. Wilson in "Relay-based PID Tuning" technical note
    kp = 0.2 * Kc
    ki = kp / (0.5 * Tc)
    kd = kp * (Tc/3)

    print("Suggested kp, ki, kd are {kp}, {ki}, {kd}".format(kp=kp, ki=ki, kd=kd))

    #RCPID.write(param.KpParam, kp, monitor=False)
    #RCPID.write(param.KiParam, ki, monitor=False)  # may need to LP I and D terms to smooth transitions
    #RCPID.write(param.KdParam, kd, monitor=False)
    RCPID.write(param.actuator,0,monitor=False)


def RelayTester(args, param, relayAmplitude, timestep):
    #PIDEngaugeChannel = "C3:PSL-SCAV_FSS_SLOWPID_EN"
    PIDEngaugeChannel = param.engage
    print("Using engage channel: {}".format(PIDEngaugeChannel))
    print("Using actuator channel: {}".format(param.actuator))
    print("relayAmplitude = {}".format(relayAmplitude))
    runtime = 60 * 60 *15 
    I0 = RCPID.read(param.actuator)  # just grabs last value of actuator for reference
    d = relayAmplitude  # relay amplitude
    offset = 0.25
    RCPID.write(PIDEngaugeChannel, 0, monitor=False)
    eprev = 0; epprev = 0
    countCrossings = maxCycle = minCycle = np.array([])
    count = maxRelay = minRelay = 0
    for ii in range(int(runtime/timestep)):
        s = RCPID.read(param.setpoint)
        p = RCPID.read(param.process)
        e = s - p 
        count += 1
        maxRelay = max(maxRelay, e)
        minRelay = min(minRelay, e)
        if e > 0:  # implement a simple relay funtion to drive characteristic plant oscillations 
            RCPID.write(param.actuator, offset + d + I0, monitor=False)
            if np.mod(ii,600) == 0:
                print("Error is {}. Setting Diff Heating to {}".format(e,offset+d+I0))
            # Here we use the previous two points to reject spurious crossings from sensor noise
            # This is crude, averaging would be better, but it works ok"
            if not eprev>0 and not epprev>0:  # reset counter and add number of counts to list
                countCrossings = np.append(countCrossings, count)
                maxCycle = np.append(maxCycle, maxRelay)
                minCycle = np.append(minCycle, minRelay)
                count = maxRelay = minRelay = 0
        else:
            RCPID.write(param.actuator, offset - d + I0, monitor=False)
            if np.mod(ii,600) == 0:
                print("Error is {}. Setting Diff Heating to {}".format(e,offset-d+I0))
        epprev = eprev
        eprev = e
        time.sleep(timestep)
    RelayPeriod = countCrossings * timestep  # compute the period from crossings
    print("Plant self oscillation period = {}".format(RelayPeriod))
    MedianRelayPeriod = np.median(RelayPeriod)  # Here we must use median to avoid count false triggers and initial start up
    print("Median Relay Period = {}".format(MedianRelayPeriod))
    print("Min amplitudes = {}".format(minCycle))
    print("Max amplitudes = {}".format(maxCycle))
    RCPID.write(PIDEngaugeChannel, 1, monitor=False)

    InducedppAmplitude = np.median(maxCycle) - np.median(minCycle)
#    Kc = 4*relayAmplitude/a/np.pi
#    
#    print("mean estimated amplitude a = {}".format(a))
#    print("Kc = {}".format(Kc))
#    print("Critical Period Tc  = {}".format(Tc))
#    # These are modified PID Zeigler-Nichols tuning constants suggest by by
#    # by David I. Wilson in "Relay-based PID Tuning" technical note
#    kp = 0.2 * Kc
#    ki = kp / (0.5 * Tc)
#    kd = kp * (Tc/3)
#
#    print("Suggested kp, ki, kd are {kp}, {ki}, {kd}".format(kp=kp, ki=ki, kd=kd))

#    RCPID.write(param.KpParam, kp, monitor=False)
#    RCPID.write(param.KiParam, ki, monitor=False)  # may need to LP I and D terms to smooth transitions
#    RCPID.write(param.KdParam, kd, monitor=False)
    return MedianRelayPeriod, InducedppAmplitude
   
def GradDecent(args, param, timestep):   
    # Set some limits for search range
    rangeKp = [-0.00250, 0.0]
    rangeKi = [-0.001, 0.0]
    rangeKd = [-0.005, 0.0]
#    if ANDChannels(param.requiredstates):  # all required binary chans true

    # Read the PID parameters to get current state
    Kp = RCPID.read(param.KpParam)
    Ki = RCPID.read(param.KiParam)
    Kd = RCPID.read(param.KdParam)

    Kp = 0.0
    Ki = 0  #= -0.00040
    Kd = 0  #-0.00100

    testtime = 60
    deltax = deltay = deltaz = -0.0001
    gamma = 0.0001 * 0.5  # needs to be on order of deltas to be a sensible approximation at local point, also factor of 0.5 seem to help in initial parabola tests

    print(time.ctime())
    
    # Find initialize first point 
    stepup = StepTest(param,
                      rail(Kp, rangeKp[0], rangeKp[1]),
                      rail(Ki, rangeKi[0], rangeKi[1]),
                      rail(Kd, rangeKd[0], rangeKd[1]),
                      0.0,
                      testtime)
    stepdown = StepTest(param,
                        rail(Kp, rangeKp[0], rangeKp[1]),
                        rail(Ki, rangeKi[0], rangeKi[1]),
                        rail(Kd, rangeKd[0], rangeKd[1]),
                        1.0,
                        testtime)
    p1 = stepup + stepdown

    print("p1 = {}".format(p1))
    for ii in range(0,1):
        
        # Find point two p2 
        stepup = StepTest(param,
                          rail(Kp+deltax, rangeKp[0], rangeKp[1]),
                          rail(Ki, rangeKi[0], rangeKi[1]),
                          rail(Kd, rangeKd[0], rangeKd[1]),
                          0.0,
                          testtime)
        stepdown = StepTest(param,
                          rail(Kp+deltax, rangeKp[0], rangeKp[1]),
                          rail(Ki, rangeKi[0], rangeKi[1]),
                          rail(Kd, rangeKd[0], rangeKd[1]),
                          1.0,
                          testtime)
        p2 = stepup + stepdown

        print("p2 = {}".format(p2))

        dx = (p2 - p1)/deltax  # here I scale the function by the mean value to normalize
        print("p2 - p1  = {}".format(p2-p1))
        print("dx = {}".format(dx))
        Kp = Kp + gamma * dx
        print("Suggested next KP is {}".format(Kp))
        p1 = p2

def StepTest(param,Kp, Ki, Kd, SetPoint, TestPeriod):
    ''' Compute cumulative error from a step test between two setpoint values.'''
    timestep = RCPID.read(param.timestep, log=False)
    score = 0
    nn = int(TestPeriod/timestep)
    
    # Perform step test with parameters
    RCPID.write(param.KpParam, Kp, monitor=False)
    RCPID.write(param.KiParam, Ki, monitor=False)  # may need to LP I and D terms to smooth transitions
    RCPID.write(param.KdParam, Kd, monitor=False)
   
    # Set up variables for exponentially weighted moving average
    fc = 1  # frequency of filter
    alpha = timestep * 2 * np.pi * fc / (timestep * 2 * np.pi * fc + 1)  # smoothing factor

    time.sleep(1.0)  # Let it settle before hitting with step function

    # Populate boxcar average and allow time for PID to settle
#    buff = np.array([])
#    lenboxcar = 10
#    for ii in range(0,lenboxcar):
#        buff = np.append(buff, RCPID.read(param.process)) 
#        time.sleep(0.1)  # Let it settle before hitting with step function
#    pavg = np.average(buff)    
#    pprev = np.append(buff, RCPID.read(param.process)) 
    

    RCPID.write(param.setpoint, SetPoint, monitor=False)

    pav = RCPID.read(param.process)
    for ii in range(1,nn):
        s = RCPID.read(param.setpoint)
        p = RCPID.read(param.process)
        pavg = alpha * p + (1 - alpha) * pavg
# commented shitty boxcar        pavg, buff = pavg + p/buff.size - buff[0]/buff.size, np.append(buff[1:], p)  # adding some poor quality LP filtering  in form of boxcar

        e = s - pavg
        score = score + abs(e)
#        print(e)

        time.sleep(timestep)
    print(score/TestPeriod)
    return score/TestPeriod

# rails x to the limit of given range
def rail(x, minx, maxx):
    return max(min(maxx, x), minx)


def ANDChannels(chanList):  # find boolean AND of a list of binary EPICs chans
    return all([RCPID.read(ii, log=False) for ii in chanList])


def grabInputArgs():
    parser = argparse.ArgumentParser(
        description="PID Autotune tests"
                    " channels")
    parser.add_argument('configfile',
                        type=str,
                        help="Enter a config .ini file + path")
    parser.add_argument('--debug',
                        action='store_true')
    return parser.parse_args()


class ImportConfig():
    def __init__(self, configfile):
        config = ConfigParser.ConfigParser()
        config.read(configfile)
        self.engage = config.get("EPICSChannelConfig",
                                  "engage")
        self.process = config.get("EPICSChannelConfig",
                                  "process")
        self.actuator = config.get("EPICSChannelConfig",
                                   "actuator")
        self.KpParam = config.get("EPICSChannelConfig",
                                  "KpParam")
        self.KiParam = config.get("EPICSChannelConfig",
                                  "KiParam")
        self.KdParam = config.get("EPICSChannelConfig",
                                  "KdParam")
        try:
            self.relayAmplitude = config.getfloat("HardActuatorLimits",
                                                  "relayAmplitude")
        except Exception:
            self.relayAmplitude = -0.005
        try:
            self.TtParam = config.get("EPICSChannelConfig",
                                       "TtParam")
        except Exception:
            self.TtParam = None

        try:
            self.TfParam = config.get("EPICSChannelConfig",
                                       "TfParam")
        except Exception:
            self.TfParam = None

        try:
            self.setpoint = config.get("EPICSChannelConfig",
                                       "setpoint")
        except Exception:  # case that user has not specified a setpoint
            self.setpoint = None

        try:
            self.timestep = config.get("EPICSChannelConfig",
                                       "timestep")
        except Exception:  # case no soft channel for loop update rate
            self.timestep = 1  # defults 1 second

        try:
            self.EPICSBlinkChan = config.get("EPICSChannelConfig",
                                             "EPICSBlinkChan")
        except Exception:
            self.EPICSBlinkChan = None

        try:  # try to get list of channels for engauge
            self.requiredstates = config.get("EPICSChannelConfig",
                                             "requiredstates").splitlines()
        except Exception:
            self.requiredstates = []

        self.hard_stops = [config.getfloat("HardActuatorLimits",
                                           "hardstops_lower"),
                           config.getfloat("HardActuatorLimits",
                                           "hardstops_upper")]
        # increment_limit obsolete
        self.increment_limit = config.getfloat("HardActuatorLimits",
                                               "incrementlimit")

        self.EPICS_channel_config_all = config.items("EPICSChannelConfig")
        self.hard_actuator_limits_all = config.items("HardActuatorLimits")


if __name__ == "__main__":
    main()
